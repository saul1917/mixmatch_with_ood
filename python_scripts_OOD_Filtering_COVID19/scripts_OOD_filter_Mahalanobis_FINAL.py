
import ood_filter_Mahalanobis

def run_Mahalanobis_Indiana_no_preprocessed():
    ood_filter_Mahalanobis_obj = ood_filter_Mahalanobis.OOD_filter_Mahalanobis(model_name="densenet")
    BATCH_SIZE = 4
    ood_filter_Mahalanobis_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/INDIANA_65_CR_35_No_Preprocessed/batches_unlabeled/batch_", ood_perc=35,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/MAHALANOBIS/INDIANA_65_CR_35_THRESH_65_No_Preprocessed",
        ood_thresh=0.65, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_ood/")

    ood_filter_Mahalanobis_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/INDIANA_35_CR_65_No_Preprocessed/batches_unlabeled/batch_",
        ood_perc=65,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/MAHALANOBIS/INDIANA_35_CR_65_THRESH_35_No_Preprocessed",
        ood_thresh=0.35, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_ood/")

def run_Mahalanobis_Indiana_preprocessed():
    ood_filter_Mahalanobis_obj = ood_filter_Mahalanobis.OOD_filter_Mahalanobis(model_name="densenet")
    BATCH_SIZE = 4
    ood_filter_Mahalanobis_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/INDIANA_65_CR_35/batches_unlabeled/batch_", ood_perc=35,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/MAHALANOBIS/INDIANA_65_CR_35_THRESH_65",
        ood_thresh=0.65, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_ood/")

    ood_filter_Mahalanobis_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/INDIANA_35_CR_65/batches_unlabeled/batch_",
        ood_perc=65,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/MAHALANOBIS/INDIANA_35_CR_65_THRESH_35",
        ood_thresh=0.35, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_ood/")


def run_Mahalanobis_NIS():
    ood_filter_Mahalanobis_obj = ood_filter_Mahalanobis.OOD_filter_Mahalanobis(model_name="densenet")
    BATCH_SIZE = 4
    """ood_filter_Mahalanobis_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/OOD_CR_35_NIS_65/batch_", ood_perc=35,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/MAHALANOBIS/NIS_65_CR_35_THRESH_65",
        ood_thresh=0.65, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_ood/")"""

    ood_filter_Mahalanobis_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/OOD_CR_65_NIS_35/batch_", ood_perc=65,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/MAHALANOBIS/OOD_CR_65_NIS_35_THRESH_65",
        ood_thresh=0.35, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_ood/")



def run_Mahalanobis_CHINA():
    ood_filter_Mahalanobis_obj = ood_filter_Mahalanobis.OOD_filter_Mahalanobis(model_name="densenet")
    BATCH_SIZE = 4
    ood_filter_Mahalanobis_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/OOD_CR_35_CHINA_65/batch_", ood_perc=35,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/MAHALANOBIS/CHINA_65_CR_35_THRESH_65",
        ood_thresh=0.65, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_ood/")

    ood_filter_Mahalanobis_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/OOD_CR_65_CHINA_35/batch_", ood_perc=65,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/MAHALANOBIS/CR_65_CHINA_35_THRESH_65",
        ood_thresh=0.35, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_ood/")

run_Mahalanobis_Indiana_no_preprocessed()