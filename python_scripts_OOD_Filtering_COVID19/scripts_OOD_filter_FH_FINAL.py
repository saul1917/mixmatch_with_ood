
import ood_filter_neg_likelihood

def run_FH_Indiana_no_preprocessed():
    ood_filter_FH_obj = ood_filter_neg_likelihood.OOD_filter_neg_likelihood(model_name="densenet")
    BATCH_SIZE = 4
    ood_filter_FH_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/INDIANA_65_CR_35_No_Preprocessed/batches_unlabeled/batch_", ood_perc=35,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/FEATURE_HISTOGRAMS/INDIANA_65_CR_35_THRESH_65_No_Preprocessed",
        ood_thresh=0.65, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_ood/")

    ood_filter_FH_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/INDIANA_35_CR_65_No_Preprocessed/batches_unlabeled/batch_",
        ood_perc=65,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/FEATURE_HISTOGRAMS/INDIANA_35_CR_65_THRESH_35_No_Preprocessed",
        ood_thresh=0.35, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_ood/")

def run_FH_Indiana():
    ood_filter_FH_obj = ood_filter_neg_likelihood.OOD_filter_neg_likelihood(model_name="densenet")
    BATCH_SIZE = 4
    """ood_filter_FH_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/INDIANA_65_CR_35/batches_unlabeled/batch_", ood_perc=35,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/FEATURE_HISTOGRAMS/INDIANA_65_CR_35_THRESH_65",
        ood_thresh=0.65, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_ood/")"""

    ood_filter_FH_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/INDIANA_35_CR_65/batches_unlabeled/batch_",
        ood_perc=65,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/FEATURE_HISTOGRAMS/INDIANA_35_CR_65_THRESH_35",
        ood_thresh=0.35, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_ood/")


def run_FH_NIS():
    ood_filter_FH_obj = ood_filter_neg_likelihood.OOD_filter_neg_likelihood(model_name="densenet")
    BATCH_SIZE = 4
    ood_filter_FH_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/OOD_CR_35_NIS_65/batch_", ood_perc=35,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/FEATURE_HISTOGRAMS/NIS_65_CR_35_THRESH_65",
        ood_thresh=0.65, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_oodrun_FH_NIS35/")

    ood_filter_FH_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/OOD_CR_65_NIS_35/batch_", ood_perc=65,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/FEATURE_HISTOGRAMS/OOD_CR_65_NIS_35_THRESH_35",
        ood_thresh=0.35, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_oodrun_FH_NIS65/")



def run_FH_CHINA():
    ood_filter_FH_obj = ood_filter_neg_likelihood.OOD_filter_neg_likelihood(model_name="densenet")
    BATCH_SIZE = 4
    ood_filter_FH_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/OOD_CR_35_CHINA_65/batch_", ood_perc=35,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/FEATURE_HISTOGRAMS/CHINA_65_CR_35_THRESH_65",
        ood_thresh=0.65, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_oodFH_CHINA35 /")

    ood_filter_FH_obj.run_filter(
        path_bunch1="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/LABELED/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/OOD_CR_65_CHINA_35/batch_", ood_perc=65,
        num_unlabeled=90, num_batches=10, size_image=220, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/FILTERED_UNLABELED/FEATURE_HISTOGRAMS/CR_65_CHINA_35_THRESH_35",
        ood_thresh=0.35, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_oodFH_CHINA65/")

run_FH_Indiana_no_preprocessed()