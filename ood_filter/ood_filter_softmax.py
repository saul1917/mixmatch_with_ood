#OOD filter using softmax
from PIL import Image as Pili
import torch
from fastai.vision import *
from torchvision import models, transforms
import random
import shutil
torch.set_printoptions(threshold=10_000)

BATCH_SIZE = 4

class OOD_filter_softmax:
    def __init__(self, model_name = "wideresnet", num_classes = 2):
        """
        OOD filter constructor
        :param model_name: name of the model to get the feature space from, pretrained with imagenet, wideresnet and densenet have been tested so far
        """
        self.model_name = model_name
        #list of scores, filepaths and labels of unlabeled data processed
        self.scores = []
        self.file_paths = []
        self.labels = []
        self.file_names = []
        self.num_classes = num_classes



    def databunch_to_tensor(self, databunch1):
        """
        Convert the databunch to tensor set
        :param databunch1: databunch to convert
        :return: converted tensor
        """
        # tensor of tensor
        tensor_bunch = torch.zeros(len(databunch1.train_ds), databunch1.train_ds[0][0].shape[0],
                                   databunch1.train_ds[0][0].shape[1], databunch1.train_ds[0][0].shape[2], device="cuda:0")
        for i in range(0, len(databunch1.train_ds)):
            # print(databunch1.train_ds[i][0].data.shape)
            tensor_bunch[i, :, :, :] = databunch1.train_ds[i][0].data.to(device="cuda:0")

        return tensor_bunch

    def copy_filtered_observations(self, dir_root, percent_to_filter):
        """
        Copy filtered observations applying the thresholds
        :param dir_root: directory where to copy the filtered data
        :param   percent_to_filter: percent of observations to keep
        :return:
        """

        thresh = self.get_threshold(percent_to_filter)
        print("Threshold ", thresh)
        print("Percent to threshold: ", percent_to_filter)
        num_selected = 0
        # store info about the observation filtering
        self.info = [""] * len(self.scores)
        # only filter training
        for i in range(0, len(self.scores)):
            # print("self.file_paths[i]")
            # print(self.file_paths[i])
            # print("Path ", self.file_paths[i])
            # print("Current score ", self.scores[i], " of observation ", i, " condition ", "test" not in str(self.file_paths[i]))
            if (self.scores[i] > thresh and "test" not in str(self.file_paths[i])):
                num_selected += 1
                rand_class = random.randint(0, self.num_classes - 1)
                path_dest = dir_root + "/train/" + str(rand_class) + "/"
                path_origin = self.file_paths[i]

                try:
                    os.makedirs(path_dest)
                except:
                    a = 0
                file_name = os.path.basename(self.file_paths[i])
                # print("File to copy", path_origin)
                # print("Path to copy", path_dest + file_name)
                shutil.copyfile(path_origin, path_dest + file_name)
                self.info[i] = "Copied, is training observation higher than thresh " + str(thresh)

            if ("test" in str(self.file_paths[i])):
                path_dest = dir_root + "/test/" + str(self.labels[i]) + "/"
                path_origin = self.file_paths[i]
                try:
                    os.makedirs(path_dest)
                except:
                    a = 0
                file_name = os.path.basename(self.file_paths[i])
                # print("File to copy", path_origin)
                # print("Path to copy", path_dest + file_name)
                shutil.copyfile(path_origin, path_dest + file_name)
                self.info[i] = "Copied, is test observation"
        print("Number of unlabeled observations preserved: ", num_selected)


    def get_threshold(self, percent_to_filter):
        """
        Get the threshold according to the list of observations and the percent of data to filter
        :param percent_to_filter: value from 0 to 1
        :return: the threshold
        """
        new_scores_no_validation = []
        for i in range(0, len(self.scores)):
            if("test" not in str(self.file_paths[i])):
                new_scores_no_validation += [self.scores[i]]

        #percent_to_filter is from  0 to 1
        print("Sorting descending order...")
        new_scores_no_validation.sort(reverse = True)
        num_to_filter = int(percent_to_filter * len(new_scores_no_validation))
        threshold = new_scores_no_validation[num_to_filter]
        return threshold


    def pil2fast(self, img, im_size = 110):
        data_transform = transforms.Compose(
            [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
        return Image(data_transform(img))

    """
    Get file names in path
    """
    def get_file_names_in_path(self, path):
        print("getting file names in path ", path)
        files_list = []
        for path, subdirs, files in os.walk(path):
            for name in files:
                file_path = os.path.join(path, name)
                files_list += [file_path]
        return files_list


    """
    Softmax calculator
    """
    def get_softmax_fastai(self, learner, input_image, is_ssdl = False):
        """

        :param learner:
        :param input_image:
        :param is_ssdl:
        :return:
        """

        cat_tensor, tensor_class, model_output = learner.predict(input_image, with_dropout = False)
        if (is_ssdl):
            model_output = nn.functional.softmax(model_output, dim=0)
        val, index = torch.max(model_output, 0)
        score_predicted = model_output[index]
        return tensor_class.item(), score_predicted


    def calculate_uncertainty_softmax_fastai_images(self, fastai_model, img_path, is_ssdl = True, im_size = 110):
        """

        :param fastai_model:
        :param img_path:
        :param is_ssdl:
        :param class_label:
        :param im_size:
        :return:
        """
        uncertainties_correct = []
        uncertainties_wrong = []
        softmaxes_all_imgs = []
        pred_class_all_imgs = []
        file_paths = []
        file_names =[]
        labels =[]
        scores = []
        list_images = self.get_file_names_in_path(img_path)
        print("total of images ", len(list_images))
        curr_label = 0
        for i in range(0, len(list_images)):
            complete_path = list_images[i]
            file_paths += [complete_path]
            file_names += [list_images[i]]
            image_pil = Pili.open(complete_path).convert('RGB')
            image_fastai = self.pil2fast(image_pil, im_size =im_size)
            pred_class, stds_sums_all = self.get_softmax_fastai(fastai_model, image_fastai, is_ssdl = is_ssdl)
            #use output with no dropout for pred class
            cat_tensor, tensor_class, model_output = fastai_model.predict(image_fastai, with_dropout=False)
            softmaxes_all_imgs += [stds_sums_all]
            """if(tensor_class.item() != class_label):
                uncertainties_wrong += [stds_sums_all]
            else:
                uncertainties_correct += [stds_sums_all]"""
            #saves the score
            scores += [stds_sums_all.item()]
            #pick any folder for destination, anyways this information is not used in Mixmatch training
            labels += [curr_label]
            if(curr_label == 0):
                curr_label = 1
            else:
                curr_label = 0

        self.file_paths = file_paths
        self.file_names = file_names
        self.scores = scores
        self.labels = labels

        return torch.tensor(softmaxes_all_imgs), uncertainties_correct, uncertainties_wrong



    def run_filter(self, path_bunch1, path_bunch2, ood_perc=100, num_unlabeled=3000,  num_batches=10, size_image=120, batch_size_p=BATCH_SIZE, dir_filtered_root = "/media/Data/saul/Datasets/Covid19/Dataset/OOD_COVID19/OOD_FILTERED/batch_", ood_thresh = 0.8, path_reports_ood = "/reports_ood_softmax/", model_name = "densenet" ):
        """
        :param path_bunch1: path for the first data bunch, labeled data
        :param path_bunch2: unlabeled data
        :param ood_perc: percentage of data ood in the unlabeled dataset
        :param num_unlabeled: number of unlabeled observations in the unlabeled dataset
        :param name_ood_dataset: name of the unlabeled dataset
        :param num_batches: Number of batches of the unlabeled dataset to filter
        :param size_image: input image dimensions for the feature extractor
        :param batch_size_p: batch size
        :param dir_filtered_root: path for the filtered data to be stored
        :param ood_thresh: ood threshold to apply
        :param path_reports_ood: path for the ood filtering reports
        :return:
        """
        batch_size_unlabeled = 2
        batch_size_labeled = 10
        epochs_train_model = 50
        model_fastai = models.densenet121
        if(model_name == "densenet"):
            model_fastai = models.densenet121
            print("Using densenet")
        if (model_name == "wideresnet"):
            model_fastai = models.WideResNet(num_groups=3,N=4,num_classes=2,k = 2,start_nf=size_image)
            print("Using wideresnet")
        if (model_name == "alexnet"):
            model_fastai = models.alexnet
            print("Using alexnet")


        global key
        key = "pdf"
        print("Filtering OOD data for dataset at: ", path_bunch2)
        print("Number of batches ", num_batches)
        for num_batch_data in range(0, num_batches):
            self.scores = []
            self.file_paths = []
            self.labels = []
            self.file_names = []
            # load pre-trained model, CORRECTION
            # model = models.alexnet(pretrained=True)

            # number of histogram bins
            num_bins = 15
            print("Processing batch of labeled and unlabeled data: ", num_batch_data)
            # paths of data for all batches
            # DEBUG INCLUDE TRAIN
            path_labeled = path_bunch1 + "/batch_" + str(num_batch_data)
            path_unlabeled = path_bunch2 + str(num_batch_data) + "/batch_" + str(num_batch_data) + "_num_unlabeled_" + str(
                num_unlabeled) + "_ood_perc_" + str(ood_perc)+ "/train/"
            print("path labeled ", path_labeled)
            print("path unlabeled ", path_unlabeled)
            # get the dataset readers
            #  S_l
            databunch_labeled = (ImageList.from_folder(path_labeled)
                                 .split_by_folder(valid="test")
                                 .label_from_folder()
                                 .transform(size=size_image)
                                 .databunch())
            #et tensor bunches
            tensorbunch_labeled = self.databunch_to_tensor(databunch_labeled)
            num_obs_labeled = tensorbunch_labeled.shape[0]
            print("Number of  labeled observations in batch: ", num_obs_labeled)
            #train the model to use for the softmax
            learner = cnn_learner(data=databunch_labeled, base_arch=model_fastai, metrics=[accuracy])
            # train the model
            print("Training the model...")
            learner.fit_one_cycle(epochs_train_model, max_lr=0.00002)
            #calculate the softmaxes
            self.calculate_uncertainty_softmax_fastai_images(learner, path_unlabeled, is_ssdl=False, im_size=size_image)


            #filter out the data and copy it
            dir_filtered = dir_filtered_root + "/batch_" + str(num_batch_data) + "/batch_" + str(
                num_batch_data) + "_num_unlabeled_" + str(num_unlabeled) + "_ood_perc_" + str(ood_perc) + "/"
            self.copy_filtered_observations(dir_filtered, ood_thresh)
            # save filtering report
            print("file_names lenght ", len(self.file_names))
            print("scores ", len(self.scores))
            dict_csv = {'File_names': self.file_names,
                        'Scores': self.scores, "Info": self.info}
            dataframe = pd.DataFrame(dict_csv, columns=['File_names', 'Scores', 'Info'])
            dataframe.to_csv(path_reports_ood + 'scores_files_batch' + str(num_batch_data) + '.csv', index=False,
                             header=True)
            print(dataframe)

def run_prueba():
    print("doing test")
    ood_filter_neg_likelihood_obj = OOD_filter_softmax(model_name="densenet")
    BATCH_SIZE = 4

    """
    :param distance: distance_str
    :return:
    """
    # S_l  is the IID data for indiana i.e image_67.jpg
    # "/media/Data/saul/Datasets/Covid19/Dataset/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels"
    # S_u is contaminated  dataset
    # /media/Data/saul/Datasets/Covid19/Dataset/OOD_COVID19/OOD_CR_25

    """ood_filter_neg_likelihood.run_filter(
        path_bunch1="/media/Data/saul/Datasets/Covid19/Dataset/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels",
        path_bunch2="/media/Data/saul/Datasets/Covid19/Dataset/OOD_COVID19/OOD_CR_50/batch_", ood_perc=50,
        num_unlabeled=90, name_ood_dataset="SVHN", num_batches=10, size_image=100, batch_size_p=BATCH_SIZE,
        dir_filtered_root="/media/Data/saul/Datasets/Covid19/Dataset/OOD_COVID19/OOD_FILTERED_CR_50_THRESH_70_ALL/",
        ood_thresh=0.7, path_reports_ood="/media/Data/saul/Code_Projects/OOD4SSDL/utilities/reports_ood/")"""

    ood_filter_neg_likelihood_obj.run_filter(
        path_bunch1="E:/GoogleDrive/DATASETS_TEMP/batches_labeled_undersampled_in_dist_BINARY_CR_30_val/",
        path_bunch2="E:/GoogleDrive/DATASETS_TEMP/OOD_SIMPLE/batch_", ood_perc=50,
        num_unlabeled=90, num_batches=1, size_image=105, batch_size_p=BATCH_SIZE,
        dir_filtered_root="E:/GoogleDrive/DATASETS_TEMP/Filtered",
        ood_thresh=0.7, path_reports_ood="E:/GoogleDrive/DATASETS_TEMP/Filtered")



def simple_test_fastai():
    size_image = 105
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/batches_labeled_undersampled_in_dist_BINARY_CR_30_val/batch_0/"
    #  S_l
    databunch_labeled = (ImageList.from_folder(path_labeled)
                         .split_none()
                         .label_from_folder()
                         .transform(size=size_image)
                         .databunch())
    model_fastai = models.densenet121
    learner = cnn_learner(data=databunch_labeled, base_arch=model_fastai, metrics=[accuracy])
    learner.fit_one_cycle(50, max_lr=0.00002)

