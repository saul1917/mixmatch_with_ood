from PIL import Image as Pili
import scipy
from math import log

import torch
from fastai.vision import *
from fastai.callbacks import CSVLogger
from fastai.callbacks.hooks import *
from torchvision import models, transforms
import cv2
from os import listdir
from os.path import isfile, join
from MixMatch_OOD_main_balance_control_COVID_cnn_learner_hm_uncertainty import MixMatchImageList
from MixMatch_OOD_main_balance_control_COVID_cnn_learner_hm_uncertainty import MixupLoss
from MixMatch_OOD_main_balance_control_COVID_cnn_learner_hm_uncertainty import MixMatchTrainer
import pandas as pd
from scipy.stats import entropy
#Tests the softmax and MCD uncertainty estimators


def get_mc_dropout_fastai_all_classes(learner, input_image, max_its=10, is_ssdl = False):
    """
    Tests monte carlo dropout for all class outputs
    :param learner: fastaimodel
    :param input_image:  input image
    :param max_its: maximum iterations for montecarlo calculation
    :param is_ssdl: is semi supervised?
    :return:
    """
    num_classes = 2
    outputs = torch.zeros(max_its, num_classes)
    preds_class = []
    for i in range(max_its):
        cat_tensor, tensor_class, model_output = learner.predict(input_image, with_dropout=True)
        #print("Model output before")
        #print(model_output)
        if (is_ssdl):
            model_output = nn.functional.softmax(model_output, dim=0)
        #print("Model output after")
        #print(model_output)
        pred_class = tensor_class.item()
        preds_class.append(pred_class)
        #for each class
        outputs[i, :] = model_output

    std_all_classes = torch.std(outputs, 0)
    std_best_class = std_all_classes.sum()
    pred_class_mean = np.mean(np.array(preds_class))
    return outputs, pred_class_mean, std_best_class

def pil2fast(img, im_size = 110):
    """
    convert pil 2 fastai  aimge
    :param img: input image
    :param im_size: image dimensions
    :return: pil image
    """
    data_transform = transforms.Compose(
        [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
    return Image(data_transform(img))

"""
Get file names in path
"""
def get_file_names_in_path(path):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    return onlyfiles



def get_softmax_fastai(learner, input_image, is_ssdl = False):
    """
    Softmax calculator
    :param learner: fastai model
    :param input_image: input image
    :param is_ssdl: is semisupervised?
    :return: class and softmax score (confidence)
    """

    cat_tensor, tensor_class, model_output = learner.predict(input_image, with_dropout = False)
    if (is_ssdl):
        model_output = nn.functional.softmax(model_output, dim=0)
    val, index = torch.max(model_output, 0)
    score_predicted = model_output[index]
    return tensor_class.item(), score_predicted


def calculate_uncertainty_softmax_fastai_images(fastai_model, img_path, is_ssdl = False, class_label = 0, im_size = 110):
    """
    Calculates the softmax uncertainty for a set of images, uses learner.predict
    :param fastai_model: fastai model
    :param img_path: path of images to calculate the uncertainty
    :param is_ssdl: is the model semi supervised?
    :param class_label: class label of the set of images
    :param im_size: image dimensions?
    :return: uncertainties correct images and uncertainties incorrect images
    """
    uncertainties_correct = []
    uncertainties_wrong = []
    softmaxes_all_imgs = []
    pred_class_all_imgs = []
    list_images = get_file_names_in_path(img_path)
    print("total of images ", len(list_images))
    for i in range(0, len(list_images)):
        complete_path = img_path + list_images[i]
        image_pil = Pili.open(complete_path).convert('RGB')
        image_fastai = pil2fast(image_pil, im_size =im_size)
        pred_class, stds_sums_all = get_softmax_fastai(fastai_model, image_fastai, is_ssdl = is_ssdl)
        #use output with no dropout for pred class
        cat_tensor, tensor_class, model_output = fastai_model.predict(image_fastai, with_dropout=False)
        softmaxes_all_imgs += [stds_sums_all]
        if(tensor_class.item() != class_label):
            uncertainties_wrong += [stds_sums_all]
        else:
            uncertainties_correct += [stds_sums_all]



    return torch.tensor(softmaxes_all_imgs), uncertainties_correct, uncertainties_wrong


def calculate_uncertainty_mcd_fastai_images(fastai_model, img_path, num_iters = 100, is_ssdl = False, class_label = 0, im_size = 110):
    """
    Calculates the MCD of a set of images
    :param fastai_model: fastai's model
    :param img_path: input image path
    :param num_iters: number of iterations for the MCD method
    :param is_ssdl: is semi supervised?
    :param class_label: class label for all the images
    :param im_size: image size?
    :return:
    """
    uncertainties_correct = []
    uncertainties_wrong = []
    stds_all_imgs = []
    pred_class_all_imgs = []
    list_images = get_file_names_in_path(img_path)
    print("total of images ", len(list_images))

    for i in range(0, len(list_images)):

        complete_path = img_path + list_images[i]
        image_pil = Pili.open(complete_path).convert('RGB')
        image_fastai = pil2fast(image_pil, im_size =im_size)
        outputs, pred_class, stds_sums_all = get_mc_dropout_fastai_all_classes(fastai_model, image_fastai, max_its=num_iters, is_ssdl=is_ssdl)
        #use output with no dropout for pred class
        cat_tensor, tensor_class, model_output = fastai_model.predict(image_fastai, with_dropout=False)


        stds_all_imgs += [stds_sums_all]
        if(tensor_class.item() != class_label):
            uncertainties_wrong += [stds_sums_all]
        else:
            uncertainties_correct += [stds_sums_all]

    print("uncertainties wrong")
    print(uncertainties_wrong)
    print("uncertainties correct")
    print(uncertainties_correct)

    return torch.tensor(stds_all_imgs), uncertainties_correct, uncertainties_wrong


"""
Estimate uncertainty for a given learner and set of images
"""
def estimate_uncertainty_softmax(learner, img_path, img_names, im_size = 110):
    """
    Uses straight model outputs
    :param learner:
    :param img_path:
    :param img_names:
    :param im_size:
    :return:
    """
    data_transform = transforms.Compose(
        [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
    model = learner.model
    # Variables needed to test
    # Stores the img we want to infer
    image = torch.zeros(1, 3, im_size, im_size).cuda()
    # Stores the results of everything
    number_classes = 2
    results_per_class = torch.zeros(len(img_names), number_classes)
    results_predicted_class = torch.zeros(len(img_names))
    #results for all images
    soft_max_best_class = torch.zeros(len(img_names))
    # Set to eval mode on all layers
    model.eval()

    #Infer all the dataset and store the results
    # For each image
    print("Number of images to do inference with : ", len(img_names))
    for i in range(len(img_names)):
        # Read the img
        im = Pili.open(img_path + img_names[i]).convert('RGB')
        # Apply transformations to it
        #print("Image shape")
        image[0] = data_transform(im)
        # Infer, apply softmax to the result and store it
        scores_all_classes = nn.functional.softmax(model(image).data.cpu(), dim=1).squeeze()
        #eliminate one unnecessary dimension
        #be sure that we are exploring dimension 0
        value, index = torch.max(scores_all_classes, 0)
        predicted_class = index
        results_per_class[i, :] = nn.functional.softmax(model(image).data.cpu(), dim=1)
        soft_max_best_class[i] = scores_all_classes[predicted_class]
    return results_per_class, soft_max_best_class

# Function to enable dropout in eval time
def enable_dropout(m):
  for each_module in m.modules():
    if each_module.__class__.__name__.startswith('Dropout'):
      each_module.train()



def load_ssdl_model(model_name = "CHINA_SSDL_model", path_models = "C:/Users/Usuario/Desktop/FAST_AI_TEST/models/", path_dataset ="C:/Users/Usuario/Desktop/FAST_AI_TEST/CR_DATASET/", SIZE_IMAGE = 110, BATCH_SIZE = 12, WORKERS = 8):
    """
    Semi supervised model loading
    :param model_name: model identifier, used in the stored model, previously trained
    :param path_models: path to the models to load
    :param path_dataset: path of the datasets to use
    :param SIZE_IMAGE: size of the image
    :param BATCH_SIZE: batch size
    :param WORKERS: number of workers
    :return:
    """
    model = models.densenet121
    meanDatasetComplete = [0.4823, 0.4823, 0.4823]
    stdDatasetComplete = [0.2231, 0.2231, 0.2231]
    norm_stats = (meanDatasetComplete, stdDatasetComplete)
    # create the dataset
    data_full = (ImageList.from_folder(path_dataset)
                 .split_by_folder(valid="test")
                 .label_from_folder()
                 .transform(
        get_transforms(do_flip=True, flip_vert=True, max_zoom=1, max_warp=None, p_affine=0, p_lighting=0),
        size=SIZE_IMAGE)
                 .databunch(bs=BATCH_SIZE, num_workers=WORKERS)
                 .normalize(norm_stats))


    print("Path ", path_models)
    # assuming ssdl model
    mixloss = MixupLoss()
    setattr(mixloss, 'reduction', 'none')
    learner_loaded_ssdl = cnn_learner(data=data_full, base_arch=model, loss_func=mixloss,
                                      callback_fns=[MixMatchTrainer], metrics=[accuracy])

    learner_loaded_ssdl.load(path_models + model_name)
    print("SSDL  model loaded")
    return learner_loaded_ssdl


def load_no_ssdl_model(path_models = "C:/Users/Usuario/Desktop/FAST_AI_TEST/models/", path_dataset ="C:/Users/Usuario/Desktop/FAST_AI_TEST/CR_DATASET/", SIZE_IMAGE = 110, BATCH_SIZE = 12, WORKERS = 8, model_name = 'model_NO_SSDL'):
    """
    Supervised model loading
    :param path_models: path to the models
    :param path_dataset: path to the dataset
    :param SIZE_IMAGE: image size
    :param BATCH_SIZE: batch size
    :param WORKERS: number of workers
    :param model_name: name or identifier of the model
    :return:
    """
    model = models.densenet121
    meanDatasetComplete = [0.4823, 0.4823, 0.4823]
    stdDatasetComplete = [0.2231, 0.2231, 0.2231]
    norm_stats = (meanDatasetComplete, stdDatasetComplete)
    #loss used in the model
    calculate_cross_entropy = nn.CrossEntropyLoss()
    # create the dataset
    data_full = (ImageList.from_folder(path_dataset)
                 .split_by_folder(valid="test")
                 .label_from_folder()
                 .transform(
        get_transforms(do_flip=True, flip_vert=True, max_zoom=1, max_warp=None, p_affine=0, p_lighting=0),
        size=SIZE_IMAGE)
                 .databunch(bs=BATCH_SIZE, num_workers=WORKERS)
                 .normalize(norm_stats))
    learner_loaded_no_ssdl = cnn_learner(data=data_full, base_arch=model, loss_func=calculate_cross_entropy,
                                         metrics=[accuracy])
    #load learner
    learner_loaded_no_ssdl.load(path_models + model_name)
    print("No SSDL model loaded!")
    return learner_loaded_no_ssdl




def test_uncertainty_softmax(learner_loaded_ssdl, learner_loaded_no_ssdl, img_path_iod, img_path_ood):
    """
    Tester for the COVID-19 data, using softmax
    :param learner_loaded_ssdl: mixmatch trained model
    :param learner_loaded_no_ssdl: supervised model to be loaded
    :param img_path_iod: image path for the in distribution data
    :param img_path_ood: image path for the out of distribution data
    :return:
    """

    #define uncertainty estimation parameters
    #img_names_ood = ["20588164.bmp", "22580218.bmp", "22613848.bmp", "22613996.bmp", "22670442.bmp", "22670488.bmp"]
    img_names_ood = get_file_names_in_path(img_path_ood)
    print("ood images list")
    print(img_names_ood)
    #img_names_iod = ["0_chest_ray.bmp", "10_chest_ray.bmp", "11_chest_ray.bmp", "12_chest_ray.bmp"]
    img_names_iod = get_file_names_in_path(img_path_iod)
    print("Starting uncertainty estimation for SSDL model...")
    print("iod images list ")
    print(img_names_iod)
    #one std per class
    pred,  softmaxes_ood_ssdl = estimate_uncertainty_softmax(learner_loaded_ssdl, img_path_ood, img_names_ood, im_size=110)
    pred,  softmaxes_iod_ssdl = estimate_uncertainty_softmax(learner_loaded_ssdl, img_path_iod, img_names_iod, im_size=110)
    print("Uncertainty estimation done!")
    print("Softmaxes for ood image: ", softmaxes_ood_ssdl)
    print("Softmaxes for iod image: ", softmaxes_iod_ssdl)

    print("Starting uncertainty estimation for NO SSDL model...")
    # one std per class
    pred,  softmaxes_ood_no_ssdl = estimate_uncertainty_softmax(learner_loaded_no_ssdl,  img_path_ood, img_names_ood, im_size=110)
    pred,  softmaxes_iod_no_ssdl = estimate_uncertainty_softmax(learner_loaded_no_ssdl,  img_path_iod, img_names_iod, im_size=110)
    print("Uncertainty estimation done!")
    print("softmaxes for ood image: ", softmaxes_ood_no_ssdl)
    print("softmaxes for iod image: ", softmaxes_iod_no_ssdl)

    print("Final results")
    #higher the better, std ood ssdl should  be higher than with no ssdl
    res_ood = torch.mean(softmaxes_ood_ssdl - softmaxes_ood_no_ssdl)
    res_ood_std = torch.std(softmaxes_ood_ssdl - softmaxes_ood_no_ssdl)
    #lower the better,
    res_iod = torch.mean(softmaxes_iod_ssdl - softmaxes_iod_no_ssdl)
    res_iod_std = torch.std(softmaxes_iod_ssdl - softmaxes_iod_no_ssdl)
    print("softmaxes OOD SSDL - NO SSDL, lower the better: ", res_ood, " std ", res_ood_std)
    print("softmaxes IOD SSDL - NO SSDL, higher the better: ", res_iod, " std ", res_iod_std)
    return softmaxes_iod_ssdl, softmaxes_ood_ssdl, softmaxes_iod_no_ssdl, softmaxes_ood_no_ssdl




def test_uncertainty_std(learner_loaded_ssdl, learner_loaded_no_ssdl, img_path_iod, img_path_ood, class_label):
    """
    Test for the MCD method
    :param learner_loaded_ssdl: path for the ssdl model
    :param learner_loaded_no_ssdl:path for the no ssdl model
    :param img_path_iod: in distribution data path
    :param img_path_ood: out of distribution data path
    :param class_label: class label
    :return:
    """
    #define uncertainty estimation parameters
    num_inferences = 100
    #img_names_ood = ["20588164.bmp", "22580218.bmp", "22613848.bmp", "22613996.bmp", "22670442.bmp", "22670488.bmp"]
    img_names_ood = get_file_names_in_path(img_path_ood)
    print("ood images list")
    print(img_names_ood)
    #img_names_iod = ["0_chest_ray.bmp", "10_chest_ray.bmp", "11_chest_ray.bmp", "12_chest_ray.bmp"]
    img_names_iod = get_file_names_in_path(img_path_iod)
    print("Starting uncertainty estimation for SSDL model...")
    print("iod images list ")
    print(img_names_iod)
    #one std per class
    pred,  std_ood_ssdl, list_correct, list_wrong = estimate_MCD_uncertainty_std_2(learner_loaded_ssdl, num_inferences, img_path_ood, img_names_ood)
    pred,  std_iod_ssdl, list_correct_ssdl, list_wrong_ssdl  = estimate_MCD_uncertainty_std_2(learner_loaded_ssdl, num_inferences, img_path_iod, img_names_iod, correct_label=class_label)
    print("Uncertainty estimation done!")
    print("Std for ood image: ", std_ood_ssdl)
    print("Std for iod image: ", std_iod_ssdl)
    print("list correct")
    print(list_correct_ssdl)
    print("list wrong")
    print(list_wrong_ssdl)

    print("Starting uncertainty estimation for NO SSDL model...")
    # one std per class
    pred,  std_ood_no_ssdl, list_correct, list_wrong  = estimate_MCD_uncertainty_std_2(learner_loaded_no_ssdl, num_inferences, img_path_ood, img_names_ood, im_size=110)
    pred,  std_iod_no_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl  = estimate_MCD_uncertainty_std_2(learner_loaded_no_ssdl, num_inferences, img_path_iod, img_names_iod, im_size=110, correct_label=class_label)
    print("Uncertainty estimation done!")
    print("Std for ood image: ", std_ood_no_ssdl)
    print("Std for iod image: ", std_iod_no_ssdl)
    print("list correct")
    print(list_correct)
    print("list wrong")
    print(list_wrong)
    print("Final results")
    #higher the better, std ood ssdl should  be higher than with no ssdl
    res_ood = torch.mean(std_ood_ssdl - std_ood_no_ssdl)
    res_ood_std = torch.std(std_ood_ssdl - std_ood_no_ssdl)
    #lower the better,
    res_iod = torch.mean(std_iod_ssdl - std_iod_no_ssdl)
    res_iod_std = torch.std(std_iod_ssdl - std_iod_no_ssdl)
    print("STD OOD SSDL - NO SSDL, higher the better: ", res_ood, " std ", res_ood_std)
    print("STD IOD SSDL - NO SSDL, lower the better: ", res_iod, " std ", res_iod_std)
    return std_iod_ssdl, std_ood_ssdl, std_iod_no_ssdl, std_ood_no_ssdl, list_correct_ssdl, list_wrong_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl




def estimate_MCD_uncertainty_std_2(learner, num_inferences, img_path, img_names,  im_size=110, number_classes=2, correct_label=-1):
    """
    Estimate the MCD uncertainty using the std, using the bare model output
    :param learner: fastai model
    :param num_inferences: number of inferences for the MCD algorithm
    :param img_path: image path
    :param img_names:
    :param im_size: image resolution
    :param number_classes: number of classes
    :param correct_label:
    :return:
    """
    # Fetch the images
    #img_names = get_file_names_in_path(img_path)
    data_transform = transforms.Compose(
        [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
    model = learner.model
    # Variables needed to test
    # Stores the img we want to infer
    print("im size ", im_size)
    image = torch.zeros(1, 3, im_size, im_size).cuda()
    # Stores the results of everything

    results_per_class = torch.zeros(num_inferences, len(img_names), number_classes)
    results_predicted_class = torch.zeros(num_inferences, len(img_names))
    # results for all images
    std_preds_best_class = torch.zeros(len(img_names))
    # store uncerainty estimations for wrong and correct predictions
    stds_wrong_preds = []
    stds_correct_preds = []
    # Set to eval mode on all layers
    model.eval()
    # Enable dropout layers
    enable_dropout(model)
    # Infer all the dataset and store the results
    # For each image
    print("Number of images to do inference with : ", len(img_names))
    predicted_class_all_images = []
    for i in range(len(img_names)):
        # Read the img
        im = Pili.open(img_path + img_names[i]).convert('RGB')
        # Apply transformations to it
        # print("Image shape")
        image[0] = data_transform(im)

        # Run inference <num_inferences> times
        print("Making ", num_inferences, " inferences...")
        predicted_class_all_inferences = []
        for it in range(num_inferences):
            # Net scores for all classes
            scores_all_classes = model(image).data.cpu().squeeze()

            # be sure that we are exploring dimension 0
            value, index = torch.max(scores_all_classes, 0)
            predicted_class = index
            # accumulate to do class estimation
            predicted_class_all_inferences += [predicted_class]
            results_per_class[it, i, :] = model(image).data.cpu()
            results_predicted_class[it, i] = scores_all_classes[predicted_class]
        # get mean predicted class, rounding it
        mean_predicted_class_image = torch.round(
            torch.mean(torch.tensor(predicted_class_all_inferences, dtype=torch.float)))
        # store predicted class per image
        predicted_class_all_images += [mean_predicted_class_image]

    '''     Calc stats of the inference     '''
    # Calc of the previous runs
    for i in range(len(img_names)):
        # Calculates std
        std_preds_best_class[i] = torch.std(results_predicted_class[:, i], dim=0)
        # keep uncertainties for correct and wrong predictions
        if (correct_label != -1 and correct_label == predicted_class_all_images[i]):
            stds_correct_preds += [std_preds_best_class[i]]
        elif (correct_label != -1 and correct_label != predicted_class_all_images[i]):
            stds_wrong_preds += [std_preds_best_class[i]]
    return results_per_class, std_preds_best_class, stds_correct_preds, stds_wrong_preds


def normalize_entropy(p):
    """
    Normalizes the entropy if needed
    :param p:
    :return:
    """
    entropy = 0
    for p_i in p:
        entropy -= p_i * log(p_i)
    print("Entropy ", entropy)
    num_buckets = len(p)
    p_norm = [1 / num_buckets] * num_buckets
    norm_coef = 0
    for p_norm_i in p_norm:
        norm_coef -= p_norm_i * log(p_norm_i)
    normalized_entropy = entropy / norm_coef
    confidence = 1 - normalized_entropy
    return confidence, normalized_entropy


def test_mcd(batch_number = 1):
    """
    Test the MCD method
    :param batch_number: given batch
    :return:
    """
    # train_covid_model(NUMBER_EPOCHS = 1)
    print("Pytorch current device")
    print(torch.cuda.current_device())
    class_label_iod_test_data = 0
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    #img_path_iod = "C:/Users/Usuario/Desktop/FAST_AI_TEST/0_test_china/"
    img_path_iod = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(class_label_iod_test_data) + "/"
    img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0/"
    #img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0c/"
    path_models = "C:/Users/Usuario/Desktop/FAST_AI_TEST/models/"

    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_"
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_"
    learner_no_ssdl = load_no_ssdl_model(path_models = path_models, path_dataset = path_labeled, model_name = model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models = path_models, path_dataset = path_labeled, model_name = model_name_ssdl)

    stds_iod_ssdl, stds_ood_ssdl, stds_iod_no_ssdl, stds_ood_no_ssdl, list_correct_ssdl, list_wrong_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl = test_uncertainty_std(learner_loaded_no_ssdl=learner_no_ssdl, learner_loaded_ssdl=learner_ssdl, img_path_iod=img_path_iod, img_path_ood=img_path_ood, class_label=class_label_iod_test_data)

    #softmaxes_iod_ssdl, softmaxes_ood_ssdl, softmaxes_iod_no_ssdl, softmaxes_ood_no_ssdl = test_uncertainty_softmax(learner_loaded_no_ssdl=learner_no_ssdl, learner_loaded_ssdl= learner_ssdl,  img_path_iod=img_path_iod, img_path_ood= img_path_ood, class_label=class_label_iod_test_data)
    #summary_name = "summary_softmax_china_batch_" + str(batch_number)
    summary_name = "summary_std_china_batch_" + str(batch_number)
    create_summaries_csv(summary_name, stds_iod_ssdl, stds_ood_ssdl, stds_iod_no_ssdl, stds_ood_no_ssdl, list_correct_ssdl, list_wrong_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl)


def create_summaries_csv(summary_name, softmaxes_iod_ssdl, softmaxes_ood_ssdl, softmaxes_iod_no_ssdl, softmaxes_ood_no_ssdl, list_correct_ssdl, list_wrong_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl):
    """
    Creates a csv with the summary of the results
    :param summary_name:
    :param softmaxes_iod_ssdl: softmax scores for the IOD data, SSDL model
    :param softmaxes_ood_ssdl: softmax scores for the OOD data, SSDL model
    :param softmaxes_iod_no_ssdl: softmax scores for the IOD data, supervised model
    :param softmaxes_ood_no_ssdl: softmax scores for the OOD data, supervised model
    :param list_correct_ssdl: list of uncertainties for correct estimations, SSDL model
    :param list_wrong_ssdl: list of uncertainties for incorrect estimations, SSDL model
    :param list_correct_no_ssdl: list of uncertainties for correct estimations, supervised model
    :param list_wrong_no_ssdl: list of uncertainties for incorrect estimations, supervised model
    :return:
    """
    list_iod_ssdl = concat_mean_std_to_list(softmaxes_iod_ssdl)
    list_ood_ssdl = concat_mean_std_to_list(softmaxes_ood_ssdl)
    list_iod_no_ssdl =concat_mean_std_to_list(softmaxes_iod_no_ssdl)
    list_ood_no_ssdl = concat_mean_std_to_list(softmaxes_ood_no_ssdl)
    #concat stats for correct and wrong lists
    print("Type of list correct ", type(list_correct_ssdl) )
    list_correct_ssdl = ["Correct"] + concat_mean_std_to_list(torch.tensor(list_correct_ssdl))
    list_wrong_ssdl = ["Incorrect"] + concat_mean_std_to_list(torch.tensor(list_wrong_ssdl))
    list_correct_no_ssdl = ["Correct"] + concat_mean_std_to_list(torch.tensor(list_correct_no_ssdl))
    list_wrong_no_ssdl = ["Incorrect"] + concat_mean_std_to_list(torch.tensor(list_wrong_no_ssdl))


    #iod data results
    stat, p_value_iod = scipy.stats.wilcoxon(list_iod_ssdl, list_iod_no_ssdl, correction=True)
    list_iod_ssdl += [p_value_iod]
    list_iod_no_ssdl += [p_value_iod]
    #create dataframe for IOD
    print("P value IOD ", p_value_iod)
    data_dictionary_iod = {'Softmaxes_iod_ssdl': list_iod_ssdl, 'Softmaxes_iod_no_ssdl': list_iod_no_ssdl}

    dataframe_iod = pd.DataFrame(data_dictionary_iod, columns=["Softmaxes_iod_ssdl", "Softmaxes_iod_no_ssdl"])
    dataframe_iod.to_csv(summary_name + "_iod.csv", index=False)
    #ood data results

    print(list_ood_ssdl)
    print(list_ood_no_ssdl)
    stat, p_value_ood = scipy.stats.wilcoxon(list_ood_ssdl, list_ood_no_ssdl, correction=True)
    list_ood_ssdl += [p_value_ood]
    list_ood_no_ssdl += [p_value_ood]
    print("P value OOD ", p_value_ood)

    data_dictionary_ood = {'Softmaxes_ood_ssdl': list_ood_ssdl, 'Softmaxes_ood_no_ssdl': list_ood_no_ssdl}
    dataframe_ood = pd.DataFrame(data_dictionary_ood, columns=["Softmaxes_ood_ssdl", "Softmaxes_ood_no_ssdl"])
    dataframe_ood.to_csv(summary_name + "_ood.csv", index=False)
    print("Summaries written!")
    items = [list_correct_ssdl, list_wrong_ssdl]
    df_ssdl = pd.DataFrame(items)
    df_ssdl.to_csv(summary_name + "ssdl_uncertainty_wrong_correct.csv", index=False )
    items = [list_correct_no_ssdl, list_wrong_no_ssdl]
    df_no_ssdl = pd.DataFrame(items)
    df_no_ssdl.to_csv(summary_name + "no_ssdl_uncertainty_wrong_correct.csv", index=False)
    print("Wrong/correct summaries written!")




def create_summaries_csv_individual(summary_name, softmaxes_iod_ssdl, softmaxes_ood_ssdl, list_correct_ssdl, list_wrong_ssdl):
    """

    :param summary_name:
    :param softmaxes_iod_ssdl: softmax scores for IOD data, SSDL model
    :param softmaxes_ood_ssdl: softmax scores for OOD data, SSDL model
    :param list_correct_ssdl: list of correct uncertainties, SSDL model
    :param list_wrong_ssdl: list of incorrect uncertainties, SSDL model
    :return:
    """
    list_iod_ssdl = concat_mean_std_to_list(softmaxes_iod_ssdl)
    list_ood_ssdl = concat_mean_std_to_list(softmaxes_ood_ssdl)

    #concat stats for correct and wrong lists
    print("Type of list correct ", type(list_correct_ssdl) )
    list_correct_ssdl = ["Correct"] + concat_mean_std_to_list(torch.tensor(list_correct_ssdl))
    list_wrong_ssdl = ["Incorrect"] + concat_mean_std_to_list(torch.tensor(list_wrong_ssdl))
    data_dictionary_ood = {'Uncertainty_ood': list_ood_ssdl}
    dataframe_ood = pd.DataFrame(data_dictionary_ood, columns=["Uncertainty_ood"])
    dataframe_ood.to_csv(summary_name + "ood_uncertainty.csv", index=False)
    data_dictionary_iod = {'Uncertainty_iod': list_iod_ssdl}
    dataframe_iod = pd.DataFrame(data_dictionary_iod, columns=["Uncertainty_iod"])
    dataframe_iod.to_csv(summary_name + "iod_uncertainty.csv", index=False)
    print("Summaries written!")
    items = [list_correct_ssdl, list_wrong_ssdl]
    df_ssdl = pd.DataFrame(items)
    df_ssdl.to_csv(summary_name + "_uncertainty_wrong_correct.csv", index=False )
    print("Wrong/correct summaries written!")


def concat_mean_std_to_list(torch_tensor):
    list_tensor = torch_tensor.numpy().tolist() + [torch.mean(torch_tensor).item()] + [torch.std(torch_tensor).item()]
    return list_tensor


def test_entropy_norm():
    p = [0.9, 0.08, 0.01, 0.0009, 0.0001]
    (confidence, normalized_entropy) = normalize_entropy(p)
    print("confidence ", confidence)
    print("norm entropy ", normalized_entropy)

if __name__ == '__main__':
    test_mcd()

