import torchvision.transforms as tfms
from PIL import Image as Pili
import scipy
from math import log

import torch
from fastai.vision import *
from fastai.callbacks import CSVLogger
from fastai.callbacks.hooks import *

from torchvision import models, transforms
import cv2
from os import listdir
from os.path import isfile, join

import pandas as pd
from scipy.stats import entropy
import DUQ_uncertainty as duq



def estimate_uncertainty_softmax(learner, img_path, img_names, im_size = 110):
    """
    Softmax uncertainty estimator
    :param learner:
    :param img_path:
    :param img_names:
    :param im_size:
    :return:
    """
    data_transform = transforms.Compose(
        [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
    model = learner.model
    # Variables needed to test
    # Stores the img we want to infer
    image = torch.zeros(1, 3, im_size, im_size).cuda()
    # Stores the results of everything
    number_classes = 2
    results_per_class = torch.zeros(len(img_names), number_classes)
    results_predicted_class = torch.zeros(len(img_names))
    #results for all images
    soft_max_best_class = torch.zeros(len(img_names))
    # Set to eval mode on all layers
    model.eval()

    #Infer all the dataset and store the results
    # For each image
    print("Number of images to do inference with : ", len(img_names))
    for i in range(len(img_names)):
        # Read the img
        im = Pili.open(img_path + img_names[i]).convert('RGB')
        # Apply transformations to it
        #print("Image shape")
        image[0] = data_transform(im)
        # Infer, apply softmax to the result and store it
        scores_all_classes = nn.functional.softmax(model(image).data.cpu(), dim=1).squeeze()
        #eliminate one unnecessary dimension
        #be sure that we are exploring dimension 0
        value, index = torch.max(scores_all_classes, 0)
        predicted_class = index
        results_per_class[i, :] = nn.functional.softmax(model(image).data.cpu(), dim=1)
        soft_max_best_class[i] = scores_all_classes[predicted_class]
    return results_per_class, soft_max_best_class



def get_softmax_fastai(learner, input_image, is_ssdl = False):
    """
    Softmax estimator for fastai models
    :param learner: fastai model
    :param input_image:  input image
    :param is_ssdl: is ssdl (mixmatch)?
    :return:
    """

    cat_tensor, tensor_class, model_output = learner.predict(input_image, with_dropout = False)
    if (is_ssdl):
        model_output = nn.functional.softmax(model_output, dim=0)
    val, index = torch.max(model_output, 0)
    score_predicted = model_output[index]
    return tensor_class.item(), score_predicted


def calculate_uncertainty_softmax_fastai_images(fastai_model, img_path, is_ssdl = False, class_label = 0, im_size = 110):
    """

    :param fastai_model:
    :param img_path:
    :param is_ssdl:
    :param class_label:
    :param im_size:
    :return:
    """
    uncertainties_correct = []
    uncertainties_wrong = []
    softmaxes_all_imgs = []
    pred_class_all_imgs = []
    list_images = get_file_names_in_path(img_path)
    print("total of images ", len(list_images))
    for i in range(0, len(list_images)):
        complete_path = img_path + list_images[i]
        image_pil = Pili.open(complete_path).convert('RGB')
        image_fastai = pil2fast(image_pil, im_size =im_size)
        pred_class, softmax_out = get_softmax_fastai(fastai_model, image_fastai, is_ssdl = is_ssdl)
        #use output with no dropout for pred class
        cat_tensor, tensor_class, model_output = fastai_model.predict(image_fastai, with_dropout=False)
        softmaxes_all_imgs += [softmax_out.item()]
        if(tensor_class.item() != class_label):
            uncertainties_wrong += [softmax_out]
        else:
            uncertainties_correct += [softmax_out.item()]



    return torch.tensor(softmaxes_all_imgs), uncertainties_correct, uncertainties_wrong
