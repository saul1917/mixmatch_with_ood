
import torchvision.transforms as tfms
from PIL import Image as Pili
import scipy
from math import log
from gradcam import *
import torch
from fastai.vision import *
from fastai.callbacks import CSVLogger
from fastai.callbacks.hooks import *
from grad_cam import (BackPropagation, Deconvolution, GradCAM, GuidedBackPropagation)
from torchvision import models, transforms
import cv2
from os import listdir
from os.path import isfile, join
from fast_ai_example import MixMatchImageList
from fast_ai_example import MixupLoss
from fast_ai_example import MixMatchTrainer
import pandas as pd
from scipy.stats import entropy
import DUQ_uncertainty as duq
"""
Get file names in path
"""
def get_file_names_in_path(path):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    return onlyfiles


"""
Sanity checks
"""


def test_compare_fastai_pytorch_output():
    class_label = 0
    im_size = 110
    ssdl_model = load_ssdl_model(path_models="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/",
                                 path_dataset="E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1",
                                 SIZE_IMAGE=110, BATCH_SIZE=12,
                                 WORKERS=8, model_name='CHINA_SSDL_model_batch_1_')
    no_ssdl_model, data_full = load_no_ssdl_model(path_models="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/",
                                                  path_dataset="E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1",
                                                  SIZE_IMAGE=110, BATCH_SIZE=12,
                                                  WORKERS=8, model_name='CHINA_NO_SSDL_model_batch_1_')
    img_path_iod = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1/test/" + str(class_label) + "/"
    list_images = get_file_names_in_path(img_path_iod)
    print("total of images ", len(list_images))
    fails_fastai = 0
    fails_pytorch = 0
    for i in range(0, len(list_images)):
        complete_path = img_path_iod + list_images[i]
        image_pil = Pili.open(complete_path).convert('RGB')
        image_ood_fastai = pil2fast(image_pil)
        data_transform = transforms.Compose(
            [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
        image_ood_tensor = data_transform(image_pil)
        cat_tensor, tensor_class, model_output = ssdl_model.predict(image_ood_fastai, with_dropout=False)
        print("Fastai evaluation output")
        print(model_output)
        print(tensor_class)
        if (tensor_class.item() != class_label):
            fails_fastai += 1
        # pytorch model
        model = ssdl_model.model
        image = torch.zeros(1, 3, im_size, im_size).cuda()
        image[0] = image_ood_tensor
        scores_all_classes = nn.functional.softmax(model(image).data.cpu(), dim=1).squeeze()
        val, index = torch.max(scores_all_classes, 0)
        print("Estimated class: ", index)
        if (index != class_label):
            fails_pytorch += 1
        print("Pytorch evaluation output")
        print(scores_all_classes)
        print(model(image).data.cpu())

    print("TOTAL FAILS FASTAI OUTPUT: ", fails_fastai)
    print("TOTAL FAILS PYTORCH OUTPUT: ", fails_pytorch)
    # Pytorch and fastai outputs are quite different
    # SSDL model can produce out of bounds (0-1) results
    """
    For instance
    Loading the dataset:


    In the CLUSTER
    Fastai evaluation output
    tensor([0.9602, 0.0398])
    Pytorch evaluation output
    tensor([0.2528, 0.7472])
    tensor([[-2.0311, -4.1816]])

    """




"""
Estimate uncertainty for a given learner and set of images
"""
def estimate_uncertainty_softmax(learner, img_path, img_names, im_size = 110):
    data_transform = transforms.Compose(
        [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
    model = learner.model
    # Variables needed to test
    # Stores the img we want to infer
    image = torch.zeros(1, 3, im_size, im_size).cuda()
    # Stores the results of everything
    number_classes = 2
    results_per_class = torch.zeros(len(img_names), number_classes)
    results_predicted_class = torch.zeros(len(img_names))
    #results for all images
    soft_max_best_class = torch.zeros(len(img_names))
    # Set to eval mode on all layers
    model.eval()

    #Infer all the dataset and store the results
    # For each image
    print("Number of images to do inference with : ", len(img_names))
    for i in range(len(img_names)):
        # Read the img
        im = Pili.open(img_path + img_names[i]).convert('RGB')
        # Apply transformations to it
        #print("Image shape")
        image[0] = data_transform(im)
        # Infer, apply softmax to the result and store it
        scores_all_classes = nn.functional.softmax(model(image).data.cpu(), dim=1).squeeze()
        #eliminate one unnecessary dimension
        #be sure that we are exploring dimension 0
        value, index = torch.max(scores_all_classes, 0)
        predicted_class = index
        results_per_class[i, :] = nn.functional.softmax(model(image).data.cpu(), dim=1)
        soft_max_best_class[i] = scores_all_classes[predicted_class]
    return results_per_class, soft_max_best_class

# Function to enable dropout in eval time
def enable_dropout(m):
  for each_module in m.modules():
    if each_module.__class__.__name__.startswith('Dropout'):
      each_module.train()


"""
Semi supervised model loading
"""
def load_ssdl_model(model_name = "CHINA_SSDL_model", path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/", path_dataset ="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/CR_DATASET/", SIZE_IMAGE = 110, BATCH_SIZE = 12, WORKERS = 8):
    model = models.densenet121
    meanDatasetComplete = [0.4823, 0.4823, 0.4823]
    stdDatasetComplete = [0.2231, 0.2231, 0.2231]
    norm_stats = (meanDatasetComplete, stdDatasetComplete)
    # create the dataset
    data_full = (ImageList.from_folder(path_dataset)
                 .split_by_folder(valid="test")
                 .label_from_folder()
                 .transform(
        get_transforms(do_flip=True, flip_vert=True, max_zoom=1, max_warp=None, p_affine=0, p_lighting=0),
        size=SIZE_IMAGE)
                 .databunch(bs=BATCH_SIZE, num_workers=WORKERS)
                 .normalize(norm_stats))


    print("Path ", path_models)
    # assuming ssdl model
    mixloss = MixupLoss()
    setattr(mixloss, 'reduction', 'none')
    learner_loaded_ssdl = cnn_learner(data=data_full, base_arch=model, loss_func=mixloss,
                                      callback_fns=[MixMatchTrainer], metrics=[accuracy])

    learner_loaded_ssdl.load(path_models + model_name)
    print("SSDL  model loaded")
    return learner_loaded_ssdl

"""
Supervised model loading
"""
def load_no_ssdl_model(path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/", path_dataset ="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/CR_DATASET/", SIZE_IMAGE = 110, BATCH_SIZE = 12, WORKERS = 8, model_name = 'model_NO_SSDL'):
    model = models.densenet121
    meanDatasetComplete = [0.4823, 0.4823, 0.4823]
    stdDatasetComplete = [0.2231, 0.2231, 0.2231]
    norm_stats = (meanDatasetComplete, stdDatasetComplete)
    #loss used in the model
    calculate_cross_entropy = nn.CrossEntropyLoss()
    # create the dataset
    data_full = (ImageList.from_folder(path_dataset)
                 .split_by_folder(valid="test")
                 .label_from_folder()
                 .transform(
        get_transforms(do_flip=True, flip_vert=True, max_zoom=1, max_warp=None, p_affine=0, p_lighting=0),
        size=SIZE_IMAGE)
                 .databunch(bs=BATCH_SIZE, num_workers=WORKERS)
                 .normalize(norm_stats))
    learner_loaded_no_ssdl = cnn_learner(data=data_full, base_arch=model, loss_func=calculate_cross_entropy,
                                         metrics=[accuracy])
    #load learner
    learner_loaded_no_ssdl.load(path_models + model_name)
    print("No SSDL model loaded!")
    return learner_loaded_no_ssdl, data_full




def test_uncertainty_softmax(learner_loaded_ssdl, learner_loaded_no_ssdl, img_path_iod, img_path_ood, class_label):

    #define uncertainty estimation parameters
    #img_names_ood = ["20588164.bmp", "22580218.bmp", "22613848.bmp", "22613996.bmp", "22670442.bmp", "22670488.bmp"]
    img_names_ood = get_file_names_in_path(img_path_ood)
    print("ood images list")
    print(img_names_ood)
    #img_names_iod = ["0_chest_ray.bmp", "10_chest_ray.bmp", "11_chest_ray.bmp", "12_chest_ray.bmp"]
    img_names_iod = get_file_names_in_path(img_path_iod)
    print("Starting uncertainty estimation for SSDL model...")
    print("iod images list ")
    print(img_names_iod)
    #one std per class
    pred,  softmaxes_ood_ssdl = estimate_uncertainty_softmax(learner_loaded_ssdl, img_path_ood, img_names_ood, im_size=110)
    pred,  softmaxes_iod_ssdl = estimate_uncertainty_softmax(learner_loaded_ssdl, img_path_iod, img_names_iod, im_size=110)
    print("Uncertainty estimation done!")
    print("Softmaxes for ood image: ", softmaxes_ood_ssdl)
    print("Softmaxes for iod image: ", softmaxes_iod_ssdl)

    print("Starting uncertainty estimation for NO SSDL model...")
    # one std per class
    pred,  softmaxes_ood_no_ssdl = estimate_uncertainty_softmax(learner_loaded_no_ssdl,  img_path_ood, img_names_ood, im_size=110)
    pred,  softmaxes_iod_no_ssdl = estimate_uncertainty_softmax(learner_loaded_no_ssdl,  img_path_iod, img_names_iod, im_size=110)
    print("Uncertainty estimation done!")
    print("softmaxes for ood image: ", softmaxes_ood_no_ssdl)
    print("softmaxes for iod image: ", softmaxes_iod_no_ssdl)

    print("Final results")
    #higher the better, std ood ssdl should  be higher than with no ssdl
    res_ood = torch.mean(softmaxes_ood_ssdl - softmaxes_ood_no_ssdl)
    res_ood_std = torch.std(softmaxes_ood_ssdl - softmaxes_ood_no_ssdl)
    #lower the better,
    res_iod = torch.mean(softmaxes_iod_ssdl - softmaxes_iod_no_ssdl)
    res_iod_std = torch.std(softmaxes_iod_ssdl - softmaxes_iod_no_ssdl)
    print("softmaxes OOD SSDL - NO SSDL, lower the better: ", res_ood, " std ", res_ood_std)
    print("softmaxes IOD SSDL - NO SSDL, higher the better: ", res_iod, " std ", res_iod_std)
    return softmaxes_iod_ssdl, softmaxes_ood_ssdl, softmaxes_iod_no_ssdl, softmaxes_ood_no_ssdl




def test_uncertainty_std_ood(learner_loaded_ssdl, learner_loaded_no_ssdl, img_path_iod, img_path_ood, class_label):

    #define uncertainty estimation parameters
    num_inferences = 100
    #img_names_ood = ["20588164.bmp", "22580218.bmp", "22613848.bmp", "22613996.bmp", "22670442.bmp", "22670488.bmp"]
    img_names_ood = get_file_names_in_path(img_path_ood)
    print("ood images list")
    print(img_names_ood)
    #img_names_iod = ["0_chest_ray.bmp", "10_chest_ray.bmp", "11_chest_ray.bmp", "12_chest_ray.bmp"]
    img_names_iod = get_file_names_in_path(img_path_iod)
    print("Starting uncertainty estimation for SSDL model...")
    print("iod images list ")
    print(img_names_iod)
    #one std per class
    pred,  std_ood_ssdl, list_correct, list_wrong = estimate_MCD_uncertainty_std_2(learner_loaded_ssdl, num_inferences, img_path_ood, img_names_ood)
    pred,  std_iod_ssdl, list_correct_ssdl, list_wrong_ssdl  = estimate_MCD_uncertainty_std_2(learner_loaded_ssdl, num_inferences, img_path_iod, img_names_iod, correct_label=class_label)
    print("Uncertainty estimation done!")
    print("Std for ood image: ", std_ood_ssdl)
    print("Std for iod image: ", std_iod_ssdl)
    print("list correct")
    print(list_correct_ssdl)
    print("list wrong")
    print(list_wrong_ssdl)

    print("Starting uncertainty estimation for NO SSDL model...")
    # one std per class
    pred,  std_ood_no_ssdl, list_correct, list_wrong  = estimate_MCD_uncertainty_std_2(learner_loaded_no_ssdl, num_inferences, img_path_ood, img_names_ood, im_size=110)
    pred,  std_iod_no_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl  = estimate_MCD_uncertainty_std_2(learner_loaded_no_ssdl, num_inferences, img_path_iod, img_names_iod, im_size=110, correct_label=class_label)
    print("Uncertainty estimation done!")
    print("Std for ood image: ", std_ood_no_ssdl)
    print("Std for iod image: ", std_iod_no_ssdl)
    print("list correct")
    print(list_correct)
    print("list wrong")
    print(list_wrong)
    print("Final results")
    #higher the better, std ood ssdl should  be higher than with no ssdl
    res_ood = torch.mean(std_ood_ssdl - std_ood_no_ssdl)
    res_ood_std = torch.std(std_ood_ssdl - std_ood_no_ssdl)
    #lower the better,
    res_iod = torch.mean(std_iod_ssdl - std_iod_no_ssdl)
    res_iod_std = torch.std(std_iod_ssdl - std_iod_no_ssdl)
    print("STD OOD SSDL - NO SSDL, higher the better: ", res_ood, " std ", res_ood_std)
    print("STD IOD SSDL - NO SSDL, lower the better: ", res_iod, " std ", res_iod_std)
    return std_iod_ssdl, std_ood_ssdl, std_iod_no_ssdl, std_ood_no_ssdl, list_correct_ssdl, list_wrong_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl



def pil2fast(img, im_size = 110):
    data_transform = transforms.Compose(
        [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
    return Image(data_transform(img))


def estimate_MCD_uncertainty_std_fastai(learner, num_inferences, img_path, img_names,  im_size=110, number_classes=2, correct_label=-1):
    std_all_images = []
    for i in range(len(img_names)):
        # Read the img
        image_pil = Pili.open(img_path + img_names[i]).convert('RGB')
        image_fastai = pil2fast(image_pil)
        get_mc_dropout_fastai
        predictions, outputs, std_best_class = get_mc_dropout_fastai(learner, image_fastai, max_its=num_inferences)
        std_all_images += [std_best_class]
    return std_all_images

"""
Softmax calculator
"""
def get_softmax_fastai(learner, input_image, is_ssdl = False):

    cat_tensor, tensor_class, model_output = learner.predict(input_image, with_dropout = False)
    if (is_ssdl):
        model_output = nn.functional.softmax(model_output, dim=0)
    val, index = torch.max(model_output, 0)
    score_predicted = model_output[index]
    return tensor_class.item(), score_predicted

def get_mc_dropout_fastai(learner, input_image, max_its=10, is_ssdl = False):
    print("Input")
    print(input_image)

    predictions = []
    outputs = []
    for i in range(max_its):
        cat_tensor, tensor_class, model_output = learner.predict(input_image, with_dropout=True)
        #print("Model output before")
        #print(model_output)
        if(is_ssdl):
            model_output = nn.functional.softmax(model_output, dim=1)
        #print("Model output after")
        #print(model_output)
        pred_class = tensor_class.item()
        outputs.append((pred_class, model_output))

        predictions.append(model_output[pred_class].item())
    std_best_class = np.std(np.array(predictions))
    return predictions, outputs, std_best_class

def get_mc_dropout_fastai_all_classes(learner, input_image, max_its=10, is_ssdl = False):

    num_classes = 2
    outputs = torch.zeros(max_its, num_classes)
    preds_class = []
    for i in range(max_its):
        cat_tensor, tensor_class, model_output = learner.predict(input_image, with_dropout=True)
        #print("Model output before")
        #print(model_output)
        if (is_ssdl):
            model_output = nn.functional.softmax(model_output, dim=0)
        #print("Model output after")
        #print(model_output)
        pred_class = tensor_class.item()
        preds_class.append(pred_class)
        #for each class
        outputs[i, :] = model_output

    std_all_classes = torch.std(outputs, 0)
    std_best_class = std_all_classes.sum()
    pred_class_mean = np.mean(np.array(preds_class))
    return outputs, pred_class_mean, std_best_class

"""
Estimate the MCD uncertainty using the std 
"""
def estimate_MCD_uncertainty_std_2(learner, num_inferences, img_path, img_names,  im_size=110, number_classes=2, correct_label=-1):
    # Fetch the images
    #img_names = get_file_names_in_path(img_path)
    data_transform = transforms.Compose(
        [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
    model = learner.model
    # Variables needed to test
    # Stores the img we want to infer
    print("im size ", im_size)
    image = torch.zeros(1, 3, im_size, im_size).cuda()
    # Stores the results of everything

    results_per_class = torch.zeros(num_inferences, len(img_names), number_classes)
    results_predicted_class = torch.zeros(num_inferences, len(img_names))
    # results for all images
    std_preds_best_class = torch.zeros(len(img_names))
    # store uncerainty estimations for wrong and correct predictions
    stds_wrong_preds = []
    stds_correct_preds = []
    # Set to eval mode on all layers
    model.eval()
    # Enable dropout layers
    enable_dropout(model)
    # Infer all the dataset and store the results
    # For each image
    print("Number of images to do inference with : ", len(img_names))
    predicted_class_all_images = []
    for i in range(len(img_names)):
        # Read the img
        im = Pili.open(img_path + img_names[i]).convert('RGB')
        # Apply transformations to it
        # print("Image shape")
        image[0] = data_transform(im)

        # Run inference <num_inferences> times
        print("Making ", num_inferences, " inferences...")
        predicted_class_all_inferences = []
        for it in range(num_inferences):
            # Net scores for all classes
            scores_all_classes = model(image).data.cpu().squeeze()

            # be sure that we are exploring dimension 0
            value, index = torch.max(scores_all_classes, 0)
            predicted_class = index
            # accumulate to do class estimation
            predicted_class_all_inferences += [predicted_class]
            results_per_class[it, i, :] = model(image).data.cpu()
            results_predicted_class[it, i] = scores_all_classes[predicted_class]
        # get mean predicted class, rounding it
        mean_predicted_class_image = torch.round(
            torch.mean(torch.tensor(predicted_class_all_inferences, dtype=torch.float)))
        # store predicted class per image
        predicted_class_all_images += [mean_predicted_class_image]

    '''     Calc stats of the inference     '''
    # Calc of the previous runs
    for i in range(len(img_names)):
        # Calculates std
        std_preds_best_class[i] = torch.std(results_predicted_class[:, i], dim=0)
        # keep uncertainties for correct and wrong predictions
        if (correct_label != -1 and correct_label == predicted_class_all_images[i]):
            stds_correct_preds += [std_preds_best_class[i]]
        elif (correct_label != -1 and correct_label != predicted_class_all_images[i]):
            stds_wrong_preds += [std_preds_best_class[i]]
    return results_per_class, std_preds_best_class, stds_correct_preds, stds_wrong_preds

"""
Normalize the entropy if needed
"""
def normalize_entropy(p):
    entropy = 0
    for p_i in p:
        entropy -= p_i * log(p_i)
    print("Entropy ", entropy)
    num_buckets = len(p)
    p_norm = [1 / num_buckets] * num_buckets
    norm_coef = 0
    for p_norm_i in p_norm:
        norm_coef -= p_norm_i * log(p_norm_i)
    normalized_entropy = entropy / norm_coef
    confidence = 1 - normalized_entropy
    return confidence, normalized_entropy


def test_mcd_ood_batch(batch_number = 1):
    # train_covid_model(NUMBER_EPOCHS = 1)
    print("Pytorch current device")
    print(torch.cuda.current_device())
    class_label_iod_test_data = 0
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    #img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(class_label_iod_test_data) + "/"
    img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0/"
    #img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0c/"
    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/"

    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_"
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_"
    learner_no_ssdl = load_no_ssdl_model(path_models = path_models, path_dataset = path_labeled, model_name = model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models = path_models, path_dataset = path_labeled, model_name = model_name_ssdl)

    stds_iod_ssdl, stds_ood_ssdl, stds_iod_no_ssdl, stds_ood_no_ssdl, list_correct_ssdl, list_wrong_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl = test_uncertainty_std_ood(learner_loaded_no_ssdl=learner_no_ssdl, learner_loaded_ssdl=learner_ssdl, img_path_iod=img_path_iod, img_path_ood=img_path_ood, class_label=class_label_iod_test_data)

    #softmaxes_iod_ssdl, softmaxes_ood_ssdl, softmaxes_iod_no_ssdl, softmaxes_ood_no_ssdl = test_uncertainty_softmax(learner_loaded_no_ssdl=learner_no_ssdl, learner_loaded_ssdl= learner_ssdl,  img_path_iod=img_path_iod, img_path_ood= img_path_ood, class_label=class_label_iod_test_data)
    #summary_name = "summary_softmax_china_batch_" + str(batch_number)
    summary_name = "summary_std_china_batch_" + str(batch_number)
    create_summaries_csv(summary_name, stds_iod_ssdl, stds_ood_ssdl, stds_iod_no_ssdl, stds_ood_no_ssdl, list_correct_ssdl, list_wrong_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl)


"""
Four tensors are received
"""
def create_summaries_csv(summary_name, softmaxes_iod_ssdl, softmaxes_ood_ssdl, softmaxes_iod_no_ssdl, softmaxes_ood_no_ssdl, list_correct_ssdl, list_wrong_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl):
    list_iod_ssdl = concat_mean_std_to_list(softmaxes_iod_ssdl)
    list_ood_ssdl = concat_mean_std_to_list(softmaxes_ood_ssdl)
    list_iod_no_ssdl =concat_mean_std_to_list(softmaxes_iod_no_ssdl)
    list_ood_no_ssdl = concat_mean_std_to_list(softmaxes_ood_no_ssdl)
    #concat stats for correct and wrong lists

    list_correct_ssdl = ["Correct"] + concat_mean_std_to_list(torch.tensor(list_correct_ssdl))
    list_wrong_ssdl = ["Incorrect"] + concat_mean_std_to_list(torch.tensor(list_wrong_ssdl))
    list_correct_no_ssdl = ["Correct"] + concat_mean_std_to_list(torch.tensor(list_correct_no_ssdl))
    list_wrong_no_ssdl = ["Incorrect"] + concat_mean_std_to_list(torch.tensor(list_wrong_no_ssdl))


    #iod data results
    stat, p_value_iod = scipy.stats.wilcoxon(list_iod_ssdl, list_iod_no_ssdl, correction=True)
    list_iod_ssdl += [p_value_iod]
    list_iod_no_ssdl += [p_value_iod]
    #create dataframe for IOD
    print("P value IOD ", p_value_iod)
    data_dictionary_iod = {'Softmaxes_iod_ssdl': list_iod_ssdl, 'Softmaxes_iod_no_ssdl': list_iod_no_ssdl}

    dataframe_iod = pd.DataFrame(data_dictionary_iod, columns=["Softmaxes_iod_ssdl", "Softmaxes_iod_no_ssdl"])
    dataframe_iod.to_csv(summary_name + "_iod.csv", index=False)
    #ood data results

    print(list_ood_ssdl)
    print(list_ood_no_ssdl)
    stat, p_value_ood = scipy.stats.wilcoxon(list_ood_ssdl, list_ood_no_ssdl, correction=True)
    list_ood_ssdl += [p_value_ood]
    list_ood_no_ssdl += [p_value_ood]
    print("P value OOD ", p_value_ood)

    data_dictionary_ood = {'Softmaxes_ood_ssdl': list_ood_ssdl, 'Softmaxes_ood_no_ssdl': list_ood_no_ssdl}
    dataframe_ood = pd.DataFrame(data_dictionary_ood, columns=["Softmaxes_ood_ssdl", "Softmaxes_ood_no_ssdl"])
    dataframe_ood.to_csv(summary_name + "_ood.csv", index=False)
    print("Summaries written!")
    items = [list_correct_ssdl, list_wrong_ssdl]
    df_ssdl = pd.DataFrame(items)
    df_ssdl.to_csv(summary_name + "ssdl_uncertainty_wrong_correct.csv", index=False )
    items = [list_correct_no_ssdl, list_wrong_no_ssdl]
    df_no_ssdl = pd.DataFrame(items)
    df_no_ssdl.to_csv(summary_name + "no_ssdl_uncertainty_wrong_correct.csv", index=False)
    print("Wrong/correct summaries written!")



def create_summaries_csv_right_wrong(summary_name, list_correct_ssdl, list_wrong_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl):

    #concat stats for correct and wrong lists
    list_correct_ssdl = ["Correct"] + concat_mean_std_to_list(torch.tensor(list_correct_ssdl))
    list_wrong_ssdl = ["Incorrect"] + concat_mean_std_to_list(torch.tensor(list_wrong_ssdl))
    list_correct_no_ssdl = ["Correct"] + concat_mean_std_to_list(torch.tensor(list_correct_no_ssdl))
    list_wrong_no_ssdl = ["Incorrect"] + concat_mean_std_to_list(torch.tensor(list_wrong_no_ssdl))

    items = [list_correct_ssdl, list_wrong_ssdl]
    df_ssdl = pd.DataFrame(items)
    df_ssdl.to_csv(summary_name + "ssdl_uncertainty_wrong_correct.csv", index=False )
    items = [list_correct_no_ssdl, list_wrong_no_ssdl]
    df_no_ssdl = pd.DataFrame(items)
    df_no_ssdl.to_csv(summary_name + "no_ssdl_uncertainty_wrong_correct.csv", index=False)
    print("Wrong/correct summaries written!")


def concat_mean_std_to_list(torch_tensor):
    list_tensor = torch_tensor.numpy().tolist() + [torch.mean(torch_tensor).item()] + [torch.std(torch_tensor).item()]

    return list_tensor




def test_mcd_fastai_simple():
    no_ssdl_model, data_full = load_no_ssdl_model(path_models="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/",
                       path_dataset="E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1", SIZE_IMAGE=110, BATCH_SIZE=12,
                       WORKERS=8, model_name='CHINA_NO_SSDL_model_batch_1_')

    ssdl_model = load_ssdl_model(path_models="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/",
                                                  path_dataset="E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1",
                                                  SIZE_IMAGE=110, BATCH_SIZE=12,
                                                  WORKERS=8, model_name='CHINA_SSDL_model_batch_1_')
    print("validation data")
    print(data_full.valid_ds)

    img = data_full.valid_ds[0][0]
    print("img")
    print(img)
    num_iters = 1000
    #for a binary classifier, std is the same for both classes
    predictions, outputs, std_best_class = get_mc_dropout_fastai_all_classes(no_ssdl_model, img, max_its=num_iters, is_ssdl=False)
    print("IOD NO SSDL")
    print(std_best_class)

    img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0/20588164.bmp"
    im_ood_pil = Pili.open(img_path_ood).convert('RGB')
    image_ood_fastai = pil2fast(im_ood_pil)
    predictions, outputs, std_best_class = get_mc_dropout_fastai_all_classes(no_ssdl_model, image_ood_fastai, max_its=num_iters, is_ssdl=False)
    print("OOD NO SSDL")
    print(std_best_class)
    print("SSDL MODEL---------------------------")
    predictions, outputs, std_best_class = get_mc_dropout_fastai_all_classes(ssdl_model, img, max_its=num_iters, is_ssdl=True)
    print("IOD SSDL:")
    print(std_best_class)
    predictions, outputs, std_best_class = get_mc_dropout_fastai_all_classes(ssdl_model, image_ood_fastai,
                                                                             max_its=num_iters, is_ssdl=True)
    print("OOD  SSDL")
    print(std_best_class)

#get_softmax_fastai(learner, input_image, is_ssdl = False, num_classes = 2)

def calculate_uncertainty_softmax_fastai_images(fastai_model, img_path, is_ssdl = False, class_label = 0, im_size = 110):
    uncertainties_correct = []
    uncertainties_wrong = []
    softmaxes_all_imgs = []
    pred_class_all_imgs = []
    list_images = get_file_names_in_path(img_path)
    print("total of images ", len(list_images))
    for i in range(0, len(list_images)):
        complete_path = img_path + list_images[i]
        image_pil = Pili.open(complete_path).convert('RGB')
        image_fastai = pil2fast(image_pil, im_size =im_size)
        pred_class, softmax_out = get_softmax_fastai(fastai_model, image_fastai, is_ssdl = is_ssdl)
        #use output with no dropout for pred class
        cat_tensor, tensor_class, model_output = fastai_model.predict(image_fastai, with_dropout=False)
        softmaxes_all_imgs += [softmax_out.item()]
        if(tensor_class.item() != class_label):
            uncertainties_wrong += [softmax_out]
        else:
            uncertainties_correct += [softmax_out.item()]



    return torch.tensor(softmaxes_all_imgs), uncertainties_correct, uncertainties_wrong


def calculate_uncertainty_mcd_fastai_images(fastai_model, img_path, num_iters = 100, is_ssdl = False, class_label = 0, im_size = 110):
    uncertainties_correct = []
    uncertainties_wrong = []
    stds_all_imgs = []
    pred_class_all_imgs = []
    list_images = get_file_names_in_path(img_path)
    print("total of images ", len(list_images))

    for i in range(0, len(list_images)):

        complete_path = img_path + list_images[i]
        image_pil = Pili.open(complete_path).convert('RGB')
        image_fastai = pil2fast(image_pil, im_size =im_size)
        outputs, pred_class, stds_sums_all = get_mc_dropout_fastai_all_classes(fastai_model, image_fastai, max_its=num_iters, is_ssdl=is_ssdl)
        #use output with no dropout for pred class
        cat_tensor, tensor_class, model_output = fastai_model.predict(image_fastai, with_dropout=False)


        stds_all_imgs += [stds_sums_all]
        if(tensor_class.item() != class_label):
            uncertainties_wrong += [stds_sums_all]
        else:
            uncertainties_correct += [stds_sums_all]

    return torch.tensor(stds_all_imgs), uncertainties_correct, uncertainties_wrong







def undersample_dataset( items_per_class, desired_proportions, num_items):
    #overrepresented class
    min_num_obs_class = int(num_items * min(desired_proportions))
    all_items = []
    #for each class, define the number of observations to oversample
    #all the classes must have the same number of observations, from the under represented class
    for i in range(0, len(desired_proportions)):
        # pick the items as usual for the class
        num_items_class_i = min_num_obs_class
        items_class_i = items_per_class[i]

        keep_idxs_i = np.random.choice(np.array(items_class_i), num_items_class_i, replace=False)
        all_items += keep_idxs_i.tolist()
        print("Selected items for class ", i )
        print(keep_idxs_i)
        print("Number of items: ")
        print(keep_idxs_i.shape)

    return all_items


def oversample_dataset( items_per_class, desired_proportions, num_items):
    #overrepresented class
    max_num_obs_class = int(num_items * max(desired_proportions))
    all_items = []
    #for each class, define the number of observations to oversample
    for i in range(0, len(desired_proportions)):
        # pick the items as usual for the class
        num_items_class_i = int(desired_proportions[i] * num_items)
        items_class_i = items_per_class[i]


        keep_idxs_i = np.random.choice(np.array(items_class_i), num_items_class_i, replace=False)
        #oversample
        num_items_to_oversample = max_num_obs_class - num_items_class_i

        all_items += keep_idxs_i.tolist()
        print("Selected items for class ", i)
        print(keep_idxs_i)
        print("Number of items: ")
        print(keep_idxs_i.shape)
        if(num_items_to_oversample > 0):
            keep_idxs_i_oversampled = np.random.choice(keep_idxs_i, num_items_to_oversample, replace=True)
            print("oversampled observations")
            print(keep_idxs_i_oversampled)
            all_items += keep_idxs_i_oversampled.tolist()

    return all_items

def test_oversample():
    items_per_class = [[60, 61, 62, 63, 64, 65, 66, 67, 68],
     [129, 130, 131, 132, 133, 134, 135, 136]]
    desired_proportions = [0.8, 0.2]
    num_items = 10
    all_items = oversample_dataset(items_per_class, desired_proportions, num_items)
    print("all items oversampled")
    print(all_items)

    all_items = undersample_dataset(items_per_class, desired_proportions, num_items)
    print("all items undersampled")
    print(all_items)


def test_softmax_fastai_iod_ood(batch_number = 1):
    # train_covid_model(NUMBER_EPOCHS = 1)
    print("Pytorch current device")
    print(torch.cuda.current_device())
    class_label_iod_test_data = 0
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    #img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(class_label_iod_test_data) + "/"
    img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0/"
    #img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0c/"
    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/"

    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_"
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_"
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models = path_models, path_dataset = path_labeled, model_name = model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models = path_models, path_dataset = path_labeled, model_name = model_name_ssdl)

    #no ssdl model
    softmaxes_no_ssdl_iod, uncertainties_correct_no_ssdl_iod, uncertainties_wrong_no_ssdl_iod = calculate_uncertainty_softmax_fastai_images(learner_no_ssdl, img_path_iod,  is_ssdl=False, class_label=class_label_iod_test_data, im_size=110)
    softmaxes_no_ssdl_ood, _, _ = calculate_uncertainty_softmax_fastai_images(learner_no_ssdl, img_path_ood, is_ssdl=False, im_size=110)
    #ssdl model
    softmaxes_ssdl_iod, uncertainties_correct_ssdl_iod, uncertainties_wrong_ssdl_iod = calculate_uncertainty_softmax_fastai_images(learner_ssdl, img_path_iod, is_ssdl=True, class_label=class_label_iod_test_data, im_size=110)
    softmaxes_ssdl_ood, _, _ = calculate_uncertainty_softmax_fastai_images(learner_ssdl, img_path_ood, is_ssdl=True, im_size=110)


    summary_name = "summary_std_china_batch_" + str(batch_number)
    create_summaries_csv(summary_name, softmaxes_ssdl_iod, softmaxes_ssdl_ood, softmaxes_no_ssdl_iod, softmaxes_no_ssdl_ood, list_correct_ssdl = uncertainties_correct_ssdl_iod, list_wrong_ssdl = uncertainties_wrong_ssdl_iod, list_correct_no_ssdl = uncertainties_correct_no_ssdl_iod, list_wrong_no_ssdl = uncertainties_wrong_no_ssdl_iod)


def test_mcd_fastai_iod_ood(batch_number = 1):
    # train_covid_model(NUMBER_EPOCHS = 1)
    print("Pytorch current device")
    print(torch.cuda.current_device())
    class_label_iod_test_data = 0
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    #img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(class_label_iod_test_data) + "/"
    img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0/"
    #img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0c/"
    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/"

    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_"
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_"
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models = path_models, path_dataset = path_labeled, model_name = model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models = path_models, path_dataset = path_labeled, model_name = model_name_ssdl)

    #no ssdl model
    stds_sums_no_ssdl_iod, uncertainties_correct_no_ssdl_iod, uncertainties_wrong_no_ssdl_iod = calculate_uncertainty_mcd_fastai_images(learner_no_ssdl, img_path_iod, num_iters=100, is_ssdl=False, class_label=class_label_iod_test_data, im_size=110)
    stds_sums_no_ssdl_ood, _, _ = calculate_uncertainty_mcd_fastai_images(learner_no_ssdl, img_path_ood, num_iters=100, is_ssdl=False, im_size=110)
    #ssdl model
    stds_sums_ssdl_iod, uncertainties_correct_ssdl_iod, uncertainties_wrong_ssdl_iod = calculate_uncertainty_mcd_fastai_images(learner_ssdl, img_path_iod, num_iters=100,
                                                                    is_ssdl=True,
                                                                    class_label=class_label_iod_test_data, im_size=110)
    stds_sums_ssdl_ood, _, _ = calculate_uncertainty_mcd_fastai_images(learner_ssdl, img_path_ood, num_iters=100,
                                                                    is_ssdl=True, im_size=110)


    summary_name = "summary_std_china_batch_" + str(batch_number)
    create_summaries_csv(summary_name, stds_sums_ssdl_iod, stds_sums_ssdl_ood, stds_sums_no_ssdl_iod, stds_sums_no_ssdl_ood, list_correct_ssdl = uncertainties_correct_ssdl_iod, list_wrong_ssdl = uncertainties_wrong_ssdl_iod, list_correct_no_ssdl = uncertainties_correct_no_ssdl_iod, list_wrong_no_ssdl = uncertainties_wrong_no_ssdl_iod)

def measure_model_accuracy_test(learner, img_path, class_label, im_size = 110):
    list_images = get_file_names_in_path(img_path)
    print("total of images ", len(list_images))
    num_test = len(list_images)
    correct_preds = 0
    wrong_preds = 0
    for i in range(0, num_test):
        complete_path = img_path + list_images[i]
        image_pil = Pili.open(complete_path).convert('RGB')
        image_fastai = pil2fast(image_pil, im_size=im_size)
        cat_tensor, tensor_class, model_output = learner.predict(image_fastai, with_dropout=False)
        if(tensor_class.item() == class_label):
            correct_preds += 1
        else:
            wrong_preds += 1

    accuracy = correct_preds/num_test
    return accuracy, correct_preds, wrong_preds

def test_model_accuracy(batch_number = 1, class_label_iod_test_data = 0):

    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    # img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"

    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/"
    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_"
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_"
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models=path_models, path_dataset=path_labeled,
                                                    model_name=model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models=path_models, path_dataset=path_labeled, model_name=model_name_ssdl)
    acc_no_ssdl, correct_preds, wrong_preds = measure_model_accuracy_test(learner_no_ssdl, img_path_iod, class_label = class_label_iod_test_data, im_size = 110)
    print("Accuracy NO SSDL ", acc_no_ssdl, " correct: ", correct_preds, " wrong: ", wrong_preds, " for class: ", class_label_iod_test_data)
    acc_ssdl, correct_preds, wrong_preds = measure_model_accuracy_test(learner_ssdl, img_path_iod, class_label=class_label_iod_test_data,
                                              im_size=110)
    print("Accuracy  SSDL ", acc_ssdl, " correct: ", correct_preds, " wrong: ", wrong_preds,  " for class: ", class_label_iod_test_data)

def get_precision_recall_f1_score(fastai_model, test_image_path_c0, test_image_path_c1, im_size = 110):
    #0 is normal or no pathology
    acc_c0, correct_preds_c0, wrong_preds_c0 = measure_model_accuracy_test(fastai_model, test_image_path_c0,
                                                                          class_label=0,
                                                                          im_size=im_size)
    print("get_precision_recall_f1_score correct preds c0 ",  correct_preds_c0, " wrong preds c0 ", wrong_preds_c0)
    # 1 is covid-19 positive
    acc_c1, correct_preds_c1, wrong_preds_c1 = measure_model_accuracy_test(fastai_model, test_image_path_c1,
                                                                           class_label=1,
                                                                           im_size=im_size)
    print("get_precision_recall_f1_score correct preds c1 ", correct_preds_c1, " wrong preds c1 ", wrong_preds_c1)
    true_positives = correct_preds_c1
    false_positives = wrong_preds_c0
    false_negatives = wrong_preds_c1
    recall = true_positives / (true_positives + false_negatives)
    precision = true_positives / (true_positives + false_positives)
    f1_score = (2 * recall * precision) / (precision + recall)
    return f1_score, recall, precision


def test_model_f1_score(batch_number = 1):
    class_label_iod_test_data = 0
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    # img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod_c0 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"
    class_label_iod_test_data = 1
    img_path_iod_c1 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"

    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/"
    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_"
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_"
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models=path_models, path_dataset=path_labeled,
                                                    model_name=model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models=path_models, path_dataset=path_labeled, model_name=model_name_ssdl)
    f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl = get_precision_recall_f1_score(learner_no_ssdl, img_path_iod_c0, img_path_iod_c1, im_size=110)
    f1_score_ssdl, recall_ssdl, precision_ssdl = get_precision_recall_f1_score(learner_ssdl,
                                                                                        img_path_iod_c0,
                                                                                        img_path_iod_c1, im_size=110)
    print("SSDL f1 score: ", f1_score_ssdl, " recall: ", recall_ssdl, " precision: ", precision_ssdl)
    print("No SSDL f1 score: ", f1_score_no_ssdl, " recall: ", recall_no_ssdl, " precision: ", precision_no_ssdl)


def test_model_f1_score_uncertainty_mcd(batch_number = 1, folder_models = "models_2", id_model = ""):
    class_label_iod_test_data = 0
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    # img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod_c0 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"
    class_label_iod_test_data = 1
    img_path_iod_c1 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"

    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/" + folder_models + "/"
    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_"+  id_model
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_"+  id_model
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models=path_models, path_dataset=path_labeled,
                                                    model_name=model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models=path_models, path_dataset=path_labeled, model_name=model_name_ssdl)
    f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl = get_precision_recall_f1_score(learner_no_ssdl, img_path_iod_c0, img_path_iod_c1, im_size=110)
    f1_score_ssdl, recall_ssdl, precision_ssdl = get_precision_recall_f1_score(learner_ssdl,
                                                                                        img_path_iod_c0,
                                                                                        img_path_iod_c1, im_size=110)
    summary_name = "F1_SCORE_SUMMARY_BATCH_SSDL.csv"
    print("SSDL f1 score: ", f1_score_ssdl, " recall: ", recall_ssdl, " precision: ", precision_ssdl)
    create_classification_metrics_summary(summary_name, f1_score_ssdl, recall_ssdl, precision_ssdl)
    print("No SSDL f1 score: ", f1_score_no_ssdl, " recall: ", recall_no_ssdl, " precision: ", precision_no_ssdl)
    summary_name = "F1_SCORE_SUMMARY_BATCH_NO_SSDL.csv"
    create_classification_metrics_summary(summary_name, f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl)
    #Uncertainty estimation
    #NO SSDL MODEL
    #Class 0
    class_label_iod_test_data = 0
    _, uncertainties_correct_no_ssdl_iod_c0, uncertainties_wrong_no_ssdl_iod_c0 = calculate_uncertainty_mcd_fastai_images(
        learner_no_ssdl, img_path_iod_c0, is_ssdl=False, class_label=class_label_iod_test_data, im_size=110)

    #Class 1
    class_label_iod_test_data = 1
    _, uncertainties_correct_no_ssdl_iod_c1, uncertainties_wrong_no_ssdl_iod_c1 = calculate_uncertainty_mcd_fastai_images(
        learner_no_ssdl, img_path_iod_c1, is_ssdl=False, class_label=class_label_iod_test_data, im_size=110)

    # SSDL MODEL
    # Class 0
    class_label_iod_test_data = 0
    _, uncertainties_correct_ssdl_iod_c0, uncertainties_wrong_ssdl_iod_c0 = calculate_uncertainty_mcd_fastai_images(
        learner_ssdl, img_path_iod_c0, is_ssdl=True, class_label=class_label_iod_test_data, im_size=110)

    # Class 1
    class_label_iod_test_data = 1
    _, uncertainties_correct_ssdl_iod_c1, uncertainties_wrong_ssdl_iod_c1 = calculate_uncertainty_mcd_fastai_images(
        learner_ssdl, img_path_iod_c1, is_ssdl=True, class_label=class_label_iod_test_data, im_size=110)

    summary_name = "CHINA_BATCH_MCD_" + str(batch_number)
    print("Type ", type(uncertainties_correct_ssdl_iod_c0))
    create_summaries_csv_right_wrong(summary_name,
                         list_correct_ssdl = uncertainties_correct_ssdl_iod_c0 + uncertainties_correct_ssdl_iod_c1, list_wrong_ssdl = uncertainties_wrong_ssdl_iod_c0 + uncertainties_wrong_ssdl_iod_c1,
                         list_correct_no_ssdl = uncertainties_correct_no_ssdl_iod_c0 + uncertainties_correct_no_ssdl_iod_c1, list_wrong_no_ssdl = uncertainties_wrong_no_ssdl_iod_c0 + uncertainties_wrong_no_ssdl_iod_c1)



def test_model_f1_score_uncertainty_softmax(batch_number = 1, folder_models = "models_2", id_model = ""):
    class_label_iod_test_data = 0
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    # img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod_c0 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"
    class_label_iod_test_data = 1
    img_path_iod_c1 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"

    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/" +  folder_models + "/"
    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_" + id_model
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_" + id_model
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models=path_models, path_dataset=path_labeled,
                                                    model_name=model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models=path_models, path_dataset=path_labeled, model_name=model_name_ssdl)
    f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl = get_precision_recall_f1_score(learner_no_ssdl, img_path_iod_c0, img_path_iod_c1, im_size=110)
    f1_score_ssdl, recall_ssdl, precision_ssdl = get_precision_recall_f1_score(learner_ssdl,
                                                                                        img_path_iod_c0,
                                                                                        img_path_iod_c1, im_size=110)

    summary_name = "F1_SCORE_SUMMARY_BATCH_SSDL.csv"
    print("SSDL f1 score: ", f1_score_ssdl, " recall: ", recall_ssdl, " precision: ", precision_ssdl)
    create_classification_metrics_summary(summary_name, f1_score_ssdl, recall_ssdl, precision_ssdl)
    print("No SSDL f1 score: ", f1_score_no_ssdl, " recall: ", recall_no_ssdl, " precision: ", precision_no_ssdl)
    summary_name = "F1_SCORE_SUMMARY_BATCH_NO_SSDL.csv"
    create_classification_metrics_summary(summary_name, f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl)
    #Uncertainty estimation
    #NO SSDL MODEL
    #Class 0
    class_label_iod_test_data = 0
    _, uncertainties_correct_no_ssdl_iod_c0, uncertainties_wrong_no_ssdl_iod_c0 = calculate_uncertainty_softmax_fastai_images(
        learner_no_ssdl, img_path_iod_c0, is_ssdl=False, class_label=class_label_iod_test_data, im_size=110)

    #Class 1
    class_label_iod_test_data = 1
    _, uncertainties_correct_no_ssdl_iod_c1, uncertainties_wrong_no_ssdl_iod_c1 = calculate_uncertainty_softmax_fastai_images(
        learner_no_ssdl, img_path_iod_c1, is_ssdl=False, class_label=class_label_iod_test_data, im_size=110)

    # SSDL MODEL
    # Class 0
    class_label_iod_test_data = 0
    _, uncertainties_correct_ssdl_iod_c0, uncertainties_wrong_ssdl_iod_c0 = calculate_uncertainty_softmax_fastai_images(
        learner_ssdl, img_path_iod_c0, is_ssdl=True, class_label=class_label_iod_test_data, im_size=110)

    # Class 1
    class_label_iod_test_data = 1
    _, uncertainties_correct_ssdl_iod_c1, uncertainties_wrong_ssdl_iod_c1 = calculate_uncertainty_softmax_fastai_images(
        learner_ssdl, img_path_iod_c1, is_ssdl=True, class_label=class_label_iod_test_data, im_size=110)

    summary_name = "CHINA_BATCH_" + str(batch_number)
    print("Type ", type(uncertainties_correct_ssdl_iod_c0))
    create_summaries_csv_right_wrong(summary_name,
                         list_correct_ssdl = uncertainties_correct_ssdl_iod_c0 + uncertainties_correct_ssdl_iod_c1, list_wrong_ssdl = uncertainties_wrong_ssdl_iod_c0 + uncertainties_wrong_ssdl_iod_c1,
                         list_correct_no_ssdl = uncertainties_correct_no_ssdl_iod_c0 + uncertainties_correct_no_ssdl_iod_c1, list_wrong_no_ssdl = uncertainties_wrong_no_ssdl_iod_c0 + uncertainties_wrong_no_ssdl_iod_c1)

def create_classification_metrics_summary(summary_name, f1_score_ssdl, recall_ssdl, precision_ssdl):
    items = [f1_score_ssdl, recall_ssdl, precision_ssdl]
    df = pd.DataFrame(items)
    df.to_csv(summary_name, index=False)


def test_model_f1_score_uncertainty_duq(batch_number = 1, folder_models = "models_2", id_model = ""):
    class_label_iod_test_data = 0
    training_path_batch_centroids_base = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/train/"
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    # img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod_c0 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"
    class_label_iod_test_data = 1
    img_path_iod_c1 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"

    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/" +  folder_models + "/"
    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_" + id_model
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_" + id_model
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models=path_models, path_dataset=path_labeled,
                                                    model_name=model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models=path_models, path_dataset=path_labeled, model_name=model_name_ssdl)
    f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl = get_precision_recall_f1_score(learner_no_ssdl, img_path_iod_c0, img_path_iod_c1, im_size=110)
    f1_score_ssdl, recall_ssdl, precision_ssdl = get_precision_recall_f1_score(learner_ssdl,
                                                                                        img_path_iod_c0,
                                                                                        img_path_iod_c1, im_size=110)

    summary_name = "F1_SCORE_SUMMARY_BATCH_SSDL.csv"
    print("SSDL f1 score: ", f1_score_ssdl, " recall: ", recall_ssdl, " precision: ", precision_ssdl)
    create_classification_metrics_summary(summary_name, f1_score_ssdl, recall_ssdl, precision_ssdl)
    print("No SSDL f1 score: ", f1_score_no_ssdl, " recall: ", recall_no_ssdl, " precision: ", precision_no_ssdl)
    summary_name = "F1_SCORE_SUMMARY_BATCH_NO_SSDL.csv"
    create_classification_metrics_summary(summary_name, f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl)
    #Uncertainty estimation
    #NO SSDL MODEL
    #Class 0
    class_label_iod_test_data = 0
    _, uncertainties_correct_no_ssdl_iod_c0, uncertainties_wrong_no_ssdl_iod_c0 = duq.calculate_uncertainty_DUQ_images(learner_no_ssdl, img_path_iod_c0, training_path_batch_centroids_base, class_label = class_label_iod_test_data, im_size = 110)

    #Class 1
    class_label_iod_test_data = 1
    _, uncertainties_correct_no_ssdl_iod_c1, uncertainties_wrong_no_ssdl_iod_c1 = duq.calculate_uncertainty_DUQ_images(learner_no_ssdl, img_path_iod_c1, training_path_batch_centroids_base, class_label = class_label_iod_test_data, im_size = 110)

    # SSDL MODEL
    # Class 0
    class_label_iod_test_data = 0
    _, uncertainties_correct_ssdl_iod_c0, uncertainties_wrong_ssdl_iod_c0 = duq.calculate_uncertainty_DUQ_images(learner_ssdl, img_path_iod_c0, training_path_batch_centroids_base, class_label = class_label_iod_test_data, im_size = 110)

    # Class 1
    class_label_iod_test_data = 1
    _, uncertainties_correct_ssdl_iod_c1, uncertainties_wrong_ssdl_iod_c1 = duq.calculate_uncertainty_DUQ_images(learner_ssdl, img_path_iod_c1, training_path_batch_centroids_base, class_label = class_label_iod_test_data, im_size = 110)

    summary_name = "CHINA_BATCH_DUQ" + str(batch_number)
    print("Type ", type(uncertainties_correct_ssdl_iod_c0))
    create_summaries_csv_right_wrong(summary_name,
                         list_correct_ssdl = uncertainties_correct_ssdl_iod_c0 + uncertainties_correct_ssdl_iod_c1, list_wrong_ssdl = uncertainties_wrong_ssdl_iod_c0 + uncertainties_wrong_ssdl_iod_c1,
                         list_correct_no_ssdl = uncertainties_correct_no_ssdl_iod_c0 + uncertainties_correct_no_ssdl_iod_c1, list_wrong_no_ssdl = uncertainties_wrong_no_ssdl_iod_c0 + uncertainties_wrong_no_ssdl_iod_c1)


def test_model_f1_score_uncertainty_duq_ssdl(batch_number = 1, folder_models = "models_2", id_model = ""):
    class_label_iod_test_data = 0
    training_path_batch_centroids_base = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/train/"
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    # img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod_c0 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"
    class_label_iod_test_data = 1
    img_path_iod_c1 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"

    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/" +  folder_models + "/"
    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_" + id_model
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_" + id_model
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models=path_models, path_dataset=path_labeled,
                                                    model_name=model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models=path_models, path_dataset=path_labeled, model_name=model_name_ssdl)
    f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl = get_precision_recall_f1_score(learner_no_ssdl, img_path_iod_c0, img_path_iod_c1, im_size=110)
    f1_score_ssdl, recall_ssdl, precision_ssdl = get_precision_recall_f1_score(learner_ssdl,
                                                                                        img_path_iod_c0,
                                                                                        img_path_iod_c1, im_size=110)

    summary_name = "F1_SCORE_SUMMARY_BATCH_SSDL.csv"
    print("SSDL f1 score: ", f1_score_ssdl, " recall: ", recall_ssdl, " precision: ", precision_ssdl)
    create_classification_metrics_summary(summary_name, f1_score_ssdl, recall_ssdl, precision_ssdl)
    print("No SSDL f1 score: ", f1_score_no_ssdl, " recall: ", recall_no_ssdl, " precision: ", precision_no_ssdl)
    summary_name = "F1_SCORE_SUMMARY_BATCH_NO_SSDL.csv"
    create_classification_metrics_summary(summary_name, f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl)
    #Uncertainty estimation

    # SSDL MODEL
    # Class 0
    class_label_iod_test_data = 0
    _, uncertainties_correct_ssdl_iod_c0, uncertainties_wrong_ssdl_iod_c0 = duq.calculate_uncertainty_DUQ_images(learner_ssdl, img_path_iod_c0, training_path_batch_centroids_base, class_label = class_label_iod_test_data, im_size = 110)

    # Class 1
    class_label_iod_test_data = 1
    _, uncertainties_correct_ssdl_iod_c1, uncertainties_wrong_ssdl_iod_c1 = duq.calculate_uncertainty_DUQ_images(learner_ssdl, img_path_iod_c1, training_path_batch_centroids_base, class_label = class_label_iod_test_data, im_size = 110)

    print("UNCERTAINTIES CORRECT SSDL c1")
    print(uncertainties_correct_ssdl_iod_c1)
    print("UNCERTAINTIES INCORRECT SSDL c1")
    print(uncertainties_wrong_ssdl_iod_c1)
    print("UNCERTAINTIES CORRECT SSDL c0")
    print(uncertainties_correct_ssdl_iod_c0)
    print("UNCERTAINTIES INCORRECT SSDL c0")
    print(uncertainties_wrong_ssdl_iod_c0)

if __name__ == '__main__':
    #test_model_f1_score_uncertainty_duq(batch_number = 0, folder_models="weights_70_labels", id_model="")
    #test_model_accuracy(batch_number = 1, class_label_iod_test_data = 0)
    #test_model_accuracy(batch_number=1, class_label_iod_test_data=1)
    for i in range(0, 10):
        test_model_f1_score_uncertainty_duq(batch_number = i, folder_models="weights_100_labels", id_model="")


"""
def test_entropy_norm():
    p = [0.9, 0.08, 0.01, 0.0009, 0.0001]
    (confidence, normalized_entropy) = normalize_entropy(p)
    print("confidence ", confidence)
    print("norm entropy ", normalized_entropy)
"""
