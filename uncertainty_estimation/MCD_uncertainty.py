
import torchvision.transforms as tfms
from PIL import Image as Pili
import scipy
from math import log

import torch
from fastai.vision import *
from fastai.callbacks import CSVLogger
from fastai.callbacks.hooks import *
from torchvision import models, transforms
import cv2
from os import listdir
from os.path import isfile, join

import pandas as pd
from scipy.stats import entropy
import DUQ_uncertainty as duq





def get_file_names_in_path(path):
    """
    Gets file names in path
    :param path:
    :return:
    """
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    return onlyfiles


def pil2fast(img, im_size = 110):
    """
    Pil to fastai image conversion
    :param img: input image
    :param im_size: desired size
    :return: fastai image
    """
    data_transform = transforms.Compose(
        [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
    return Image(data_transform(img))


def estimate_MCD_uncertainty_std_fastai(learner, num_inferences, img_path, img_names):
    """
    Estimates the MCD uncertainty, using learner predict from fastai
    :param learner: model to make inference
    :param num_inferences: number of inferences to calculate dispersion
    :param img_path: image path
    :param img_names: image names
    :return: standard deviation of inferences for all the images
    """
    std_all_images = []
    for i in range(len(img_names)):
        # Read the img
        image_pil = Pili.open(img_path + img_names[i]).convert('RGB')
        image_fastai = pil2fast(image_pil)
        get_mc_dropout_fastai
        predictions, outputs, std_best_class = get_mc_dropout_fastai(learner, image_fastai, max_its=num_inferences)
        std_all_images += [std_best_class]
    return std_all_images



def get_mc_dropout_fastai(learner, input_image, max_its=10, is_ssdl = False):
    """
    gets the montecarlo dropout for using fastai's predict
    :param learner:model to make inference
    :param input_image: number of inferences to calculate dispersion
    :param max_its: maximum number of iterations
    :param is_ssdl: is semi supervised?
    :return:
    """
    print("Input")
    print(input_image)

    predictions = []
    outputs = []
    #go through each iteration to make the prediction, injecting dropout
    for i in range(max_its):
        cat_tensor, tensor_class, model_output = learner.predict(input_image, with_dropout=True)
        #print("Model output before")
        #print(model_output)
        if(is_ssdl):
            model_output = nn.functional.softmax(model_output, dim=1)
        #print("Model output after")
        #print(model_output)
        pred_class = tensor_class.item()
        outputs.append((pred_class, model_output))

        predictions.append(model_output[pred_class].item())
    std_best_class = np.std(np.array(predictions))
    return predictions, outputs, std_best_class

def get_mc_dropout_fastai_all_classes(learner, input_image, max_its=10, is_ssdl = False):
    """
    gets the montecarlo dropout for using fastai's predict, for all the classes
    :param learner: model to make inference
    :param input_image: number of inferences to calculate dispersion
    :param max_its: maximum number of iterations
    :param is_ssdl: is semi supervised?
    :return:
    """
    num_classes = 2
    outputs = torch.zeros(max_its, num_classes)
    preds_class = []
    for i in range(max_its):
        cat_tensor, tensor_class, model_output = learner.predict(input_image, with_dropout=True)
        #print("Model output before")
        #print(model_output)
        if (is_ssdl):
            model_output = nn.functional.softmax(model_output, dim=0)
        #print("Model output after")
        #print(model_output)
        pred_class = tensor_class.item()
        preds_class.append(pred_class)
        #for each class
        outputs[i, :] = model_output

    std_all_classes = torch.std(outputs, 0)
    std_best_class = std_all_classes.sum()
    pred_class_mean = np.mean(np.array(preds_class))
    return outputs, pred_class_mean, std_best_class


def estimate_MCD_uncertainty_std_2(learner, num_inferences, img_path, img_names,  im_size=110, number_classes=2, correct_label=-1):
    """
    gets the montecarlo dropout for using pytorch's output
    :param learner:  model to make inference
    :param num_inferences: number of inferences to calculate dispersion
    :param img_path: path of the images to evaluate its predictive uncertainty
    :param img_names: image names
    :param im_size: image resolution
    :param number_classes: K number of classes
    :param correct_label: label of the loaded images
    :return:
    """
    # Fetch the images
    #img_names = get_file_names_in_path(img_path)
    data_transform = transforms.Compose(
        [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
    model = learner.model
    # Variables needed to test
    # Stores the img we want to infer
    #print("im size ", im_size)
    image = torch.zeros(1, 3, im_size, im_size).cuda()
    # Stores the results of everything

    results_per_class = torch.zeros(num_inferences, len(img_names), number_classes)
    results_predicted_class = torch.zeros(num_inferences, len(img_names))
    # results for all images
    std_preds_best_class = torch.zeros(len(img_names))
    # store uncerainty estimations for wrong and correct predictions
    stds_wrong_preds = []
    stds_correct_preds = []
    # Set to eval mode on all layers
    model.eval()
    # Enable dropout layers
    enable_dropout(model)
    # Infer all the dataset and store the results
    # For each image
    print("Number of images to do inference with : ", len(img_names))
    predicted_class_all_images = []
    for i in range(len(img_names)):
        # Read the img
        im = Pili.open(img_path + img_names[i]).convert('RGB')
        # Apply transformations to it
        # print("Image shape")
        image[0] = data_transform(im)

        # Run inference <num_inferences> times
        print("Making ", num_inferences, " inferences...")
        predicted_class_all_inferences = []
        for it in range(num_inferences):
            # Net scores for all classes
            scores_all_classes = model(image).data.cpu().squeeze()

            # be sure that we are exploring dimension 0
            value, index = torch.max(scores_all_classes, 0)
            predicted_class = index
            # accumulate to do class estimation
            predicted_class_all_inferences += [predicted_class]
            results_per_class[it, i, :] = model(image).data.cpu()
            results_predicted_class[it, i] = scores_all_classes[predicted_class]
        # get mean predicted class, rounding it
        mean_predicted_class_image = torch.round(
            torch.mean(torch.tensor(predicted_class_all_inferences, dtype=torch.float)))
        # store predicted class per image
        predicted_class_all_images += [mean_predicted_class_image]

    '''     Calc stats of the inference     '''
    # Calc of the previous runs
    for i in range(len(img_names)):
        # Calculates std
        std_preds_best_class[i] = torch.std(results_predicted_class[:, i], dim=0)
        # keep uncertainties for correct and wrong predictions
        if (correct_label != -1 and correct_label == predicted_class_all_images[i]):
            stds_correct_preds += [std_preds_best_class[i]]
        elif (correct_label != -1 and correct_label != predicted_class_all_images[i]):
            stds_wrong_preds += [std_preds_best_class[i]]
    return results_per_class, std_preds_best_class, stds_correct_preds, stds_wrong_preds



def enable_dropout(m):
  """
  Function to enable dropout in eval time
  :param m:
  :return:
  """
  for each_module in m.modules():
    if each_module.__class__.__name__.startswith('Dropout'):
      each_module.train()


def normalize_entropy(p):
    """
    Normalizes the entropy if needed
    :param p: output probability density
    :return:
    """
    entropy = 0
    for p_i in p:
        entropy -= p_i * log(p_i)
    print("Entropy ", entropy)
    num_buckets = len(p)
    p_norm = [1 / num_buckets] * num_buckets
    norm_coef = 0
    for p_norm_i in p_norm:
        norm_coef -= p_norm_i * log(p_norm_i)
    normalized_entropy = entropy / norm_coef
    confidence = 1 - normalized_entropy
    return confidence, normalized_entropy




def calculate_uncertainty_mcd_fastai_images(fastai_model, img_path, num_iters = 100, is_ssdl = False, class_label = 0, im_size = 110):
    """
    Calculates uncertaint for a set of images, using the MCD method
    :param fastai_model: fastai model to make inferences
    :param img_path: path of images
    :param num_iters: number of iterations
    :param is_ssdl:is semi supervised?
    :param class_label: label of the loaded images
    :param im_size: image resolution
    :return:
    """
    uncertainties_correct = []
    uncertainties_wrong = []
    stds_all_imgs = []
    pred_class_all_imgs = []
    list_images = get_file_names_in_path(img_path)
    print("total of images ", len(list_images))

    for i in range(0, len(list_images)):

        complete_path = img_path + list_images[i]
        image_pil = Pili.open(complete_path).convert('RGB')
        image_fastai = pil2fast(image_pil, im_size =im_size)
        outputs, pred_class, stds_sums_all = get_mc_dropout_fastai_all_classes(fastai_model, image_fastai, max_its=num_iters, is_ssdl=is_ssdl)
        #use output with no dropout for pred class
        cat_tensor, tensor_class, model_output = fastai_model.predict(image_fastai, with_dropout=False)


        stds_all_imgs += [stds_sums_all]
        if(tensor_class.item() != class_label):
            uncertainties_wrong += [stds_sums_all]
        else:
            uncertainties_correct += [stds_sums_all]

    return torch.tensor(stds_all_imgs), uncertainties_correct, uncertainties_wrong

