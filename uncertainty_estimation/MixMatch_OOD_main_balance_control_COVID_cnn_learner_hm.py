import re
from fastai.vision import *
from fastai.callbacks import CSVLogger

from numbers import Integral

import torch
import logging
import sys
from torchvision.utils import save_image
import numpy as np
#from utilities.InBreastDataset import InBreastDataset
from utilities.run_context import RunContext
from utilities.gradcam import *

import utilities.cli as cli
import utilities.uncertainty_estimators as uncertainty
import torchvision
#from utilities.albumentations_manager import get_albumentations

"""
w = torch.cuda.FloatTensor([1.0, 0.9, 1.1])
learn = create_cnn(data, models.resnet18, metrics = error_rate, loss_func=torch.nn.CrossEntropyLoss(weight=w))
learn.fit_one_cycle(1, 0.001)

https://forums.fast.ai/t/changing-default-loss-functions/28981
"""

class MultiTransformLabelList(LabelList):
    def __getitem__(self, idxs: Union[int, np.ndarray]) -> 'LabelList':
        """
        Create K  transformed images for the unlabeled data
        :param idxs:
        :return:
        """
        "return a single (x, y) if `idxs` is an integer or a new `LabelList` object if `idxs` is a range."
        global args
        #print("MULTITRANSFORM LIST")
        idxs = try_int(idxs)
        if isinstance(idxs, Integral):
            if self.item is None:
                #CALLED EVEN FOR UNLABELED DATA, Y IS USED!

                x, y = self.x[idxs], self.y[idxs]
            else:
                x, y = self.item, 0
            if self.tfms or self.tfmargs:
                #THIS IS DONE FOR UNLABELED DATA
                # I've changed this line to return a list of augmented images
                #print("AUGMENTING IMAGES")

                x = [x.apply_tfms(self.tfms, **self.tfmargs) for _ in range(args.K_transforms)]
            if hasattr(self, 'tfms_y') and self.tfm_y and self.item is None:
                #print("AUGMENTING LABELS")
                #IS NOT CALLED FOR UNLABELED DATA
                y = y.apply_tfms(self.tfms_y, **{**self.tfmargs_y, 'do_resolve': False})
            if y is None: y = 0
            return x, y
        else:
            return self.new(self.x[idxs], self.y[idxs])



def MixmatchCollate(batch):
    """
    # I'll also need to change the default collate function to accomodate multiple augments
    :param batch:
    :return:
    """
    batch = to_data(batch)
    if isinstance(batch[0][0], list):
        batch = [[torch.stack(s[0]), s[1]] for s in batch]
    return torch.utils.data.dataloader.default_collate(batch)





class MixupLoss(nn.Module):
    """
    Implements the mixup loss
    """

    def forward(self, preds, target, unsort=None, ramp=None, bs=None):
        """
        Ramp, unsort and bs is None when doing validation
        :param preds:
        :param target:
        :param unsort:ab
        :param ramp:w
        :param bs:
        :return:
        """
        global args, class_weights

        if(args.balanced==5):

            return self.forward_balanced_cross_entropy(preds, target, unsort, ramp, bs)
        else:
            #assign the same weight for the classes, in disregard with the dataset
            weight = 1 / len(class_weights)
            for i in range(0, len(class_weights)):
                class_weights[i] = weight
            #class_weights = torch.tensor([0.3333, 0.3333, 0.3333], device ="cuda:0")
            return self.forward_balanced_cross_entropy(preds, target, unsort, ramp, bs)

    def forward_cross_entropy(self, preds, target, unsort=None, ramp=None, bs=None):
        
        global args
        if unsort is None:
            return F.cross_entropy(preds, target)

        calculate_cross_entropy = nn.CrossEntropyLoss()
        preds = preds[unsort]
        preds_l = preds[:bs]
        preds_ul = preds[bs:]
        # calculate log of softmax, to ensure correct usage of cross entropy
        # one column per class, one batch per row
        # preds_l = torch.log_softmax(preds_l,dim=1)

        preds_ul = torch.softmax(preds_ul, dim=1)
        # TARGETS CANNOT BE 1-K ONE HOT VECTOR
        (highest_values, highest_classes) = torch.max(target[:bs], 1)

        highest_classes = highest_classes.long()

        loss_x = calculate_cross_entropy(preds_l, highest_classes)
        # loss_x = -(preds_l * target[:bs]).sum(dim=1).mean()
        loss_u = F.mse_loss(preds_ul, target[bs:])
        self.loss_x = loss_x.item()
        self.loss_u = loss_u.item()
        return loss_x + args.lambda_unsupervised * ramp * loss_u


    def forward_original(self, preds, target, unsort=None, ramp=None, num_labeled=None):
        global args
        """
        Implements the forward pass of the loss function
        :param preds: predictions of the model
        :param target: ground truth targets
        :param unsort: ?
        :param ramp: ramp weight
        :param num_labeled:
        :return:
        """
        if unsort is None:
            #used for evaluation
            return F.cross_entropy(preds,target)
        preds = preds[unsort]
        #labeled and unlabeled observations were packed in the same array
        preds_l = preds[:num_labeled]
        preds_ul = preds[num_labeled:]
        #apply logarithm to softmax of output, to ensure the correct usage of cross entropy
        preds_l = torch.log_softmax(preds_l,dim=1)
        preds_ul = torch.softmax(preds_ul,dim=1)
        #consider using CE_loss = nn.CrossEntropyLoss(reduction='none')(inputs, targets)
        loss_x = -(preds_l * target[:num_labeled]).sum(dim=1).mean()
        loss_u = F.mse_loss(preds_ul, target[num_labeled:])
        self.loss_x = loss_x.item()
        self.loss_u = loss_u.item()
        return loss_x + args.lambda_unsupervised * ramp * loss_u

    def forward_balanced(self, preds, target, unsort=None, ramp=None, bs=None):
        """
        Balanced forward implementation
        :param preds:
        :param target:
        :param unsort:
        :param ramp:
        :param bs:
        :return:
        """
        global args
        if unsort is None:
            return F.cross_entropy(preds, target)
        # target contains mixed up targets!! not just 0s and 1s
        preds = preds[unsort]
        preds_l = preds[:bs]
        preds_ul = preds[bs:]
        # calculate log of softmax, to ensure correct usage of cross entropy
        # one column per class, one batch per row
        preds_l = torch.log_softmax(preds_l, dim=1)

        # get the weights for the labeled observations
        weights_labeled = self.get_weights_observations(target[:bs])
        preds_ul = torch.softmax(preds_ul, dim=1)
        # get the weights for the unlabeled observations
        weights_unlabeled = self.get_weights_observations(target[bs:])
        loss_x = -(weights_labeled * preds_l * target[:bs]).sum(dim=1).mean()
        loss_u = F.mse_loss(weights_unlabeled * preds_ul, weights_unlabeled * target[bs:])
        self.loss_x = loss_x.item()
        self.loss_u = loss_u.item()
        return loss_x + args.lambda_unsupervised * ramp * loss_u

    def forward_balanced_cross_entropy(self, preds, target, unsort=None, ramp=None, bs=None):
        global args, class_weights
        if unsort is None:
            return F.cross_entropy(preds, target)
        #weights_labeled = self.get_weights_observations(target[:bs]).float()
        weights_unlabeled = self.get_weights_observations(target[bs:]).float()
        calculate_cross_entropy = nn.CrossEntropyLoss(weight = class_weights.float())
        preds = preds[unsort]
        preds_l = preds[:bs]
        preds_ul = preds[bs:]
        # calculate log of softmax, to ensure correct usage of cross entropy
        # one column per class, one batch per row
        # preds_l = torch.log_softmax(preds_l,dim=1)
        preds_ul = torch.softmax(preds_ul, dim=1)
        # TARGETS CANNOT BE 1-K ONE HOT VECTOR
        (highest_values, highest_classes) = torch.max(target[:bs], 1)
        highest_classes = highest_classes.long()
        loss_x = calculate_cross_entropy(preds_l, highest_classes)
        # loss_x = -(preds_l * target[:bs]).sum(dim=1).mean()


        loss_u = F.mse_loss(weights_unlabeled * preds_ul, weights_unlabeled * target[bs:])
        self.loss_x = loss_x.item()
        self.loss_u = loss_u.item()
        return loss_x + args.lambda_unsupervised * ramp * loss_u

    def forward_balanced_cross_entropy_original(self, preds, target, unsort=None, ramp=None, bs=None):
        global args, class_weights
        if unsort is None:
            return F.cross_entropy(preds, target)
        #weights_labeled = self.get_weights_observations(target[:bs]).float()
        weights_unlabeled = self.get_weights_observations(target[bs:]).float()
        print("Weights unlabeled")
        print(class_weights)
        calculate_cross_entropy = nn.CrossEntropyLoss(weight = class_weights.float())
        preds = preds[unsort]
        preds_l = preds[:bs]
        preds_ul = preds[bs:]
        # calculate log of softmax, to ensure correct usage of cross entropy
        # one column per class, one batch per row
        # preds_l = torch.log_softmax(preds_l,dim=1)
        preds_ul = torch.softmax(preds_ul, dim=1)
        # TARGETS CANNOT BE 1-K ONE HOT VECTOR
        (highest_values, highest_classes) = torch.max(target[:bs], 1)
        highest_classes = highest_classes.long()
        loss_x = calculate_cross_entropy(preds_l, highest_classes)
        # loss_x = -(preds_l * target[:bs]).sum(dim=1).mean()


        loss_u = F.mse_loss(weights_unlabeled * preds_ul, weights_unlabeled * target[bs:])
        self.loss_x = loss_x.item()
        self.loss_u = loss_u.item()
        return loss_x + args.lambda_unsupervised * ramp * loss_u





    def get_weights_observations(self, array_predictions):
        global class_weights

        # each column is a class, each row an observation
        num_classes = array_predictions.shape[1]
        num_observations = array_predictions.shape[0]
        (highest_values, highest_classes) = torch.max(array_predictions, 1)
        # turn the highest_classes array a column vector
        highest_classes_col = highest_classes.view(-1, 1)
        # highest classes for all the observations (rows) and classes (columns)
        highest_classes_all = highest_classes_col.repeat(1, num_classes)
        # print("highest classes all")
        # print(highest_classes_all)
        # scores all
        scores_all = class_weights[highest_classes_all]
        scores_all.to(device="cuda:0")
        return scores_all



class MixMatchImageList(ImageList):


    """
    Custom ImageList with filter function
    """
    def filter_train(self, num_items, seed = 23488):
        """
        Takes a number of observations as labeled, assumes that the evaluation observations are in the test folder
        :param num_items:
        :param seed: The seed is fixed for reproducibility
        :return: return the filtering function by itself
        """
        global args
        path_unlabeled = args.path_unlabeled
        if (args.path_unlabeled == ""):
          path_unlabeled = args.path_labeled
        #this means that a customized unlabeled dataset is not to be used, just pick the rest of the labelled data as unlabelled
        if(path_unlabeled == args.path_labeled):
          train_idxs = np.array([i for i, observation in enumerate(self.items) if Path(observation).parts[-3] != "test"])
          #valid_idxs = np.array([i for i, observation in enumerate(self.items) if Path(observation).parts[-3] == "test"])
        else:
          # IGNORE THE DATA ALREADY IN THE UNLABELED DATASET
          dataset_unlabeled =  torchvision.datasets.ImageFolder(path_unlabeled + "/train/")
          list_file_names_unlabeled = dataset_unlabeled.imgs
          for i in range(0, len(list_file_names_unlabeled)):
            #delete root of path
            #print("Before ", list_file_names_unlabeled[i])
            list_file_names_unlabeled[i] = list_file_names_unlabeled[i][0].replace(path_unlabeled, "")
            #print("after ", list_file_names_unlabeled[i])
          list_train = []
          #add  to train if is not in the unlabeled dataset
          for i, observation in enumerate(self.items):
            path_1 = str(Path(observation))
            sub_str = args.path_labeled
            path_2 = path_1.replace(sub_str, "")
            path_2 = path_2.replace("train/", "")
            is_path_in_unlabeled = path_2 in list_file_names_unlabeled
            #add the observation to the train list, if is not in the unlabeled dataset
            if( not "test" in path_2 and not is_path_in_unlabeled):
              list_train += [i]
          #store the train idxs c
          train_idxs = np.array(list_train)            
          logger.info("Customized number of unlabeled observations " + str(len(list_file_names_unlabeled)))
          
        valid_idxs = np.array([i for i, observation in enumerate(self.items) if Path(observation).parts[-3] == "test"])
        #return []
        # for reproducibility
        np.random.seed(seed)
        # keep the number of items desired, 500 by default
        keep_idxs = np.random.choice(train_idxs, num_items, replace=False)
        
        logger.info("Number of labeled observations: " + str(len(keep_idxs)))
        logger.info("First labeled id: " + str(keep_idxs[0]))
        logger.info("Number of validation observations: " + str(len(valid_idxs)))
        logger.info("Number of training observations " + str(len(train_idxs)))
        self.items = np.array([o for i, o in enumerate(self.items) if i in np.concatenate([keep_idxs, valid_idxs])])
        return self

    def filter_train_balance_control(self, num_items, seed=23488, desired_proportions = []):
        """
        :param num_items:
        :param seed:
        :param desired_proportions: The desired percentage of observations per class, to control class umbalance for labeled observations
        :return:
        """
        global args, class_weights
        # for reproducibility
        np.random.seed(seed)
        num_items_per_class = []
        #create a list of lists with the items per class
        items_per_class = [[] for _ in range(len(desired_proportions))]

        #calculate the number of items per class
        for i in range(0, len(desired_proportions)):
            num_items_per_class += [int(desired_proportions[i] * num_items) ]
        #get label dictionary
        label_dictionary = self.get_labels_dict()
        path_unlabeled = args.path_unlabeled
        if (args.path_unlabeled == ""):
            path_unlabeled = args.path_labeled
        # this means that a customized unlabeled dataset is not to be used, just pick the rest of the labelled data as unlabelled
        if (path_unlabeled == args.path_labeled):
            train_idxs_all_list = []
            for i, observation in enumerate(self.items):
                if (Path(observation).parts[-3] != "test"):
                    path_1 = str(Path(observation))
                    train_idxs_all_list += [i]
                    substr_train = re.findall(r"/\d+/", path_1)
                    label_num_str = re.findall(r"\d+", substr_train[0])
                    label = int(label_num_str[0])
                    proxy_label = label_dictionary[label]
                    # add the element to the corresponding sub list of observations for this class, according to label
                    items_per_class[proxy_label] += [i]
        else:
            # IGNORE THE DATA ALREADY IN THE UNLABELED DATASET
            dataset_unlabeled = torchvision.datasets.ImageFolder(path_unlabeled + "/train/")
            list_file_names_unlabeled = dataset_unlabeled.imgs
            for i in range(0, len(list_file_names_unlabeled)):
                # delete root of path
                # print("Before ", list_file_names_unlabeled[i])
                list_file_names_unlabeled[i] = list_file_names_unlabeled[i][0].replace(path_unlabeled, "")
                # print("after ", list_file_names_unlabeled[i])
            list_train = []
            # add  to train if is not in the unlabeled dataset

            for i, observation in enumerate(self.items):
                path_1 = str(Path(observation))
                sub_str = args.path_labeled
                path_2 = path_1.replace(sub_str, "")
                path_2 = path_2.replace("train/", "")
                is_path_in_unlabeled = path_2 in list_file_names_unlabeled
                # add the observation to the train list, if is not in the unlabeled dataset
                if (not "test" in path_2 and not is_path_in_unlabeled):
                    list_train += [i]
                    #get substring with train and class folder
                    substr_train = re.findall(r"/\d+/", path_2)
                    label_num_str = re.findall(r"\d+", substr_train[0])
                    label = int(label_num_str[0])
                    proxy_label = label_dictionary[label]
                    #add the element to the corresponding sub list of observations for this class, according to label
                    items_per_class[proxy_label] += [i]
            logger.info("Customized number of unlabeled  observations " + str(len(list_file_names_unlabeled)))
        #concat all the observations
        keep_idxs_all = []
        for i in range(0, len(desired_proportions)):
            #for each class, select the given number of random labels
            items_class_i = items_per_class[i]
            keep_idxs_i = np.random.choice(np.array(items_class_i), num_items_per_class[i], replace=False)
            keep_idxs_all += keep_idxs_i.tolist()

        keep_idxs_all_np = np.array(keep_idxs_all)

        #the test dataset is done when building the folder
        valid_idxs = np.array([i for i, observation in enumerate(self.items) if Path(observation).parts[-3] == "test"])
        logger.info("Number of labeled observations: " + str(len(keep_idxs_all_np)))
        logger.info("First labeled id: " + str(keep_idxs_all_np[0]))
        logger.info("Number of  validation observations: " + str(len(valid_idxs)))
        logger.info("Number  of training observations " + str(len(keep_idxs_all_np)))
        self.items = np.array([o for i, o in enumerate(self.items) if i in np.concatenate([keep_idxs_all_np, valid_idxs])])
        return self
    def get_labels_dict(self):
        """
        Get the dictionary with the labels
        :return:
        """
        proxy_label_counter = 0
        dictionary = {-1:0}
        for i, observation in enumerate(self.items):
            if (Path(observation).parts[-3] != "test"):
                path_1 = str(Path(observation))

                substr_train = re.findall(r"/\d+/", path_1)
                label_num_str = re.findall(r"\d+", substr_train[0])
                label = int(label_num_str[0])
                #if the element does not exist, add it
                try:
                    a = dictionary[label]
                except:
                    dictionary[label] = proxy_label_counter
                    proxy_label_counter += 1
        return dictionary


class MixMatchTrainer(LearnerCallback):
    """
    Mix match trainer functions
    """
    #order = -20

    def on_train_begin(self, **kwargs):
        """
        Callback used when the trainer is beginning, inits variables
        :param kwargs:
        :return:
        """
        global data_labeled
        self.l_dl = iter(data_labeled.train_dl)
        #metrics recorder
        self.smoothL, self.smoothUL = SmoothenValue(0.98), SmoothenValue(0.98)
        #metrics to be displayed in the table
        #REMOVE!
        #self.recorder.add_metric_names(["l_loss", "ul_loss"])
        self.it = 0

    def mixup(self, a_x, a_y, b_x, b_y):
        """
        Mixup augments data by mixing labels and pseudo labels and its observations
        :param a_x:
        :param a_y:
        :param b_x:
        :param b_y:
        :param alpha:
        :return:
        """
        global args
        alpha = args.alpha_mix
        l = np.random.beta(alpha, alpha)
        l = max(l, 1 - l)
        x = l * a_x + (1 - l) * b_x
        y = l * a_y + (1 - l) * b_y
        return x, y

    def sharpen(self, p):
        global args
        """
        Sharpens the distribution output, to encourage confidence
        :param p:
        :param T:
        :return:
        """
        T = args.T_sharpening
        u = p ** (1 / T)
        return u / u.sum(dim=1, keepdim=True)

    def on_batch_begin(self, train, last_input, last_target, **kwargs):
        """
        Called on batch training at the begining
        :param train:
        :param last_input:
        :param last_target:
        :param kwargs:
        :return:
        """
        global data_labeled, args
        if not train: return
        try:
            x_l, y_l = next(self.l_dl)
        except:
            self.l_dl = iter(data_labeled.train_dl)
            x_l, y_l = next(self.l_dl)
        #print!
        x_ul = last_input
        with torch.no_grad():
            #calculates the pseudo sharpened labels
            ul_labels = self.sharpen(
                torch.softmax(torch.stack([self.learn.model(x_ul[:, i]) for i in range(x_ul.shape[1])], dim=1),
                              dim=2).mean(dim=1))
        #create torch array of unlabeled data
        x_ul = torch.cat([x for x in x_ul])

        #WE CAN CALCULATE HERE THE CONFIDENCE COEFFICIENT

        ul_labels = torch.cat([y.unsqueeze(0).expand(args.K_transforms, -1) for y in ul_labels])

        l_labels = torch.eye(data_labeled.c).cuda()[y_l]

        w_x = torch.cat([x_l, x_ul])
        w_y = torch.cat([l_labels, ul_labels])
        idxs = torch.randperm(w_x.shape[0])
        #create mixed input and targets
        mixed_input, mixed_target = self.mixup(w_x, w_y, w_x[idxs], w_y[idxs])
        bn_idxs = torch.randperm(mixed_input.shape[0])
        unsort = [0] * len(bn_idxs)
        for i, j in enumerate(bn_idxs): unsort[j] = i
        mixed_input = mixed_input[bn_idxs]

        ramp = self.it / args.rampup_coefficient if self.it < args.rampup_coefficient else 1.0
        return {"last_input": mixed_input, "last_target": (mixed_target, unsort, ramp, x_l.shape[0])}

    def on_batch_end(self, train, **kwargs):
        """
        Add the metrics at the end of the batch training
        :param train:
        :param kwargs:
        :return:
        """
        if not train: return
        self.smoothL.add_value(self.learn.loss_func.loss_x)
        self.smoothUL.add_value(self.learn.loss_func.loss_u)
        self.it += 1

        """def on_epoch_end(self, last_metrics, **kwargs):
        Avoid adding weird stuff on metrics table
        When the epoch ends, add the accmulated metric values
        :param last_metrics:
        :param kwargs:
        :return:
        
        return add_metrics(last_metrics, [self.smoothL.smooth, self.smoothUL.smooth])
        """
def get_dataset_stats(args):
    # meanDatasetComplete = [0.485, 0.456, 0.406],
    # stdDatasetComplete = [0.229, 0.224, 0.225]
    if(args.norm_stats.strip() == "MNIST"):
        # stats for MNIST, replace!!
        meanDatasetComplete = [0.1307, 0.1307, 0.1307]
        stdDatasetComplete = [0.3081, 0.3081, 0.3081]
    elif(args.norm_stats.strip() == "Covid"):
        # stats COVID
        meanDatasetComplete =  [0.5415, 0.5417, 0.5423]
        stdDatasetComplete = [0.2231, 0.2231, 0.2231]

    elif (args.norm_stats.strip() == "Covid_cr"):
        # stats COVID
        meanDatasetComplete = [0.4823, 0.4823, 0.4823]
        stdDatasetComplete = [0.2218, 0.2219, 0.2220]

    return (meanDatasetComplete, stdDatasetComplete)

def calculate_weights(list_labels):
    """
    Calculate the class weights according to the number of observations
    :param list_labels:
    :return:
    """
    global logger, args
    array_labels = np.array(list_labels)
    logger.info("Using balanced loss: " + str(args.balanced))
    list_classes = np.unique(array_labels)

    weight_classes = np.zeros(len(list_classes))
    for curr_class in list_classes:

        number_observations_class = len(array_labels[array_labels == curr_class])
        logger.info("Number observations " + str(number_observations_class) + " for class " + str(curr_class))
        weight_classes[curr_class] = 1 / number_observations_class

    weight_classes = weight_classes / weight_classes.sum()
    #CAREFUL!!!!!!!
    #weight_classes = torch.tensor([0.3333, 0.3333, 0.3333])


    logger.info("Weights to use: " + str(weight_classes))
    weight_classes_tensor = torch.tensor(weight_classes, device ="cuda:0" )
    return weight_classes_tensor

def get_datasets():
    global desired_labeled_classes_dist
    """
    Get datasets   (FAST AI data bunches ) for labeled, unlabeled and validation
    :return: data_labeled (limited labeled data), data_unlabeled , data_full (complete labeled dataset)
    """
    global args, data_labeled, logger, class_weights
    path_labeled = args.path_labeled
    path_unlabeled = args.path_unlabeled
    if (args.path_unlabeled == ""):
        path_unlabeled = path_labeled
    #get dataset mean and std
    norm_stats = get_dataset_stats(args)
    logger.info("Loading labeled data from: " + path_labeled)
    logger.info("Loading unlabeled data from: " + path_unlabeled)
    # Create two databunch objects for the labeled and unlabled images. A fastai databunch is a container for train, validation, and
    # test dataloaders which automatically processes transforms and puts the data on the gpu.
    # https://docs.fast.ai/vision.transform.html
    data_labeled = (MixMatchImageList.from_folder(path_labeled)
                    .filter_train_balance_control(args.number_labeled, seed = 4200, desired_proportions = desired_labeled_classes_dist)  # Use 500 labeled images for traning
                    .split_by_folder(valid="test")  # test on all 10000 images in test set
                    .label_from_folder()
                    .transform(get_transforms(do_flip = True, flip_vert = True, max_zoom=1, max_warp=None, p_affine=0, p_lighting = 0),
                               size=args.size_image)
                    # On windows, must set num_workers=0. Otherwise, remove the argument for a potential performance improvement
                    .databunch(bs=args.batch_size, num_workers=args.workers)
                    .normalize(norm_stats))





    # normalize_funcs(mean:FloatTensor, std:FloatTensor, do_x:bool=True, do_y:bool=False)
    train_set = set(data_labeled.train_ds.x.items)
    #get the list of labels for the dataset
    list_labels = data_labeled.train_ds.y.items
    

    #calculate the class weights
    class_weights = calculate_weights(list_labels)
    # load the unlabeled data
    #filter picks the labeled images not contained in the unlabeled dataset, in the case of SSDL
    #the test set is in the unlabeled folder!!!!

    src = (ImageList.from_folder(path_unlabeled)
           .filter_by_func(lambda x: x not in train_set)
           .split_by_folder(valid="test")
           )

    #AUGMENT THE DATA
    src.train._label_list = MultiTransformLabelList
    # https://docs.fast.ai/vision.transform.html
    # data not in the train_set and splitted by test folder is used as unlabeled
    #WHY DOES IT NEED THE LABEL FROM FOLDER???? THAT INFORMATION IS NOT USED. IS JUST FOR USING THE FAST AI LABELER
    data_unlabeled = (src.label_from_folder()
                      .transform(get_transforms(do_flip = True, flip_vert = True, max_zoom=1, max_warp=None, p_affine=0, p_lighting = 0), size=args.size_image)
                      .databunch(bs=args.batch_size, collate_fn=MixmatchCollate, num_workers=10)
                      .normalize(norm_stats))
    logger.info("Information for unlabeled training data: ")
    list_labels_unlabeled = data_unlabeled.train_ds.y.items
    calculate_weights(list_labels_unlabeled)
    logger.info("Information for validation data: ")
    print("MISSING! ")
    #list_labels_unlabeled = data_unlabeled.test_ds.y.items
    #calculate_weights(list_labels_unlabeled)


    # Databunch with all 50k images labeled, for baseline
    data_full = (ImageList.from_folder(path_labeled)
                 .split_by_folder(valid="test")
                 .label_from_folder()
                 .transform(get_transforms(do_flip = True, flip_vert = True, max_zoom=1, max_warp=None, p_affine=0, p_lighting = 0),
                            size=args.size_image)
                 .databunch(bs=args.batch_size, num_workers=args.workers)
                 .normalize(norm_stats))
    return (data_labeled, data_unlabeled, data_full)




def train_mix_match():
    """
    Train the mix match model
    :param path_labeled:
    :param path_unlabeled:
    :param number_epochs:
    :param learning_rate:
    :param mode:
    :return:
    """
    global data_labeled, is_colab, logger, args
    learning_rate = args.lr
    number_epochs = args.epochs

    # Use weighted loss
    loss = CrossEntropyFlat()
    # Use this dropout rate only for densenet
    drop_p = 4 * [0.5]
    logger = logging.getLogger('main')
    (data_labeled, data_unlabeled, data_full)= get_datasets()

    #start_nf the initial number of features
    """
    Wide ResNet with num_groups and a width of k.
    Each group contains N blocks. start_nf the initial number of features. Dropout of drop_p is applied in between the two convolutions in each block. The expected input channel size is fixed at 3.
    Structure: initial convolution -> num_groups x N blocks -> final layers of regularization and pooli
    """
    if(args.model == "wide_resnet"):
        #model = models.WideResNet(num_groups=3,N=4,num_classes=args.num_classes,k = 2,start_nf=args.size_image)
        model =  models.densenet121
    elif(args.model == "densenet"):
        model = models.densenet121(num_classes=args.num_classes)
    elif(args.model == "squeezenet"):
        model = models.squeezenet1_1(num_classes=args.num_classes)
    elif(args.model.strip() == "alexnet"):
        logger.info("Using alexnet")
        model = models.alexnet(num_classes=args.num_classes)

    if (args.mode.strip() == "fully_supervised"):
        logger.info("Training fully supervised model")
        # Edit: We can find the answer ‘Note that metrics are always calculated on the validation set.’ on this page: https://docs.fast.ai/training.html 42.
        if (is_colab):
            learn = cnn_learner(data_full, model, metrics=[accuracy])
        else: #, callback_fns = [CSVLogger]
            learn = cnn_learner(data_full, model, metrics=[accuracy], callback_fns = [CSVLogger])



    if (args.mode.strip() == "partial_supervised"):
        logger.info("Training supervised model with a limited set of labeled data")
        if(is_colab):
            #uses loss_func=FlattenedLoss of CrossEntropyLoss()
            learn = Learner(data_labeled, model, metrics=[accuracy])
        else:
            if(args.balanced == 5):
                logger.info("Using balanced cross entropy")
                calculate_cross_entropy = nn.CrossEntropyLoss(weight=class_weights.float())
                learn = cnn_learner(data_labeled, model, metrics=[accuracy], callback_fns = [CSVLogger], loss_func = calculate_cross_entropy)
            else:
                learn = cnn_learner(data_labeled, model, metrics=[accuracy], callback_fns=[CSVLogger])


        #learn.fit_one_cycle(number_epochs, learning_rate, wd=args.weight_decay)

    """
    fit[source][test]
    fit(epochs:int, lr:Union[float, Collection[float], slice]=slice(None, 0.003, None), wd:Floats=None, callbacks:Collection[Callback]=None)
    Fit the model on this learner with lr learning rate, wd weight decay for epochs with callbacks.
    """
    
    if (args.mode.strip() == "ssdl"):
        logger.info("Training semi supervised model with limited set of labeled data")
        # https://datascience.stackexchange.com/questions/15989/micro-average-vs-macro-average-performance-in-a-multiclass-classification-settin
        mixloss = MixupLoss()
        setattr(mixloss, 'reduction', 'none')
        if(is_colab):
            learn = cnn_learner(data_unlabeled, model, loss_func=mixloss, callback_fns=[MixMatchTrainer], metrics=[accuracy])
        else:
            learn = cnn_learner(data_unlabeled, model, loss_func=mixloss, callback_fns=[MixMatchTrainer, CSVLogger],
                            metrics=[accuracy])
        # learn.fit_one_cycle(600,1e-4,wd=1e-4)
        #learn.fit_one_cycle(number_epochs, learning_rate, wd=args.weight_decay)
    #train the model
    learn.fit_one_cycle(number_epochs, learning_rate, wd=args.weight_decay)
    #if it is not colab, write the csv to harddrive
    if(not is_colab):
        logged_frame = learn.csv_logger.read_logged_file()
        context.write_run_log(logged_frame, args.results_file_name)
    #save the model weights
    if(args.save_weights):
        path_weights = args.weights_path_name

        logger.info("Saving weights in: " + str(path_weights))
        #learn.export(path_weights)
        learn.save(path_weights)
        save_heatmaps(learn)
    print("Starting report of uncertainty")
    report_uncertainty(learn, batch_number = 0)


def save_heatmaps(learn):
    interp = ClassificationInterpretation.from_learner(learn, ds_type=DatasetType.Valid)
    #10 images of one class
    for i in range(0, 10):
        image_idx = i
        path_image_heatmaps_0 = args.weights_path_name + str(image_idx) + ".png"
        # taken from https://forums.fast.ai/t/gradcam-and-guided-backprop-intergration-in-fastai-library/33462
        gcam = GradCam.from_interp(learn, interp, image_idx)  # image_idx from ds.valid_ds or ds.test_ds
        gcam.plot(filename=path_image_heatmaps_0)

    #10 images of another one
    for i in range(30, 40):
        image_idx = i
        path_image_heatmaps_0 = args.weights_path_name + str(image_idx) + ".png"
        # taken from https://forums.fast.ai/t/gradcam-and-guided-backprop-intergration-in-fastai-library/33462
        gcam = GradCam.from_interp(learn, interp, image_idx)  # image_idx from ds.valid_ds or ds.test_ds
        gcam.plot(filename=path_image_heatmaps_0)

    logger.info("Activation maps saved at: " + str(path_image_heatmaps_0))


def get_activation_maps(learner):
    preds, y, losses = learner.get_preds(with_loss=True)
    # preds,y,losses = learn.get_preds(with_loss=True, ds_type=DatasetType.Train)
    interpretation = ClassificationInterpretation(learner, preds, y, losses)
    figure = interpretation.plot_top_losses(12, figsize=(20, 20), heatmap=True, return_fig=True)
    plt.show()
    figure.savefig("la_figura.png")
		
def main_colab():
    global args, logger, is_colab
    is_colab = True
    dateInfo = "{date:%Y-%m-%d_%H_%M_%S}".format(date=datetime.now())
    logging.basicConfig(filename="log_" + dateInfo + ".txt", level=logging.INFO, format='%(message)s')
    logger = logging.getLogger('main')    
    #Get the default arguments
    args = create_parser().parse_args(args=[])
    #args.balanced = False
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.info("Arguments:  " + str(args))
    train_mix_match()

if __name__ == '__main__':
    global args, counter, context, logger, is_colab, desired_labeled_classes_dist
    is_colab = False
    args = cli.parse_commandline_args()
    #convert labeled desired class dist
    desired_labeled_classes_dist = []
    desired_labeled_classes_str = args.desired_labeled_classes_dist
    # assumes a string with the format '0, 0, 0, 11, 0, 0, 0, 0, 0, 19, 0, 9, 0, 0, 0, 0, 0, 0, 11'
    if (desired_labeled_classes_str != ""):
        desired_labeled_classes_dist = [float(s) for s in desired_labeled_classes_str.split(',')]

    print("Desired labeled dist")
    print(desired_labeled_classes_dist)
    logger = logging.getLogger('main')
    context = RunContext(logging, args)
    logger.info("Learning rate " + str(args.lr))
    #But no matter what I try my model never generalizes. My training loss and validation loss are diverging almost immediately, and my training loss << validation loss. So I’m overfitting.

    train_mix_match()


def report_uncertainty( learner, batch_number = 0):
    # train_covid_model(NUMBER_EPOCHS = 1)
    class_label_iod_test_data = 0
    img_path_iod = "/media/Data/saul/Datasets/Covid19/Dataset/batches_labeled_undersampled_in_dist_BINARY_CHINA_30_val/batch_" + str(batch_number) + "/test/" + str(class_label_iod_test_data) + "/"
    img_path_ood = "/media/Data/saul/Datasets/PSP_dataset12/no_few"
    img_names_iod = uncertainty.get_file_names_in_path(img_path_iod)
    img_names_ood = uncertainty.get_file_names_in_path(img_path_ood)
    print("ood images list")
    num_inferences = 100
    preds_ood, stds_ood, a, b = uncertainty.estimate_MCD_uncertainty_std_2(learner, num_inferences, img_path_ood, img_names_ood)
    preds_iod, stds_iod, list_correct_iod, list_wrong_iod = uncertainty.estimate_MCD_uncertainty_std_2(learner, num_inferences, img_path_iod, img_names_iod, correct_label=class_label)
    summary_name = "summary_std_china_batch_no_ssdl_" + str(batch_number)
    if(args.mode.strip() == "ssdl"):
        summary_name = "summary_std_china_batch_ssdl_" + str(batch_number)
    #uncertainty estimation
    uncertainty.create_summaries_csv_individual(summary_name, stds_iod, stds_ood, list_correct_iod, list_wrong_iod)