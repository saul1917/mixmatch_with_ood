
import torchvision.transforms as tfms
from PIL import Image as Pili
import scipy
from math import log

import torch
from fastai.vision import *
from fastai.callbacks import CSVLogger
from fastai.callbacks.hooks import *
from torchvision import models, transforms
import cv2
from os import listdir
from os.path import isfile, join

import pandas as pd
from scipy.stats import entropy
#import natsort
import os
import torch
import pandas as pd
from skimage import io, transform
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils


def load_ssdl_model(model_name = "CHINA_SSDL_model", path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/", path_dataset ="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/CR_DATASET/", SIZE_IMAGE = 110, BATCH_SIZE = 12, WORKERS = 8):
    """
    Semi supervised model loading
    :param model_name: model id or name
    :param path_models: path to the model
    :param path_dataset: path to the dataset
    :param SIZE_IMAGE: size of the image
    :param BATCH_SIZE: batch size
    :param WORKERS: number of workers
    :return: loaded model
    """
    model = models.densenet121
    meanDatasetComplete = [0.4823, 0.4823, 0.4823]
    stdDatasetComplete = [0.2231, 0.2231, 0.2231]
    norm_stats = (meanDatasetComplete, stdDatasetComplete)
    # create the dataset
    data_full = (ImageList.from_folder(path_dataset)
                 .split_by_folder(valid="test")
                 .label_from_folder()
                 .transform(
        get_transforms(do_flip=True, flip_vert=True, max_zoom=1, max_warp=None, p_affine=0, p_lighting=0),
        size=SIZE_IMAGE)
                 .databunch(bs=BATCH_SIZE, num_workers=WORKERS)
                 .normalize(norm_stats))


    print("Path ", path_models)
    # assuming ssdl model
    mixloss = MixupLoss()
    setattr(mixloss, 'reduction', 'none')
    learner_loaded_ssdl = cnn_learner(data=data_full, base_arch=model, loss_func=mixloss,
                                      callback_fns=[MixMatchTrainer], metrics=[accuracy])

    learner_loaded_ssdl.load(path_models + model_name)
    print("SSDL  model loaded")
    return learner_loaded_ssdl


def load_no_ssdl_model(path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/", path_dataset ="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/CR_DATASET/", SIZE_IMAGE = 110, BATCH_SIZE = 12, WORKERS = 8, model_name = 'model_NO_SSDL'):
    """
    Supervised model loading
    :param model_name: model id or name
    :param path_models: path to the model
    :param path_dataset: path to the dataset
    :param SIZE_IMAGE: size of the image
    :param BATCH_SIZE: batch size
    :param WORKERS: number of workers
    :return: loaded model
    """
    model = models.densenet121
    meanDatasetComplete = [0.4823, 0.4823, 0.4823]
    stdDatasetComplete = [0.2231, 0.2231, 0.2231]
    norm_stats = (meanDatasetComplete, stdDatasetComplete)
    #loss used in the model
    calculate_cross_entropy = nn.CrossEntropyLoss()
    # create the dataset
    data_full = (ImageList.from_folder(path_dataset)
                 .split_by_folder(valid="test")
                 .label_from_folder()
                 .transform(
        get_transforms(do_flip=True, flip_vert=True, max_zoom=1, max_warp=None, p_affine=0, p_lighting=0),
        size=SIZE_IMAGE)
                 .databunch(bs=BATCH_SIZE, num_workers=WORKERS)
                 .normalize(norm_stats))
    learner_loaded_no_ssdl = cnn_learner(data=data_full, base_arch=model, loss_func=calculate_cross_entropy,
                                         metrics=[accuracy])
    #load learner
    learner_loaded_no_ssdl.load(path_models + model_name)
    print("No SSDL model loaded!")
    return learner_loaded_no_ssdl, data_full





def get_file_names_in_path(path):
    """
    Get file names in path
    :param path: string with path
    :return: returns list of files
    """
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    return onlyfiles


"""
Sanity checks
"""

def pil2fast(img, im_size = 110):
    """
    Pil image to fastai
    :param img: input pil image
    :param im_size: input image dimensions
    :return: fastai image
    """
    data_transform = transforms.Compose(
        [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
    return Image(data_transform(img))

def test_compare_fastai_pytorch_output():
    """
    Sanity check that compares fastai output with pytorch
    :return:
    """
    class_label = 0
    im_size = 110
    meanDatasetComplete = [0.4823, 0.4823, 0.4823]
    stdDatasetComplete = [0.2231, 0.2231, 0.2231]
    ssdl_model = load_ssdl_model(path_models="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/",
                                 path_dataset="E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1",
                                 SIZE_IMAGE=110, BATCH_SIZE=12,
                                 WORKERS=8, model_name='CHINA_SSDL_model_batch_1_')
    no_ssdl_model, data_full = load_no_ssdl_model(path_models="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/",
                                                  path_dataset="E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1",
                                                  SIZE_IMAGE=110, BATCH_SIZE=12,
                                                  WORKERS=8, model_name='CHINA_NO_SSDL_model_batch_1_')
    img_path_iod = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1/test/" + str(class_label) + "/"
    list_images = get_file_names_in_path(img_path_iod)
    print("total of images ", len(list_images))
    fails_fastai = 0
    fails_pytorch = 0
    for i in range(0, len(list_images)):
        complete_path = img_path_iod + list_images[i]
        image_pil = Pili.open(complete_path).convert('RGB')
        image_ood_fastai = pil2fast(image_pil)
        data_transform = transforms.Compose(
            [transforms.Resize((im_size, im_size)), transforms.ToTensor(), transforms.Normalize(meanDatasetComplete, stdDatasetComplete)])
        image_ood_tensor = data_transform(image_pil)
        cat_tensor, tensor_class, model_output = no_ssdl_model.predict(image_ood_fastai, with_dropout=False)
        print("Fastai evaluation output")
        print(model_output)
        print(tensor_class)
        if (tensor_class.item() != class_label):
            fails_fastai += 1
        # pytorch model
        model = no_ssdl_model.model
        image = torch.zeros(1, 3, im_size, im_size).cuda()
        image[0] = image_ood_tensor
        scores_all_classes = nn.functional.softmax(model(image).data.cpu(), dim=1).squeeze()
        val, index = torch.max(scores_all_classes, 0)
        print("Estimated class: ", index)
        if (index != class_label):
            fails_pytorch += 1
        print("Pytorch evaluation output")
        print(scores_all_classes)
        print(model(image).data.cpu())

    print("TOTAL FAILS FASTAI OUTPUT: ", fails_fastai)
    print("TOTAL FAILS PYTORCH OUTPUT: ", fails_pytorch)
    # Pytorch and fastai outputs are quite different
    # SSDL model can produce out of bounds (0-1) results


class CustomDataSet(Dataset):
    """
    Custom dataset loader
    """
    def __init__(self, main_dir, transform):
        """
        Loads a dataset and applies transformations
        :param main_dir: main directory to load
        :param transform: transforms to apply
        """
        self.main_dir = main_dir
        self.transform = transform
        all_imgs = os.listdir(main_dir)
        self.total_imgs = natsort.natsorted(all_imgs)

    def __len__(self):
        """
        Size of the dataset
        :return:
        """
        return len(self.total_imgs)

    def __getitem__(self, idx):
        """
        Gets item of the dataset
        :param idx: item id
        :return:
        """
        img_loc = os.path.join(self.main_dir, self.total_imgs[idx])
        #print("trying to open ", img_loc)
        image = Pili.open(img_loc).convert("RGB")

        tensor_image = self.transform(image)
        return tensor_image

def calculate_class_centroids(model_fastai, dataset_path_base, im_size = 110, batch_size = 12, class_centroid = 0):
    """
    Calculates the class centroids for the given dataset in the feature space
    :param model_fastai: fastai model
    :param dataset_path_base: dataset path
    :param im_size: image dimensions
    :param batch_size: batch size
    :param class_centroid: class number
    :return: returns the centroid
    """
    dataset_path_base += str(class_centroid)

    model_pytorch = model_fastai.model
    #print("Model device ", model_pytorch.get_device())

    meanDatasetComplete = [0.4823, 0.4823, 0.4823]
    stdDatasetComplete = [0.2231, 0.2231, 0.2231]

    data_transform = transforms.Compose(
        [transforms.Resize((im_size, im_size)), transforms.ToTensor(),
         transforms.Normalize(meanDatasetComplete, stdDatasetComplete)])
    my_dataset_pytorch = CustomDataSet(dataset_path_base, transform=data_transform)
    #load the dataset
    train_loader = DataLoader(my_dataset_pytorch, batch_size=batch_size, shuffle=False,
                                   num_workers=4, drop_last=True)

    total_observations = 0
    #extracts the dataset features with the given model
    for id_batch, images_batch in enumerate(train_loader):
        images_batch = images_batch.cuda()
        total_observations += images_batch.shape[0]
        #scores_all_classes = nn.functional.softmax(model_pytorch(images_batch).data, dim=1).squeeze()
        features_batch = extract_features(model_pytorch, images_batch)
        sum_batch = torch.sum(features_batch, dim=0)

    centroid_class = sum_batch / total_observations
    return centroid_class


def extract_features(model_pytorch, images_batch):
    """
    Extracts the features for the given model and images batch
    :param model_pytorch: model to extract features
    :param images_batch:
    :return:
    """
    #the last layer is usually the classifier
    feature_extractor = model_pytorch[0:-1]
    features_batch = feature_extractor(images_batch)
    features_flattened = torch.flatten(features_batch.data, start_dim = 1, end_dim = -1)
    return features_flattened

def test_centroid_calculator():
    """
    Sanity checker for centroid calculation
    :return:
    """

    no_ssdl_model, data_full = load_no_ssdl_model(path_models="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/",
                                                  path_dataset="E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1",
                                                  SIZE_IMAGE=110, BATCH_SIZE=12,
                                                  WORKERS=8, model_name='CHINA_NO_SSDL_model_batch_1_')

    training_path_batch_centroids_base = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1/train/"
    test_class = 0
    test_images_in_dist = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1/test/" + str(test_class) + "/"
    img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0/"
    duqs_all_imgs, uncertainties_correct, uncertainties_wrong = calculate_uncertainty_DUQ_images(no_ssdl_model, test_images_in_dist, training_path_batch_centroids_base,  class_label=test_class, im_size=110)
    print("DUQS ALL")
    print(duqs_all_imgs)
    print("DUQS correct")
    print(uncertainties_correct)
    print("DUQS incorrect")
    print(uncertainties_wrong)

def calculate_uncertainty_DUQ_images(fastai_model, input_images_path, training_images_path, class_label = 0, im_size = 110):
    """
    Calculate uncertainty using DUQ for a set of images
    :param fastai_model: fastai model to use
    :param input_images_path: input images path
    :param training_images_path: training images path
    :param class_label: class label for all the images
    :param im_size: images resolution
    :return:
    """
    #reset the list of centroids
    global list_centroids
    list_centroids = []
    uncertainties_correct = []
    uncertainties_wrong = []
    duqs_all_imgs = []
    list_images = get_file_names_in_path(input_images_path)
    meanDatasetComplete = [0.4823, 0.4823, 0.4823]
    stdDatasetComplete = [0.2231, 0.2231, 0.2231]

    data_transform = transforms.Compose(
        [transforms.Resize((im_size, im_size)), transforms.ToTensor(),
         transforms.Normalize(meanDatasetComplete, stdDatasetComplete)])
    #go through each image
    for i in range(0, len(list_images)):
        complete_path = input_images_path + list_images[i]
        image_pil = Pili.open(complete_path).convert('RGB')
        image_pytorch = data_transform(image_pil)
        image_fastai = pil2fast(image_pil)
        #calculates DUQ for the given image
        max_distance = get_DUQ_image(fastai_model, image_pytorch, training_images_path)
        #use output with no dropout for pred class
        cat_tensor, tensor_class, model_output = fastai_model.predict(image_fastai, with_dropout=False)
        duqs_all_imgs += [max_distance.item()]
        if(tensor_class.item() != class_label):
            uncertainties_wrong += [max_distance]
        else:
            uncertainties_correct += [max_distance.item()]



    return torch.tensor(duqs_all_imgs), uncertainties_correct, uncertainties_wrong

def get_DUQ_image(fastai_model, input_image_pytorch, training_images_path_base, num_classes = 2):
    """
    Gets the DUQ for a single image
    :param fastai_model:
    :param input_image_pytorch:
    :param training_images_path_base:
    :param num_classes:
    :return:
    """
    global list_centroids
    distances_classes = []
    input_image_pytorch = input_image_pytorch.unsqueeze(dim = 0).cuda()
    features_input_image = extract_features(fastai_model.model, input_image_pytorch)
    #for each class, calculate its centroid, and take the closest one
    for i in range(0, num_classes):
        if(len(list_centroids) != num_classes):
            #calculate list of centroids in case it has not been calculated
            centroid_class_i = calculate_class_centroids(fastai_model, training_images_path_base, im_size=110, batch_size=12,
                                                         class_centroid=0)
            list_centroids += [centroid_class_i]
        else:
            centroid_class_i = list_centroids[i]
        #take L2 distance
        distance_i = torch.norm(centroid_class_i - features_input_image, 2)
        distances_classes += [distance_i]
    #take the closest one
    max_l2_distance = max(distances_classes)
    return max_l2_distance



if __name__ == '__main__':
    use_cuda = torch.cuda.is_available()
    print("Is cuda available? ", use_cuda)
    test_centroid_calculator()



