from scipy.spatial import distance

import sys
#distance measurer for CIFAR 10
import time

sys.path.insert(0, '../../dataset_distance_measurer/')
from dataset_distance_measurer import dataset_distance_tester_pdf
from dataset_distance_measurer import dataset_distance_tester


def run_tests_pdf_COVID_OOD_NIS(distance_str ="cosine", ood_perc = 100, batch_size_p = 40, model_name = "alexnet"):
    """
    :param distance: distance_str
    :return:
    """
    #path_labelled =  "/media/Data/saul/Datasets/CIFAR10/batches_labeled_in_dist_60"
    #path_base_unlabelled = "/media/Data/saul/Datasets/CIFAR10/batches_unlabeled_HALF_60"
    path_labelled = "/media/Data/saul/Datasets/Covid19/Dataset/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels"
    path_base_unlabelled = "/media/Data/saul/Datasets/Covid19/Dataset/OOD_COVID19/OOD_NIS"
    if(distance_str == "js"):
        distance_func = distance.jensenshannon
    elif(distance_str == "cosine"):
        distance_func = distance.cosine

    #HALF
    print("Calculating " + distance_str  + " distance for CR dataset")
    dataset_distance_tester_pdf(
        path_bunch1=path_labelled,
        path_bunch2= path_base_unlabelled + "/batch_", ood_perc=ood_perc,
        num_unlabeled=90, name_ood_dataset="NIS_OOD_pdf_"+ model_name + distance_str + "_nl_" + str(batch_size_p), num_batches=10, distance_func = distance_func,  batch_size_p = batch_size_p, model_name = model_name)


def run_tests_pdf_COVID_OOD_CHINA(distance_str ="cosine", ood_perc = 100, batch_size_p = 40, model_name = "alexnet"):
    """
    :param distance: distance_str
    :return:
    """
    #path_labelled =  "/media/Data/saul/Datasets/CIFAR10/batches_labeled_in_dist_60"
    #path_base_unlabelled = "/media/Data/saul/Datasets/CIFAR10/batches_unlabeled_HALF_60"
    path_labelled = "/media/Data/saul/Datasets/Covid19/Dataset/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels"
    path_base_unlabelled = "/media/Data/saul/Datasets/Covid19/Dataset/OOD_COVID19/OOD_CHINA"
    if(distance_str == "js"):
        distance_func = distance.jensenshannon
    elif(distance_str == "cosine"):
        distance_func = distance.cosine

    #HALF
    print("Calculating " + distance_str  + " distance for CR dataset")
    dataset_distance_tester_pdf(
        path_bunch1=path_labelled,
        path_bunch2= path_base_unlabelled + "/batch_", ood_perc=ood_perc,
        num_unlabeled=90, name_ood_dataset="CHINA_OOD_pdf_" + model_name +"_nl_" + str(batch_size_p), num_batches=10, distance_func = distance_func,  batch_size_p = batch_size_p, model_name = model_name)


def run_tests_pdf_COVID_OOD_CR(distance_str ="cosine", ood_perc = 100, batch_size_p = 40, model_name = "alexnet"):
    """
    :param distance: distance_str
    :return:
    """
    #path_labelled =  "/media/Data/saul/Datasets/CIFAR10/batches_labeled_in_dist_60"
    #path_base_unlabelled = "/media/Data/saul/Datasets/CIFAR10/batches_unlabeled_HALF_60"
    path_labelled = "/media/Data/saul/Datasets/Covid19/Dataset/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels"
    path_base_unlabelled = "/media/Data/saul/Datasets/Covid19/Dataset/OOD_COVID19/OOD_CR"
    if(distance_str == "js"):
        distance_func = distance.jensenshannon
    elif(distance_str == "cosine"):
        distance_func = distance.cosine

    #HALF
    print("Calculating " + distance_str  + " distance for CR dataset")
    dataset_distance_tester_pdf(
        path_bunch1=path_labelled,
        path_bunch2= path_base_unlabelled + "/batch_", ood_perc=ood_perc,
        num_unlabeled=90, name_ood_dataset="CR_OOD_20_labels_pdf_" + model_name + distance_str + "_ood_perc_" + str(ood_perc)  + "_nl_" + str(batch_size_p), num_batches=10, distance_func = distance_func,  batch_size_p = batch_size_p, model_name = model_name)



def run_tests_pdf_COVID_OOD_CHINA_degrees(distance_str ="cosine",  batch_size_p = 40, model_name = "alexnet"):
    """
    :param distance: distance_str
    :return:
    """
    if (distance_str == "js"):
        distance_func = distance.jensenshannon
    elif (distance_str == "cosine"):
        distance_func = distance.cosine


    ood_perc = 35
    path_labelled = "/media/Data/saul/Datasets/Covid19/Dataset/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels"
    path_base_unlabelled = "/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/OOD_CR_35_CHINA_65"
    print("Calculating " + distance_str  + " distance for CR dataset")
    dataset_distance_tester_pdf(
        path_bunch1=path_labelled,
        path_bunch2= path_base_unlabelled + "/batch_", ood_perc=ood_perc,
        num_unlabeled=90, name_ood_dataset="CHINA_OOD_pdf_CR_35_CHINA_65_"+ model_name + distance_str + "_nl_" + str(batch_size_p), num_batches=10, distance_func = distance_func,  batch_size_p = batch_size_p, model_name = model_name)

    ood_perc = 65

    path_base_unlabelled = "/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/OOD_CR_65_CHINA_35"
    print("Calculating " + distance_str + " distance for CR dataset")
    dataset_distance_tester_pdf(
        path_bunch1=path_labelled,
        path_bunch2=path_base_unlabelled + "/batch_", ood_perc=ood_perc,
        num_unlabeled=90, name_ood_dataset="CHINA_OOD_pdf_CR_35_CHINA_65_" + model_name + distance_str + "_nl_" + str(batch_size_p), num_batches=10,
        distance_func=distance_func, batch_size_p=40, model_name = model_name)


def run_tests_pdf_COVID_OOD_NIS_degrees(distance_str ="cosine",  batch_size_p = 40, model_name = "alexnet"):
    """
    :param distance: distance_str
    :return:
    """
    if (distance_str == "js"):
        distance_func = distance.jensenshannon
    elif (distance_str == "cosine"):
        distance_func = distance.cosine


    ood_perc = 35
    path_labelled = "/media/Data/saul/Datasets/Covid19/Dataset/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels"
    path_base_unlabelled = "/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/OOD_CR_35_NIS_65"
    print("Calculating " + distance_str  + " distance for CR dataset")
    dataset_distance_tester_pdf(
        path_bunch1=path_labelled,
        path_bunch2= path_base_unlabelled + "/batch_", ood_perc=ood_perc,
        num_unlabeled=90, name_ood_dataset="NIS_OOD_pdf_CR_35_NIS_65" + distance_str  + "_nl_" + str(batch_size_p), num_batches=10, distance_func = distance_func,  batch_size_p = batch_size_p, model_name=model_name)

    ood_perc = 65
    path_labelled = "/media/Data/saul/Datasets/Covid19/Dataset/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels"
    path_base_unlabelled = "/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/OOD_CR_65_NIS_35"
    print("Calculating " + distance_str + " distance for CR dataset")
    dataset_distance_tester_pdf(
        path_bunch1=path_labelled,
        path_bunch2=path_base_unlabelled + "/batch_", ood_perc=ood_perc,
        num_unlabeled=90, name_ood_dataset="NIS_OOD_pdf_CR_65_NIS_35" + distance_str  + "_nl_" + str(batch_size_p), num_batches=10,
        distance_func=distance_func, batch_size_p=40, model_name=model_name)


def run_tests_pdf_COVID_OOD_INDIANA_degrees_no_pp(distance_str ="cosine",  batch_size_p = 40, model_name = "alexnet"):
    """
    :param distance: distance_str
    :return:
    """
    if (distance_str == "js"):
        distance_func = distance.jensenshannon
    elif (distance_str == "cosine"):
        distance_func = distance.cosine


    ood_perc = 35
    path_labelled = "/media/Data/saul/Datasets/Covid19/Dataset/batches_labeled_undersampled_in_dist_BINARY_INDIANA_30_val_40_labels"
    path_base_unlabelled = "/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/INDIANA_65_CR_35_No_Preprocessed/batches_unlabeled/"
    print("Calculating " + distance_str  + " distance for INDIANA-CR dataset")
    dataset_distance_tester_pdf(
        path_bunch1=path_labelled,
        path_bunch2= path_base_unlabelled + "/batch_", ood_perc=ood_perc,
        num_unlabeled=90, name_ood_dataset="INDIANAS_OOD_pdf_CR_65_NIS_35_no_pp_alexnet_" + model_name + distance_str  + "_nl_" + str(batch_size_p), num_batches=3, distance_func = distance_func,  batch_size_p = batch_size_p, model_name=model_name, size_image = 220)






def run_ood_tests_pdf_40_labels_densenet():
    start_time = time.time()
    run_tests_pdf_COVID_OOD_INDIANA_degrees_no_pp(distance_str="cosine", batch_size_p=40, model_name = "densenet")
    total_time = (time.time() - start_time)
    print("total time in secs: ", str(total_time))






run_ood_tests_pdf_40_labels_densenet()