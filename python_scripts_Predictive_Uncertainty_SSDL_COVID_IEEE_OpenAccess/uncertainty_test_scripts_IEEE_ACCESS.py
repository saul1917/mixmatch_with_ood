
import torchvision.transforms as tfms
from PIL import Image as Pili
import scipy
from math import log
#from gradcam import *
import torch
from fastai.vision import *
from fastai.callbacks import CSVLogger
from fastai.callbacks.hooks import *
#from grad_cam import (BackPropagation, Deconvolution, GradCAM, GuidedBackPropagation)
from torchvision import models, transforms
import cv2
from os import listdir
from os.path import isfile, join
sys.path.insert(0, '../')
sys.path.insert(0, '../utilities/')
from MixMatch_OOD_main_balance_control_COVID import *
#from fast_ai_example import MixMatchImageList
#from fast_ai_example import MixupLoss
#from fast_ai_example import MixMatchTrainer
import pandas as pd

from scipy.stats import entropy

sys.path.insert(0, '../uncertainty_estimation/')
from Softmax_uncertainty import *
from MCD_uncertainty import *
import DUQ_uncertainty as duq

def get_file_names_in_path(path):
    """
    Get file names in path
    :param path: string path to use
    :return:
    """
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    return onlyfiles



def test_compare_fastai_pytorch_output():
    """
    Sanity checks
    :return:
    """
    class_label = 0
    im_size = 110
    ssdl_model = load_ssdl_model(path_models="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/",
                                 path_dataset="E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1",
                                 SIZE_IMAGE=110, BATCH_SIZE=12,
                                 WORKERS=8, model_name='CHINA_SSDL_model_batch_1_')
    no_ssdl_model, data_full = load_no_ssdl_model(path_models="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/",
                                                  path_dataset="E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1",
                                                  SIZE_IMAGE=110, BATCH_SIZE=12,
                                                  WORKERS=8, model_name='CHINA_NO_SSDL_model_batch_1_')
    img_path_iod = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1/test/" + str(class_label) + "/"
    list_images = get_file_names_in_path(img_path_iod)
    print("total of images ", len(list_images))
    fails_fastai = 0
    fails_pytorch = 0
    for i in range(0, len(list_images)):
        complete_path = img_path_iod + list_images[i]
        image_pil = Pili.open(complete_path).convert('RGB')
        image_ood_fastai = pil2fast(image_pil)
        data_transform = transforms.Compose(
            [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
        image_ood_tensor = data_transform(image_pil)
        cat_tensor, tensor_class, model_output = ssdl_model.predict(image_ood_fastai, with_dropout=False)
        print("Fastai evaluation output")
        print(model_output)
        print(tensor_class)
        if (tensor_class.item() != class_label):
            fails_fastai += 1
        # pytorch model
        model = ssdl_model.model
        image = torch.zeros(1, 3, im_size, im_size).cuda()
        image[0] = image_ood_tensor
        scores_all_classes = nn.functional.softmax(model(image).data.cpu(), dim=1).squeeze()
        val, index = torch.max(scores_all_classes, 0)
        print("Estimated class: ", index)
        if (index != class_label):
            fails_pytorch += 1
        print("Pytorch evaluation output")
        print(scores_all_classes)
        print(model(image).data.cpu())

    print("TOTAL FAILS FASTAI OUTPUT: ", fails_fastai)
    print("TOTAL FAILS PYTORCH OUTPUT: ", fails_pytorch)
    # Pytorch and fastai outputs are quite different
    # SSDL model can produce out of bounds (0-1) results
    """
    For instance
    Loading the dataset:


    In the CLUSTER
    Fastai evaluation output
    tensor([0.9602, 0.0398])
    Pytorch evaluation output
    tensor([0.2528, 0.7472])
    tensor([[-2.0311, -4.1816]])

    """



def test_mcd_ood_batch(batch_number = 1):
    """
    unit test 1
    :param batch_number:
    :return:
    """
    # train_covid_model(NUMBER_EPOCHS = 1)
    print("Pytorch current device")
    print(torch.cuda.current_device())
    class_label_iod_test_data = 0
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    #img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(class_label_iod_test_data) + "/"
    img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0/"
    #img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0c/"
    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/"

    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_"
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_"
    learner_no_ssdl = load_no_ssdl_model(path_models = path_models, path_dataset = path_labeled, model_name = model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models = path_models, path_dataset = path_labeled, model_name = model_name_ssdl)

    stds_iod_ssdl, stds_ood_ssdl, stds_iod_no_ssdl, stds_ood_no_ssdl, list_correct_ssdl, list_wrong_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl = test_uncertainty_std_ood(learner_loaded_no_ssdl=learner_no_ssdl, learner_loaded_ssdl=learner_ssdl, img_path_iod=img_path_iod, img_path_ood=img_path_ood, class_label=class_label_iod_test_data)

    #softmaxes_iod_ssdl, softmaxes_ood_ssdl, softmaxes_iod_no_ssdl, softmaxes_ood_no_ssdl = test_uncertainty_softmax(learner_loaded_no_ssdl=learner_no_ssdl, learner_loaded_ssdl= learner_ssdl,  img_path_iod=img_path_iod, img_path_ood= img_path_ood, class_label=class_label_iod_test_data)
    #summary_name = "summary_softmax_china_batch_" + str(batch_number)
    summary_name = "summary_std_china_batch_" + str(batch_number)
    create_summaries_csv(summary_name, stds_iod_ssdl, stds_ood_ssdl, stds_iod_no_ssdl, stds_ood_no_ssdl, list_correct_ssdl, list_wrong_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl)



def test_uncertainty_std_ood(learner_loaded_ssdl, learner_loaded_no_ssdl, img_path_iod, img_path_ood, class_label):
    """
    Sanity check of estimate_MCD_uncertainty_std_2
    :param learner_loaded_ssdl:
    :param learner_loaded_no_ssdl:
    :param img_path_iod:
    :param img_path_ood:
    :param class_label:
    :return:
    """
    #define uncertainty estimation parameters
    num_inferences = 100
    #img_names_ood = ["20588164.bmp", "22580218.bmp", "22613848.bmp", "22613996.bmp", "22670442.bmp", "22670488.bmp"]
    img_names_ood = get_file_names_in_path(img_path_ood)
    print("ood images list")
    print(img_names_ood)
    #img_names_iod = ["0_chest_ray.bmp", "10_chest_ray.bmp", "11_chest_ray.bmp", "12_chest_ray.bmp"]
    img_names_iod = get_file_names_in_path(img_path_iod)
    print("Starting uncertainty estimation for SSDL model...")
    print("iod images list ")
    print(img_names_iod)
    #one std per class
    pred,  std_ood_ssdl, list_correct, list_wrong = estimate_MCD_uncertainty_std_2(learner_loaded_ssdl, num_inferences, img_path_ood, img_names_ood)
    pred,  std_iod_ssdl, list_correct_ssdl, list_wrong_ssdl  = estimate_MCD_uncertainty_std_2(learner_loaded_ssdl, num_inferences, img_path_iod, img_names_iod, correct_label=class_label)
    print("Uncertainty estimation done!")
    print("Std for ood image: ", std_ood_ssdl)
    print("Std for iod image: ", std_iod_ssdl)
    print("list correct")
    print(list_correct_ssdl)
    print("list wrong")
    print(list_wrong_ssdl)

    print("Starting uncertainty estimation for NO SSDL model...")
    # one std per class
    pred,  std_ood_no_ssdl, list_correct, list_wrong  = estimate_MCD_uncertainty_std_2(learner_loaded_no_ssdl, num_inferences, img_path_ood, img_names_ood, im_size=110)
    pred,  std_iod_no_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl  = estimate_MCD_uncertainty_std_2(learner_loaded_no_ssdl, num_inferences, img_path_iod, img_names_iod, im_size=110, correct_label=class_label)
    print("Uncertainty estimation done!")
    print("Std for ood image: ", std_ood_no_ssdl)
    print("Std for iod image: ", std_iod_no_ssdl)
    print("list correct")
    print(list_correct)
    print("list wrong")
    print(list_wrong)
    print("Final results")
    #higher the better, std ood ssdl should  be higher than with no ssdl
    res_ood = torch.mean(std_ood_ssdl - std_ood_no_ssdl)
    res_ood_std = torch.std(std_ood_ssdl - std_ood_no_ssdl)
    #lower the better,
    res_iod = torch.mean(std_iod_ssdl - std_iod_no_ssdl)
    res_iod_std = torch.std(std_iod_ssdl - std_iod_no_ssdl)
    print("STD OOD SSDL - NO SSDL, higher the better: ", res_ood, " std ", res_ood_std)
    print("STD IOD SSDL - NO SSDL, lower the better: ", res_iod, " std ", res_iod_std)
    return std_iod_ssdl, std_ood_ssdl, std_iod_no_ssdl, std_ood_no_ssdl, list_correct_ssdl, list_wrong_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl




def load_ssdl_model(model_name = "CHINA_SSDL_model", path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/", path_dataset ="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/CR_DATASET/", SIZE_IMAGE = 110, BATCH_SIZE = 12, WORKERS = 8):
    """
    Semi supervised model loading
    :param model_name:
    :param path_models:
    :param path_dataset:
    :param SIZE_IMAGE:
    :param BATCH_SIZE:
    :param WORKERS:
    :return:
    """
    model = models.densenet121
    meanDatasetComplete = [0.4823, 0.4823, 0.4823]
    stdDatasetComplete = [0.2231, 0.2231, 0.2231]
    norm_stats = (meanDatasetComplete, stdDatasetComplete)
    # create the dataset
    data_full = (ImageList.from_folder(path_dataset)
                 .split_by_folder(valid="test")
                 .label_from_folder()
                 .transform(
        get_transforms(do_flip=True, flip_vert=True, max_zoom=1, max_warp=None, p_affine=0, p_lighting=0),
        size=SIZE_IMAGE)
                 .databunch(bs=BATCH_SIZE, num_workers=WORKERS)
                 .normalize(norm_stats))


    print("Path ", path_models)
    # assuming ssdl model
    mixloss = MixupLoss()
    setattr(mixloss, 'reduction', 'none')
    learner_loaded_ssdl = cnn_learner(data=data_full, base_arch=model, loss_func=mixloss,
                                      callback_fns=[MixMatchTrainer], metrics=[accuracy])

    learner_loaded_ssdl.load(path_models + model_name)
    print("SSDL  model loaded")
    return learner_loaded_ssdl


def load_no_ssdl_model(path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/", path_dataset ="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/CR_DATASET/", SIZE_IMAGE = 110, BATCH_SIZE = 12, WORKERS = 8, model_name = 'model_NO_SSDL'):
    """
    Supervised model loading
    :param path_models:
    :param path_dataset:
    :param SIZE_IMAGE:
    :param BATCH_SIZE:
    :param WORKERS:
    :param model_name:
    :return:
    """
    model = models.densenet121
    meanDatasetComplete = [0.4823, 0.4823, 0.4823]
    stdDatasetComplete = [0.2231, 0.2231, 0.2231]
    norm_stats = (meanDatasetComplete, stdDatasetComplete)
    #loss used in the model
    calculate_cross_entropy = nn.CrossEntropyLoss()
    # create the dataset
    data_full = (ImageList.from_folder(path_dataset)
                 .split_by_folder(valid="test")
                 .label_from_folder()
                 .transform(
        get_transforms(do_flip=True, flip_vert=True, max_zoom=1, max_warp=None, p_affine=0, p_lighting=0),
        size=SIZE_IMAGE)
                 .databunch(bs=BATCH_SIZE, num_workers=WORKERS)
                 .normalize(norm_stats))
    learner_loaded_no_ssdl = cnn_learner(data=data_full, base_arch=model, loss_func=calculate_cross_entropy,
                                         metrics=[accuracy])
    #load learner
    learner_loaded_no_ssdl.load(path_models + model_name)
    print("No SSDL model loaded!")
    return learner_loaded_no_ssdl, data_full




def test_uncertainty_softmax(learner_loaded_ssdl, learner_loaded_no_ssdl, img_path_iod, img_path_ood, class_label):
    """
    Test for softmax uncertainty estimator
    :param learner_loaded_ssdl:
    :param learner_loaded_no_ssdl:
    :param img_path_iod:
    :param img_path_ood:
    :param class_label:
    :return:
    """
    #define uncertainty estimation parameters
    #img_names_ood = ["20588164.bmp", "22580218.bmp", "22613848.bmp", "22613996.bmp", "22670442.bmp", "22670488.bmp"]
    img_names_ood = get_file_names_in_path(img_path_ood)
    print("ood images list")
    print(img_names_ood)
    #img_names_iod = ["0_chest_ray.bmp", "10_chest_ray.bmp", "11_chest_ray.bmp", "12_chest_ray.bmp"]
    img_names_iod = get_file_names_in_path(img_path_iod)
    print("Starting uncertainty estimation for SSDL model...")
    print("iod images list ")
    print(img_names_iod)
    #one std per class
    pred,  softmaxes_ood_ssdl = estimate_uncertainty_softmax(learner_loaded_ssdl, img_path_ood, img_names_ood, im_size=110)
    pred,  softmaxes_iod_ssdl = estimate_uncertainty_softmax(learner_loaded_ssdl, img_path_iod, img_names_iod, im_size=110)
    print("Uncertainty estimation done!")
    print("Softmaxes for ood image: ", softmaxes_ood_ssdl)
    print("Softmaxes for iod image: ", softmaxes_iod_ssdl)

    print("Starting uncertainty estimation for NO SSDL model...")
    # one std per class
    pred,  softmaxes_ood_no_ssdl = estimate_uncertainty_softmax(learner_loaded_no_ssdl,  img_path_ood, img_names_ood, im_size=110)
    pred,  softmaxes_iod_no_ssdl = estimate_uncertainty_softmax(learner_loaded_no_ssdl,  img_path_iod, img_names_iod, im_size=110)
    print("Uncertainty estimation done!")
    print("softmaxes for ood image: ", softmaxes_ood_no_ssdl)
    print("softmaxes for iod image: ", softmaxes_iod_no_ssdl)

    print("Final results")
    #higher the better, std ood ssdl should  be higher than with no ssdl
    res_ood = torch.mean(softmaxes_ood_ssdl - softmaxes_ood_no_ssdl)
    res_ood_std = torch.std(softmaxes_ood_ssdl - softmaxes_ood_no_ssdl)
    #lower the better,
    res_iod = torch.mean(softmaxes_iod_ssdl - softmaxes_iod_no_ssdl)
    res_iod_std = torch.std(softmaxes_iod_ssdl - softmaxes_iod_no_ssdl)
    print("softmaxes OOD SSDL - NO SSDL, lower the better: ", res_ood, " std ", res_ood_std)
    print("softmaxes IOD SSDL - NO SSDL, higher the better: ", res_iod, " std ", res_iod_std)
    return softmaxes_iod_ssdl, softmaxes_ood_ssdl, softmaxes_iod_no_ssdl, softmaxes_ood_no_ssdl




def create_summaries_csv(summary_name, softmaxes_iod_ssdl, softmaxes_ood_ssdl, softmaxes_iod_no_ssdl, softmaxes_ood_no_ssdl, list_correct_ssdl, list_wrong_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl):
    """
    Create csv summaries
    :param summary_name:
    :param softmaxes_iod_ssdl:
    :param softmaxes_ood_ssdl:
    :param softmaxes_iod_no_ssdl:
    :param softmaxes_ood_no_ssdl:
    :param list_correct_ssdl:
    :param list_wrong_ssdl:
    :param list_correct_no_ssdl:
    :param list_wrong_no_ssdl:
    :return:
    """
    list_iod_ssdl = concat_mean_std_to_list(softmaxes_iod_ssdl)
    list_ood_ssdl = concat_mean_std_to_list(softmaxes_ood_ssdl)
    list_iod_no_ssdl =concat_mean_std_to_list(softmaxes_iod_no_ssdl)
    list_ood_no_ssdl = concat_mean_std_to_list(softmaxes_ood_no_ssdl)
    #concat stats for correct and wrong lists

    list_correct_ssdl = ["Correct"] + concat_mean_std_to_list(torch.tensor(list_correct_ssdl))
    list_wrong_ssdl = ["Incorrect"] + concat_mean_std_to_list(torch.tensor(list_wrong_ssdl))
    list_correct_no_ssdl = ["Correct"] + concat_mean_std_to_list(torch.tensor(list_correct_no_ssdl))
    list_wrong_no_ssdl = ["Incorrect"] + concat_mean_std_to_list(torch.tensor(list_wrong_no_ssdl))


    #iod data results
    stat, p_value_iod = scipy.stats.wilcoxon(list_iod_ssdl, list_iod_no_ssdl, correction=True)
    list_iod_ssdl += [p_value_iod]
    list_iod_no_ssdl += [p_value_iod]
    #create dataframe for IOD
    print("P value IOD ", p_value_iod)
    data_dictionary_iod = {'Softmaxes_iod_ssdl': list_iod_ssdl, 'Softmaxes_iod_no_ssdl': list_iod_no_ssdl}

    dataframe_iod = pd.DataFrame(data_dictionary_iod, columns=["Softmaxes_iod_ssdl", "Softmaxes_iod_no_ssdl"])
    dataframe_iod.to_csv(summary_name + "_iod.csv", index=False)
    #ood data results

    print(list_ood_ssdl)
    print(list_ood_no_ssdl)
    stat, p_value_ood = scipy.stats.wilcoxon(list_ood_ssdl, list_ood_no_ssdl, correction=True)
    list_ood_ssdl += [p_value_ood]
    list_ood_no_ssdl += [p_value_ood]
    print("P value OOD ", p_value_ood)

    data_dictionary_ood = {'Softmaxes_ood_ssdl': list_ood_ssdl, 'Softmaxes_ood_no_ssdl': list_ood_no_ssdl}
    dataframe_ood = pd.DataFrame(data_dictionary_ood, columns=["Softmaxes_ood_ssdl", "Softmaxes_ood_no_ssdl"])
    dataframe_ood.to_csv(summary_name + "_ood.csv", index=False)
    print("Summaries written!")
    items = [list_correct_ssdl, list_wrong_ssdl]
    df_ssdl = pd.DataFrame(items)
    df_ssdl.to_csv(summary_name + "ssdl_uncertainty_wrong_correct.csv", index=False )
    items = [list_correct_no_ssdl, list_wrong_no_ssdl]
    df_no_ssdl = pd.DataFrame(items)
    df_no_ssdl.to_csv(summary_name + "no_ssdl_uncertainty_wrong_correct.csv", index=False)
    print("Wrong/correct summaries written!")



def create_summaries_csv_right_wrong(summary_name, list_correct_ssdl, list_wrong_ssdl, list_correct_no_ssdl, list_wrong_no_ssdl):
    """
    Creates the summary for the wrong and correct estimations
    :param summary_name:
    :param list_correct_ssdl:
    :param list_wrong_ssdl:
    :param list_correct_no_ssdl:
    :param list_wrong_no_ssdl:
    :return:
    """
    #concat stats for correct and wrong lists
    list_correct_ssdl = ["Correct"] + concat_mean_std_to_list(torch.tensor(list_correct_ssdl))
    list_wrong_ssdl = ["Incorrect"] + concat_mean_std_to_list(torch.tensor(list_wrong_ssdl))
    list_correct_no_ssdl = ["Correct"] + concat_mean_std_to_list(torch.tensor(list_correct_no_ssdl))
    list_wrong_no_ssdl = ["Incorrect"] + concat_mean_std_to_list(torch.tensor(list_wrong_no_ssdl))

    items = [list_correct_ssdl, list_wrong_ssdl]
    df_ssdl = pd.DataFrame(items)
    df_ssdl.to_csv(summary_name + "ssdl_uncertainty_wrong_correct.csv", index=False )
    items = [list_correct_no_ssdl, list_wrong_no_ssdl]
    df_no_ssdl = pd.DataFrame(items)
    df_no_ssdl.to_csv(summary_name + "no_ssdl_uncertainty_wrong_correct.csv", index=False)
    print("Wrong/correct summaries written!")


def concat_mean_std_to_list(torch_tensor):
    """
    Concatenates the mean and std to the list
    :param torch_tensor:
    :return:
    """
    list_tensor = torch_tensor.numpy().tolist() + [torch.mean(torch_tensor).item()] + [torch.std(torch_tensor).item()]
    return list_tensor




def test_mcd_fastai_simple():
    """
    Simple tests for the MCD uncertainty estimator
    :return:
    """
    no_ssdl_model, data_full = load_no_ssdl_model(path_models="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/",
                       path_dataset="E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1", SIZE_IMAGE=110, BATCH_SIZE=12,
                       WORKERS=8, model_name='CHINA_NO_SSDL_model_batch_1_')

    ssdl_model = load_ssdl_model(path_models="C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/",
                                                  path_dataset="E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_1",
                                                  SIZE_IMAGE=110, BATCH_SIZE=12,
                                                  WORKERS=8, model_name='CHINA_SSDL_model_batch_1_')
    print("validation data")
    print(data_full.valid_ds)

    img = data_full.valid_ds[0][0]
    print("img")
    print(img)
    num_iters = 1000
    #for a binary classifier, std is the same for both classes
    predictions, outputs, std_best_class = get_mc_dropout_fastai_all_classes(no_ssdl_model, img, max_its=num_iters, is_ssdl=False)
    print("IOD NO SSDL")
    print(std_best_class)

    img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0/20588164.bmp"
    im_ood_pil = Pili.open(img_path_ood).convert('RGB')
    image_ood_fastai = pil2fast(im_ood_pil)
    predictions, outputs, std_best_class = get_mc_dropout_fastai_all_classes(no_ssdl_model, image_ood_fastai, max_its=num_iters, is_ssdl=False)
    print("OOD NO SSDL")
    print(std_best_class)
    print("SSDL MODEL---------------------------")
    predictions, outputs, std_best_class = get_mc_dropout_fastai_all_classes(ssdl_model, img, max_its=num_iters, is_ssdl=True)
    print("IOD SSDL:")
    print(std_best_class)
    predictions, outputs, std_best_class = get_mc_dropout_fastai_all_classes(ssdl_model, image_ood_fastai,
                                                                             max_its=num_iters, is_ssdl=True)
    print("OOD  SSDL")
    print(std_best_class)









def test_softmax_fastai_iod_ood(batch_number = 1):
    """
    Control Test softmax with fastai
    :param batch_number:
    :return:
    """
    # train_covid_model(NUMBER_EPOCHS = 1)
    print("Pytorch current device")
    print(torch.cuda.current_device())
    class_label_iod_test_data = 0
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    #img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(class_label_iod_test_data) + "/"
    img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0/"
    #img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0c/"
    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/"

    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_"
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_"
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models = path_models, path_dataset = path_labeled, model_name = model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models = path_models, path_dataset = path_labeled, model_name = model_name_ssdl)

    #no ssdl model
    softmaxes_no_ssdl_iod, uncertainties_correct_no_ssdl_iod, uncertainties_wrong_no_ssdl_iod = calculate_uncertainty_softmax_fastai_images(learner_no_ssdl, img_path_iod,  is_ssdl=False, class_label=class_label_iod_test_data, im_size=110)
    softmaxes_no_ssdl_ood, _, _ = calculate_uncertainty_softmax_fastai_images(learner_no_ssdl, img_path_ood, is_ssdl=False, im_size=110)
    #ssdl model
    softmaxes_ssdl_iod, uncertainties_correct_ssdl_iod, uncertainties_wrong_ssdl_iod = calculate_uncertainty_softmax_fastai_images(learner_ssdl, img_path_iod, is_ssdl=True, class_label=class_label_iod_test_data, im_size=110)
    softmaxes_ssdl_ood, _, _ = calculate_uncertainty_softmax_fastai_images(learner_ssdl, img_path_ood, is_ssdl=True, im_size=110)


    summary_name = "summary_std_china_batch_" + str(batch_number)
    create_summaries_csv(summary_name, softmaxes_ssdl_iod, softmaxes_ssdl_ood, softmaxes_no_ssdl_iod, softmaxes_no_ssdl_ood, list_correct_ssdl = uncertainties_correct_ssdl_iod, list_wrong_ssdl = uncertainties_wrong_ssdl_iod, list_correct_no_ssdl = uncertainties_correct_no_ssdl_iod, list_wrong_no_ssdl = uncertainties_wrong_no_ssdl_iod)


def test_mcd_fastai_iod_ood(batch_number = 1):
    """
    Control test for MCD
    :param batch_number:
    :return:
    """
    # train_covid_model(NUMBER_EPOCHS = 1)
    print("Pytorch current device")
    print(torch.cuda.current_device())
    class_label_iod_test_data = 0
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    #img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(class_label_iod_test_data) + "/"
    img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0/"
    #img_path_ood = "E:/GoogleDrive/DATASETS_TEMP/Breasts_0c/"
    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/"

    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_"
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_"
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models = path_models, path_dataset = path_labeled, model_name = model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models = path_models, path_dataset = path_labeled, model_name = model_name_ssdl)

    #no ssdl model
    stds_sums_no_ssdl_iod, uncertainties_correct_no_ssdl_iod, uncertainties_wrong_no_ssdl_iod = calculate_uncertainty_mcd_fastai_images(learner_no_ssdl, img_path_iod, num_iters=100, is_ssdl=False, class_label=class_label_iod_test_data, im_size=110)
    stds_sums_no_ssdl_ood, _, _ = calculate_uncertainty_mcd_fastai_images(learner_no_ssdl, img_path_ood, num_iters=100, is_ssdl=False, im_size=110)
    #ssdl model
    stds_sums_ssdl_iod, uncertainties_correct_ssdl_iod, uncertainties_wrong_ssdl_iod = calculate_uncertainty_mcd_fastai_images(learner_ssdl, img_path_iod, num_iters=100,
                                                                    is_ssdl=True,
                                                                    class_label=class_label_iod_test_data, im_size=110)
    stds_sums_ssdl_ood, _, _ = calculate_uncertainty_mcd_fastai_images(learner_ssdl, img_path_ood, num_iters=100,
                                                                    is_ssdl=True, im_size=110)


    summary_name = "summary_std_china_batch_" + str(batch_number)
    create_summaries_csv(summary_name, stds_sums_ssdl_iod, stds_sums_ssdl_ood, stds_sums_no_ssdl_iod, stds_sums_no_ssdl_ood, list_correct_ssdl = uncertainties_correct_ssdl_iod, list_wrong_ssdl = uncertainties_wrong_ssdl_iod, list_correct_no_ssdl = uncertainties_correct_no_ssdl_iod, list_wrong_no_ssdl = uncertainties_wrong_no_ssdl_iod)

def measure_model_accuracy_test(learner, img_path, class_label, im_size = 110):
    """
    Measures the model's accuracy with test data
    :param learner: fastai model
    :param img_path: images path
    :param class_label: class for the loaded images
    :param im_size: image resolution
    :return:
    """
    list_images = get_file_names_in_path(img_path)
    print("total of images ", len(list_images))
    num_test = len(list_images)
    correct_preds = 0
    wrong_preds = 0
    for i in range(0, num_test):
        complete_path = img_path + list_images[i]
        image_pil = Pili.open(complete_path).convert('RGB')
        image_fastai = pil2fast(image_pil, im_size=im_size)
        cat_tensor, tensor_class, model_output = learner.predict(image_fastai, with_dropout=False)
        if(tensor_class.item() == class_label):
            correct_preds += 1
        else:
            wrong_preds += 1

    accuracy = correct_preds/num_test
    return accuracy, correct_preds, wrong_preds

def test_model_accuracy(batch_number = 1, class_label_iod_test_data = 0):
    """
    Tests the model's accuracy (test data)
    :param batch_number: batch number
    :param class_label_iod_test_data:
    :return:
    """
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    # img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"

    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/"
    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_"
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_"
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models=path_models, path_dataset=path_labeled,
                                                    model_name=model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models=path_models, path_dataset=path_labeled, model_name=model_name_ssdl)
    acc_no_ssdl, correct_preds, wrong_preds = measure_model_accuracy_test(learner_no_ssdl, img_path_iod, class_label = class_label_iod_test_data, im_size = 110)
    print("Accuracy NO SSDL ", acc_no_ssdl, " correct: ", correct_preds, " wrong: ", wrong_preds, " for class: ", class_label_iod_test_data)
    acc_ssdl, correct_preds, wrong_preds = measure_model_accuracy_test(learner_ssdl, img_path_iod, class_label=class_label_iod_test_data,
                                              im_size=110)
    print("Accuracy  SSDL ", acc_ssdl, " correct: ", correct_preds, " wrong: ", wrong_preds,  " for class: ", class_label_iod_test_data)

def get_precision_recall_f1_score(fastai_model, test_image_path_c0, test_image_path_c1, im_size = 110):
    """
    Gets precision recall and f1 score of the given model
    :param fastai_model:
    :param test_image_path_c0:
    :param test_image_path_c1:
    :param im_size:
    :return:
    """
    #0 is normal or no pathology
    acc_c0, correct_preds_c0, wrong_preds_c0 = measure_model_accuracy_test(fastai_model, test_image_path_c0,
                                                                          class_label=0,
                                                                          im_size=im_size)
    print("get_precision_recall_f1_score correct preds c0 ",  correct_preds_c0, " wrong preds c0 ", wrong_preds_c0)
    # 1 is covid-19 positive
    acc_c1, correct_preds_c1, wrong_preds_c1 = measure_model_accuracy_test(fastai_model, test_image_path_c1,
                                                                           class_label=1,
                                                                           im_size=im_size)
    print("get_precision_recall_f1_score correct preds c1 ", correct_preds_c1, " wrong preds c1 ", wrong_preds_c1)
    true_positives = correct_preds_c1
    false_positives = wrong_preds_c0
    false_negatives = wrong_preds_c1
    recall = true_positives / (true_positives + false_negatives)
    precision = true_positives / (true_positives + false_positives)
    f1_score = (2 * recall * precision) / (precision + recall)
    return f1_score, recall, precision


def test_model_f1_score(batch_number = 1):
    """
    Tests the model f1 score
    :param batch_number: given batch to test
    :return:
    """
    class_label_iod_test_data = 0
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    # img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod_c0 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"
    class_label_iod_test_data = 1
    img_path_iod_c1 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"

    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/models/"
    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_"
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_"
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models=path_models, path_dataset=path_labeled,
                                                    model_name=model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models=path_models, path_dataset=path_labeled, model_name=model_name_ssdl)
    f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl = get_precision_recall_f1_score(learner_no_ssdl, img_path_iod_c0, img_path_iod_c1, im_size=110)
    f1_score_ssdl, recall_ssdl, precision_ssdl = get_precision_recall_f1_score(learner_ssdl,
                                                                                        img_path_iod_c0,
                                                                                        img_path_iod_c1, im_size=110)
    print("SSDL f1 score: ", f1_score_ssdl, " recall: ", recall_ssdl, " precision: ", precision_ssdl)
    print("No SSDL f1 score: ", f1_score_no_ssdl, " recall: ", recall_no_ssdl, " precision: ", precision_no_ssdl)


def test_model_f1_score_uncertainty_mcd(batch_number = 1, folder_models = "models_2", id_model = ""):
    """
    Gets f1 score and uncertainty using the MCD method
    :param batch_number: batch number to load
    :param folder_models: folder containing the models
    :param id_model: id of the model to load
    :return:
    """
    class_label_iod_test_data = 0
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    # img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod_c0 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"
    class_label_iod_test_data = 1
    img_path_iod_c1 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"

    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/" + folder_models + "/"
    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_"+  id_model
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_"+  id_model
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models=path_models, path_dataset=path_labeled,
                                                    model_name=model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models=path_models, path_dataset=path_labeled, model_name=model_name_ssdl)
    f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl = get_precision_recall_f1_score(learner_no_ssdl, img_path_iod_c0, img_path_iod_c1, im_size=110)
    f1_score_ssdl, recall_ssdl, precision_ssdl = get_precision_recall_f1_score(learner_ssdl,
                                                                                        img_path_iod_c0,
                                                                                        img_path_iod_c1, im_size=110)
    summary_name = "F1_SCORE_SUMMARY_BATCH_SSDL.csv"
    print("SSDL f1 score: ", f1_score_ssdl, " recall: ", recall_ssdl, " precision: ", precision_ssdl)
    create_classification_metrics_summary(summary_name, f1_score_ssdl, recall_ssdl, precision_ssdl)
    print("No SSDL f1 score: ", f1_score_no_ssdl, " recall: ", recall_no_ssdl, " precision: ", precision_no_ssdl)
    summary_name = "F1_SCORE_SUMMARY_BATCH_NO_SSDL.csv"
    create_classification_metrics_summary(summary_name, f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl)
    #Uncertainty estimation
    #NO SSDL MODEL
    #Class 0
    class_label_iod_test_data = 0
    _, uncertainties_correct_no_ssdl_iod_c0, uncertainties_wrong_no_ssdl_iod_c0 = calculate_uncertainty_mcd_fastai_images(
        learner_no_ssdl, img_path_iod_c0, is_ssdl=False, class_label=class_label_iod_test_data, im_size=110)

    #Class 1
    class_label_iod_test_data = 1
    _, uncertainties_correct_no_ssdl_iod_c1, uncertainties_wrong_no_ssdl_iod_c1 = calculate_uncertainty_mcd_fastai_images(
        learner_no_ssdl, img_path_iod_c1, is_ssdl=False, class_label=class_label_iod_test_data, im_size=110)

    # SSDL MODEL
    # Class 0
    class_label_iod_test_data = 0
    _, uncertainties_correct_ssdl_iod_c0, uncertainties_wrong_ssdl_iod_c0 = calculate_uncertainty_mcd_fastai_images(
        learner_ssdl, img_path_iod_c0, is_ssdl=True, class_label=class_label_iod_test_data, im_size=110)

    # Class 1
    class_label_iod_test_data = 1
    _, uncertainties_correct_ssdl_iod_c1, uncertainties_wrong_ssdl_iod_c1 = calculate_uncertainty_mcd_fastai_images(
        learner_ssdl, img_path_iod_c1, is_ssdl=True, class_label=class_label_iod_test_data, im_size=110)

    summary_name = "CHINA_BATCH_MCD_" + str(batch_number)
    print("Type ", type(uncertainties_correct_ssdl_iod_c0))
    create_summaries_csv_right_wrong(summary_name,
                         list_correct_ssdl = uncertainties_correct_ssdl_iod_c0 + uncertainties_correct_ssdl_iod_c1, list_wrong_ssdl = uncertainties_wrong_ssdl_iod_c0 + uncertainties_wrong_ssdl_iod_c1,
                         list_correct_no_ssdl = uncertainties_correct_no_ssdl_iod_c0 + uncertainties_correct_no_ssdl_iod_c1, list_wrong_no_ssdl = uncertainties_wrong_no_ssdl_iod_c0 + uncertainties_wrong_no_ssdl_iod_c1)



def test_model_f1_score_uncertainty_softmax(batch_number = 1, folder_models = "models_2", id_model = ""):
    """
    Gets f1 score and uncertainty using the Softmax method
    :param batch_number: batch number to load
    :param folder_models: folder containing the models
    :param id_model: id of the model to load
    :return:
    """
    class_label_iod_test_data = 0
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    # img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod_c0 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"
    class_label_iod_test_data = 1
    img_path_iod_c1 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"

    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/" +  folder_models + "/"
    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_" + id_model
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_" + id_model
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models=path_models, path_dataset=path_labeled,
                                                    model_name=model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models=path_models, path_dataset=path_labeled, model_name=model_name_ssdl)
    f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl = get_precision_recall_f1_score(learner_no_ssdl, img_path_iod_c0, img_path_iod_c1, im_size=110)
    f1_score_ssdl, recall_ssdl, precision_ssdl = get_precision_recall_f1_score(learner_ssdl,
                                                                                        img_path_iod_c0,
                                                                                        img_path_iod_c1, im_size=110)

    summary_name = "F1_SCORE_SUMMARY_BATCH_SSDL.csv"
    print("SSDL f1 score: ", f1_score_ssdl, " recall: ", recall_ssdl, " precision: ", precision_ssdl)
    create_classification_metrics_summary(summary_name, f1_score_ssdl, recall_ssdl, precision_ssdl)
    print("No SSDL f1 score: ", f1_score_no_ssdl, " recall: ", recall_no_ssdl, " precision: ", precision_no_ssdl)
    summary_name = "F1_SCORE_SUMMARY_BATCH_NO_SSDL.csv"
    create_classification_metrics_summary(summary_name, f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl)
    #Uncertainty estimation
    #NO SSDL MODEL
    #Class 0
    class_label_iod_test_data = 0
    _, uncertainties_correct_no_ssdl_iod_c0, uncertainties_wrong_no_ssdl_iod_c0 = calculate_uncertainty_softmax_fastai_images(
        learner_no_ssdl, img_path_iod_c0, is_ssdl=False, class_label=class_label_iod_test_data, im_size=110)

    #Class 1
    class_label_iod_test_data = 1
    _, uncertainties_correct_no_ssdl_iod_c1, uncertainties_wrong_no_ssdl_iod_c1 = calculate_uncertainty_softmax_fastai_images(
        learner_no_ssdl, img_path_iod_c1, is_ssdl=False, class_label=class_label_iod_test_data, im_size=110)

    # SSDL MODEL
    # Class 0
    class_label_iod_test_data = 0
    _, uncertainties_correct_ssdl_iod_c0, uncertainties_wrong_ssdl_iod_c0 = calculate_uncertainty_softmax_fastai_images(
        learner_ssdl, img_path_iod_c0, is_ssdl=True, class_label=class_label_iod_test_data, im_size=110)

    # Class 1
    class_label_iod_test_data = 1
    _, uncertainties_correct_ssdl_iod_c1, uncertainties_wrong_ssdl_iod_c1 = calculate_uncertainty_softmax_fastai_images(
        learner_ssdl, img_path_iod_c1, is_ssdl=True, class_label=class_label_iod_test_data, im_size=110)

    summary_name = "CHINA_BATCH_" + str(batch_number)
    print("Type ", type(uncertainties_correct_ssdl_iod_c0))
    create_summaries_csv_right_wrong(summary_name,
                         list_correct_ssdl = uncertainties_correct_ssdl_iod_c0 + uncertainties_correct_ssdl_iod_c1, list_wrong_ssdl = uncertainties_wrong_ssdl_iod_c0 + uncertainties_wrong_ssdl_iod_c1,
                         list_correct_no_ssdl = uncertainties_correct_no_ssdl_iod_c0 + uncertainties_correct_no_ssdl_iod_c1, list_wrong_no_ssdl = uncertainties_wrong_no_ssdl_iod_c0 + uncertainties_wrong_no_ssdl_iod_c1)

def create_classification_metrics_summary(summary_name, f1_score_ssdl, recall_ssdl, precision_ssdl):
    """
    Creates a summary of the classification metrics
    :param summary_name: name of the file
    :param f1_score_ssdl:  f1 score
    :param recall_ssdl: recall
    :param precision_ssdl: precision
    :return:
    """
    items = [f1_score_ssdl, recall_ssdl, precision_ssdl]
    df = pd.DataFrame(items)
    df.to_csv(summary_name, index=False)


def test_model_f1_score_uncertainty_duq(batch_number = 1, folder_models = "models_2", id_model = ""):
    """
    Gets f1 score and uncertainty using the DUQ method
    :param batch_number: batch number to load
    :param folder_models: folder containing the models
    :param id_model: id of the model to load
    :return:
    """
    class_label_iod_test_data = 0
    training_path_batch_centroids_base = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/train/"
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    # img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod_c0 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"
    class_label_iod_test_data = 1
    img_path_iod_c1 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"

    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/" +  folder_models + "/"
    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_" + id_model
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_" + id_model
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models=path_models, path_dataset=path_labeled,
                                                    model_name=model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models=path_models, path_dataset=path_labeled, model_name=model_name_ssdl)
    f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl = get_precision_recall_f1_score(learner_no_ssdl, img_path_iod_c0, img_path_iod_c1, im_size=110)
    f1_score_ssdl, recall_ssdl, precision_ssdl = get_precision_recall_f1_score(learner_ssdl,
                                                                                        img_path_iod_c0,
                                                                                        img_path_iod_c1, im_size=110)

    summary_name = "F1_SCORE_SUMMARY_BATCH_SSDL.csv"
    print("SSDL f1 score: ", f1_score_ssdl, " recall: ", recall_ssdl, " precision: ", precision_ssdl)
    create_classification_metrics_summary(summary_name, f1_score_ssdl, recall_ssdl, precision_ssdl)
    print("No SSDL f1 score: ", f1_score_no_ssdl, " recall: ", recall_no_ssdl, " precision: ", precision_no_ssdl)
    summary_name = "F1_SCORE_SUMMARY_BATCH_NO_SSDL.csv"
    create_classification_metrics_summary(summary_name, f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl)
    #Uncertainty estimation
    #NO SSDL MODEL
    #Class 0
    class_label_iod_test_data = 0
    _, uncertainties_correct_no_ssdl_iod_c0, uncertainties_wrong_no_ssdl_iod_c0 = duq.calculate_uncertainty_DUQ_images(learner_no_ssdl, img_path_iod_c0, training_path_batch_centroids_base, class_label = class_label_iod_test_data, im_size = 110)

    #Class 1
    class_label_iod_test_data = 1
    _, uncertainties_correct_no_ssdl_iod_c1, uncertainties_wrong_no_ssdl_iod_c1 = duq.calculate_uncertainty_DUQ_images(learner_no_ssdl, img_path_iod_c1, training_path_batch_centroids_base, class_label = class_label_iod_test_data, im_size = 110)

    # SSDL MODEL
    # Class 0
    class_label_iod_test_data = 0
    _, uncertainties_correct_ssdl_iod_c0, uncertainties_wrong_ssdl_iod_c0 = duq.calculate_uncertainty_DUQ_images(learner_ssdl, img_path_iod_c0, training_path_batch_centroids_base, class_label = class_label_iod_test_data, im_size = 110)

    # Class 1
    class_label_iod_test_data = 1
    _, uncertainties_correct_ssdl_iod_c1, uncertainties_wrong_ssdl_iod_c1 = duq.calculate_uncertainty_DUQ_images(learner_ssdl, img_path_iod_c1, training_path_batch_centroids_base, class_label = class_label_iod_test_data, im_size = 110)

    summary_name = "CHINA_BATCH_DUQ" + str(batch_number)
    print("Type ", type(uncertainties_correct_ssdl_iod_c0))
    create_summaries_csv_right_wrong(summary_name,
                         list_correct_ssdl = uncertainties_correct_ssdl_iod_c0 + uncertainties_correct_ssdl_iod_c1, list_wrong_ssdl = uncertainties_wrong_ssdl_iod_c0 + uncertainties_wrong_ssdl_iod_c1,
                         list_correct_no_ssdl = uncertainties_correct_no_ssdl_iod_c0 + uncertainties_correct_no_ssdl_iod_c1, list_wrong_no_ssdl = uncertainties_wrong_no_ssdl_iod_c0 + uncertainties_wrong_no_ssdl_iod_c1)


def test_model_f1_score_uncertainty_duq_ssdl(batch_number = 1, folder_models = "models_2", id_model = ""):
    """
    Gets f1 score and uncertainty using the DUQ method, for the SSDL model
    :param batch_number: batch number to load
    :param folder_models: folder containing the models
    :param id_model: id of the model to load
    :return:
    """
    class_label_iod_test_data = 0
    training_path_batch_centroids_base = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/train/"
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/"
    # img_path_iod = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/0_test_china/"
    img_path_iod_c0 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"
    class_label_iod_test_data = 1
    img_path_iod_c1 = "E:/GoogleDrive/DATASETS_TEMP/CHINA_DATASET/batch_" + str(batch_number) + "/test/" + str(
        class_label_iod_test_data) + "/"

    path_models = "C:/Users/Usuario/Desktop/HMS_UNCERTAINTY_TESTS/" +  folder_models + "/"
    model_name_no_ssdl = 'CHINA_NO_SSDL_model_batch_' + str(batch_number) + "_" + id_model
    model_name_ssdl = 'CHINA_SSDL_model_batch_' + str(batch_number) + "_" + id_model
    learner_no_ssdl, data_full = load_no_ssdl_model(path_models=path_models, path_dataset=path_labeled,
                                                    model_name=model_name_no_ssdl)
    learner_ssdl = load_ssdl_model(path_models=path_models, path_dataset=path_labeled, model_name=model_name_ssdl)
    f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl = get_precision_recall_f1_score(learner_no_ssdl, img_path_iod_c0, img_path_iod_c1, im_size=110)
    f1_score_ssdl, recall_ssdl, precision_ssdl = get_precision_recall_f1_score(learner_ssdl,
                                                                                        img_path_iod_c0,
                                                                                        img_path_iod_c1, im_size=110)

    summary_name = "F1_SCORE_SUMMARY_BATCH_SSDL.csv"
    print("SSDL f1 score: ", f1_score_ssdl, " recall: ", recall_ssdl, " precision: ", precision_ssdl)
    create_classification_metrics_summary(summary_name, f1_score_ssdl, recall_ssdl, precision_ssdl)
    print("No SSDL f1 score: ", f1_score_no_ssdl, " recall: ", recall_no_ssdl, " precision: ", precision_no_ssdl)
    summary_name = "F1_SCORE_SUMMARY_BATCH_NO_SSDL.csv"
    create_classification_metrics_summary(summary_name, f1_score_no_ssdl, recall_no_ssdl, precision_no_ssdl)
    #Uncertainty estimation

    # SSDL MODEL
    # Class 0
    class_label_iod_test_data = 0
    _, uncertainties_correct_ssdl_iod_c0, uncertainties_wrong_ssdl_iod_c0 = duq.calculate_uncertainty_DUQ_images(learner_ssdl, img_path_iod_c0, training_path_batch_centroids_base, class_label = class_label_iod_test_data, im_size = 110)

    # Class 1
    class_label_iod_test_data = 1
    _, uncertainties_correct_ssdl_iod_c1, uncertainties_wrong_ssdl_iod_c1 = duq.calculate_uncertainty_DUQ_images(learner_ssdl, img_path_iod_c1, training_path_batch_centroids_base, class_label = class_label_iod_test_data, im_size = 110)

    print("UNCERTAINTIES CORRECT SSDL c1")
    print(uncertainties_correct_ssdl_iod_c1)
    print("UNCERTAINTIES INCORRECT SSDL c1")
    print(uncertainties_wrong_ssdl_iod_c1)
    print("UNCERTAINTIES CORRECT SSDL c0")
    print(uncertainties_correct_ssdl_iod_c0)
    print("UNCERTAINTIES INCORRECT SSDL c0")
    print(uncertainties_wrong_ssdl_iod_c0)

if __name__ == '__main__':
    #test_model_f1_score_uncertainty_duq(batch_number = 0, folder_models="weights_70_labels", id_model="")
    #test_model_accuracy(batch_number = 1, class_label_iod_test_data = 0)
    #test_model_accuracy(batch_number=1, class_label_iod_test_data=1)
    for i in range(0, 10):
        test_model_f1_score_uncertainty_duq(batch_number = i, folder_models="weights_100_labels", id_model="")


