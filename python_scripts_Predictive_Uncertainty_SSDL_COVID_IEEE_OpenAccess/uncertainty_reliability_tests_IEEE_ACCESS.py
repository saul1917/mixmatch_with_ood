import pandas as pd
import torch
import time
import numpy as np
import torchvision.models as models3
from scipy.stats import entropy
from scipy.spatial import distance
import matplotlib
import matplotlib.pyplot as plt
sys.path.insert(0, '../uncertainty_estimation/')
from metrics_reliability_uncertainty import *



def run_mcd_test(num_labels = 100):
    """
    Runs MCD test, IEEE Access paper
    :param num_labels: number of labels for the tested model
    :return:
    """
    results_folder = "C:/Users/Usuario/Dropbox/UNIVERSIDAD/DMU/DMU_THESIS_SAUL/PapersDrafts/UncertaintyExplainability/SUMMARIES_UNCERTAINTY/RESULTS_MCD_$_LABELS_BALANCED_DATA"
    base_file_name ="/CHINA_BATCH_MCD_$#_uncertainty_wrong_correct.csv"
    correct_tensor_all_tensor_ssdl, wrong_tensor_all_tensor_ssdl, correct_tensor_all_tensor_no_ssdl, wrong_tensor_all_tensor_no_ssdl, js_dist, hist1_correct_ssdl, hist2_wrong_ssdl, bucks1_correct_ssdl, hist1_correct_no_ssdl, hist2_wrong_no_ssdl, bucks1_correct_no_ssdl = calculate_stats(
        results_folder, base_file_name, num_batches=10, number_labels=num_labels)
    plot_histograms(bucks1_correct_ssdl, hist1_correct_ssdl, bucks1_correct_ssdl, hist2_wrong_ssdl,
                    plot_name="plot_mcd_ssdl_" + str(num_labels) + ".pdf", )
    plot_histograms(bucks1_correct_no_ssdl, hist1_correct_no_ssdl, bucks1_correct_no_ssdl, hist2_wrong_no_ssdl,
                    plot_name="plot_mcd_no_ssdl_" + str(num_labels) + ".pdf", )
    x_plot_uncertainty_no_ssdl, y_plot_acc_no_ssdl, y_plot_acc_no_ssdl_std = calculate_percentiles(correct_tensor_all_tensor_no_ssdl,
                                                                           wrong_tensor_all_tensor_no_ssdl)
    x_plot_uncertainty_ssdl, y_plot_acc_ssdl, y_plot_acc_ssdl_std = calculate_percentiles(correct_tensor_all_tensor_ssdl,
                                                                     wrong_tensor_all_tensor_ssdl)
    plot_pair_functions(x_plot_uncertainty_no_ssdl, y_plot_acc_no_ssdl, y_plot_acc_no_ssdl_std, x_plot_uncertainty_ssdl, y_plot_acc_ssdl, y_plot_acc_ssdl_std,
                        plot_name="plot_acc_curve_mcd_ssdl_vs_no_ssdl_" + str(num_labels) + ".pdf")



def run_softmax_test(num_labels = 30):
    """
    Runs Softmax test, IEEE Access paper
    :param num_labels:
    :return:
    """
    results_folder = "C:/Users/Usuario/Dropbox/UNIVERSIDAD/DMU/DMU_THESIS_SAUL/PapersDrafts/UncertaintyExplainability/SUMMARIES_UNCERTAINTY/RESULTS_SOFTMAX_$_LABELS_BALANCED_DATA"
    base_file_name ="/CHINA_BATCH_$#_uncertainty_wrong_correct.csv"
    correct_tensor_all_tensor_ssdl, wrong_tensor_all_tensor_ssdl, correct_tensor_all_tensor_no_ssdl, wrong_tensor_all_tensor_no_ssdl, js_dist, hist1_correct_ssdl, hist2_wrong_ssdl, bucks1_correct_ssdl, hist1_correct_no_ssdl, hist2_wrong_no_ssdl, bucks1_correct_no_ssdl = calculate_stats(results_folder, base_file_name, num_batches = 10, number_labels = num_labels)
    plot_histograms(bucks1_correct_ssdl, hist1_correct_ssdl, bucks1_correct_ssdl, hist2_wrong_ssdl,
                    plot_name="plot_softmax_ssdl_" + str(num_labels) + ".pdf", )
    plot_histograms(bucks1_correct_no_ssdl, hist1_correct_no_ssdl, bucks1_correct_no_ssdl, hist2_wrong_no_ssdl,
                    plot_name="plot_softmax_no_ssdl_" + str(num_labels) + ".pdf", )
    #x_plot_uncertainty_no_ssdl, y_plot_acc_no_ssdl, y_plot_acc_no_ssdl_std = calculate_percentiles(correct_tensor_all_tensor_no_ssdl, wrong_tensor_all_tensor_no_ssdl)
    #x_plot_uncertainty_ssdl, y_plot_acc_ssdl, y_plot_acc_ssdl_std = calculate_percentiles(correct_tensor_all_tensor_ssdl, wrong_tensor_all_tensor_ssdl)
    #plot_pair_functions(x_plot_uncertainty_no_ssdl, y_plot_acc_no_ssdl, y_plot_acc_no_ssdl_std, x_plot_uncertainty_ssdl, y_plot_acc_ssdl, y_plot_acc_ssdl_std,
    #                    plot_name="plot_acc_curve_softmax_ssdl_vs_no_ssdl_" + str(num_labels) + ".pdf")



def run_duq_test(num_labels = 30):
    """
    Runs DUQ test, IEEE Access paper
    :param num_labels:
    :return:
    """
    results_folder = "C:/Users/Usuario/Dropbox/UNIVERSIDAD/DMU/DMU_THESIS_SAUL/PapersDrafts/UncertaintyExplainability/SUMMARIES_UNCERTAINTY/RESULTS_DUQ_$_LABELS_BALANCED_DATA"
    base_file_name ="/CHINA_BATCH_DUQ$#_uncertainty_wrong_correct.csv"
    correct_tensor_all_tensor_ssdl, wrong_tensor_all_tensor_ssdl, correct_tensor_all_tensor_no_ssdl, wrong_tensor_all_tensor_no_ssdl, js_dist, hist1_correct_ssdl, hist2_wrong_ssdl, bucks1_correct_ssdl, hist1_correct_no_ssdl, hist2_wrong_no_ssdl, bucks1_correct_no_ssdl = calculate_stats(
        results_folder, base_file_name, num_batches=10, number_labels=num_labels)
    plot_histograms(bucks1_correct_ssdl, hist1_correct_ssdl, bucks1_correct_ssdl, hist2_wrong_ssdl,
                    plot_name="plot_duq_ssdl_" + str(num_labels) + ".pdf", )
    plot_histograms(bucks1_correct_no_ssdl, hist1_correct_no_ssdl, bucks1_correct_no_ssdl, hist2_wrong_no_ssdl,
                    plot_name="plot_duq_no_ssdl_" + str(num_labels) + ".pdf", )
    x_plot_uncertainty_no_ssdl, y_plot_acc_no_ssdl, y_plot_acc_no_ssdl_std = calculate_percentiles(correct_tensor_all_tensor_no_ssdl,
                                                                           wrong_tensor_all_tensor_no_ssdl)
    x_plot_uncertainty_ssdl, y_plot_acc_ssdl, y_plot_acc_ssdl_std = calculate_percentiles(correct_tensor_all_tensor_ssdl,
                                                                     wrong_tensor_all_tensor_ssdl)
    plot_pair_functions(x_plot_uncertainty_no_ssdl, y_plot_acc_no_ssdl, y_plot_acc_no_ssdl_std, x_plot_uncertainty_ssdl, y_plot_acc_ssdl, y_plot_acc_ssdl_std,
                        plot_name="plot_acc_curve_duq_ssdl_vs_no_ssdl_" + str(num_labels) + ".pdf")


def run_all():
    num_labels = [20, 30, 60, 70, 100]
    for i in num_labels:
       # run_duq_test(num_labels = i)
       # time.sleep(1)
       # run_mcd_test(num_labels = i)
       # time.sleep(1)
        run_softmax_test(num_labels = i)
       # time.sleep(1)

if __name__ == '__main__':
    run_all()
