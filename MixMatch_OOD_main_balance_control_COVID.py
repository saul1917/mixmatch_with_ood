from torchvision import models, transforms
import utilities.metrics as metrics
from PIL import Image as Pili
import re
from fastai.vision import *
from fastai.metrics import *
from fastai.callbacks import CSVLogger
from numbers import Integral
import torch
import logging
import sys
from torchvision.utils import save_image
import numpy as np
from utilities.run_context import RunContext
import utilities.cli as cli
import torchvision
import timeit
import time

def pil2fast(img, im_size = 110):
    """
    Converts a pil image to fastAi image
    :param img: pil input image
    :param im_size: desired size to convert
    :return: FastAI Image
    """
    data_transform = transforms.Compose(
        [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
    return Image(data_transform(img))



class MultiTransformLabelList(LabelList):
    def __getitem__(self, idxs: Union[int, np.ndarray]) -> 'LabelList':
        """
        Create K  transformed images for the unlabeled data
        :param idxs:
        :return:return a single (x, y) if `idxs` is an integer or a new `LabelList` object if `idxs` is a range.
        """
        #list of arguments, managed as a global variable
        global args
        idxs = try_int(idxs)
        if isinstance(idxs, Integral):
            if self.item is None:
                #CALLED EVEN FOR UNLABELED DATA, Y IS USED!, but it does not have any value
                x, y = self.x[idxs], self.y[idxs]
            else:
                x, y = self.item, 0
            if self.tfms or self.tfmargs:
                #THIS IS DONE FOR UNLABELED DATA
                # I've changed this line to return a list of augmented images
                x = [x.apply_tfms(self.tfms, **self.tfmargs) for _ in range(args.K_transforms)]
            if hasattr(self, 'tfms_y') and self.tfm_y and self.item is None:
                #Augments the labels
                #IS NOT CALLED FOR UNLABELED DATA
                y = y.apply_tfms(self.tfms_y, **{**self.tfmargs_y, 'do_resolve': False})
            if y is None: y = 0
            return x, y
        else:
            return self.new(self.x[idxs], self.y[idxs])



def MixmatchCollate(batch):
    """
    Stacks the data
    # I'll also need to change the default collate function to accomodate multiple augments
    :param batch:
    :return:
    """
    batch = to_data(batch)
    if isinstance(batch[0][0], list):
        batch = [[torch.stack(s[0]), s[1]] for s in batch]
    return torch.utils.data.dataloader.default_collate(batch)





class MixupLoss(nn.Module):
    """
    Implements the mixup loss
    """

    def forward(self, preds, target, unsort=None, ramp=None, bs=None):
        """
        Forward pass of the loss function
        Ramp, unsort and bs is None when doing validation
        :param preds: Model's predictions
        :param target: Targets of the input observations
        :param unsort:ab
        :param ramp:w  value used to weigh the unlabeled loss
        :param bs: batchsize
        :return: the evaluated loss
        """
        global args, class_weights

        if(args.balanced==5):
            #The PBC is meant to be used, implemented in the function forward_balanced_cross_entropy
            return self.forward_balanced_cross_entropy(preds, target, unsort, ramp, bs)
        else:
            #No weight correction according to the labels
            #assign the same weight for the classes, in disregard with the dataset
            weight = 1 / len(class_weights)
            for i in range(0, len(class_weights)):
                class_weights[i] = weight
            #i.e, class_weights = torch.tensor([0.3333, 0.3333, 0.3333], device ="cuda:0") for 3 classes
            return self.forward_balanced_cross_entropy(preds, target, unsort, ramp, bs)




    def forward_balanced_cross_entropy(self, preds, target, unsort=None, ramp=None, bs=None):
        """
        Implements the PBC method proposed for imbalance correction, and tested with COVID-19 and mammogram data
       :param preds: Model's predictions
        :param target: Targets of the input observations
        :param unsort:ab
        :param ramp:w  value used to weigh the unlabeled loss
        :param bs: batchsize
        :return: the evaluated loss
        """
        global args, class_weights
        if unsort is None:
            return F.cross_entropy(preds, target)
        #weights_labeled = self.get_weights_observations(target[:bs]).float()
        weights_unlabeled = self.get_weights_observations(target[bs:]).float()
        calculate_cross_entropy = nn.CrossEntropyLoss(weight = class_weights.float())
        preds = preds[unsort]
        preds_l = preds[:bs]
        preds_ul = preds[bs:]
        # calculate log of softmax, to ensure correct usage of cross entropy
        # one column per class, one batch per row
        preds_ul = torch.softmax(preds_ul, dim=1)
        # TARGETS CANNOT BE 1-K ONE HOT VECTOR
        (highest_values, highest_classes) = torch.max(target[:bs], 1)
        highest_classes = highest_classes.long()
        loss_x = calculate_cross_entropy(preds_l, highest_classes)
        loss_u = F.mse_loss(weights_unlabeled * preds_ul, weights_unlabeled * target[bs:])
        self.loss_x = loss_x.item()
        self.loss_u = loss_u.item()
        return loss_x + args.lambda_unsupervised * ramp * loss_u




    def get_weights_observations(self, array_labels):
        """
        Calculates the weights according to the array of labels received
        :param array_labels: array of labels received (real targets)
        :return: returns the weights observation wise
        """
        global class_weights

        # each column is a class, each row an observation
        num_classes = array_labels.shape[1]
        num_observations = array_labels.shape[0]
        (highest_values, highest_classes) = torch.max(array_labels, 1)
        # turn the highest_classes array a column vector
        highest_classes_col = highest_classes.view(-1, 1)
        # highest classes for all the observations (rows) and classes (columns)
        highest_classes_all = highest_classes_col.repeat(1, num_classes)

        # scores all
        scores_all = class_weights[highest_classes_all]
        scores_all.to(device="cuda:0")
        return scores_all



class MixMatchImageList(ImageList):


    """
    Custom ImageList with filter function
    """
    def filter_train(self, num_items, seed = 23488):
        """
        Takes a number of observations as labeled, assumes that the evaluation observations are in the test folder
        :param num_items: total number of items
        :param seed: The seed is fixed for reproducibility
        :return: return the filtering function by itself
        """
        global args
        path_unlabeled = args.path_unlabeled
        if (args.path_unlabeled == ""):
          path_unlabeled = args.path_labeled
        #this means that a customized unlabeled dataset is not to be used, just pick the rest of the labelled data as unlabelled
        if(path_unlabeled == args.path_labeled):
          train_idxs = np.array([i for i, observation in enumerate(self.items) if Path(observation).parts[-3] != "test"])
          #valid_idxs = np.array([i for i, observation in enumerate(self.items) if Path(observation).parts[-3] == "test"])
        else:
          # IGNORE THE DATA ALREADY IN THE UNLABELED DATASET
          dataset_unlabeled =  torchvision.datasets.ImageFolder(path_unlabeled + "/train/")
          list_file_names_unlabeled = dataset_unlabeled.imgs
          for i in range(0, len(list_file_names_unlabeled)):
            #delete root of path
            list_file_names_unlabeled[i] = list_file_names_unlabeled[i][0].replace(path_unlabeled, "")
          list_train = []
          #add  to train if is not in the unlabeled dataset
          for i, observation in enumerate(self.items):
            path_1 = str(Path(observation))
            sub_str = args.path_labeled
            path_2 = path_1.replace(sub_str, "")
            path_2 = path_2.replace("train/", "")
            is_path_in_unlabeled = path_2 in list_file_names_unlabeled
            #add the observation to the train list, if is not in the unlabeled dataset
            if( not "test" in path_2 and not is_path_in_unlabeled):
              list_train += [i]
          #store the train idxs c
          train_idxs = np.array(list_train)            
          logger.info("Customized number of unlabeled observations " + str(len(list_file_names_unlabeled)))
          
        valid_idxs = np.array([i for i, observation in enumerate(self.items) if Path(observation).parts[-3] == "test"])
        #return []
        # for reproducibility
        np.random.seed(seed)
        # keep the number of items desired, 500 by default
        keep_idxs = np.random.choice(train_idxs, num_items, replace=False)
        
        logger.info("Number of labeled observations: " + str(len(keep_idxs)))
        logger.info("First labeled id: " + str(keep_idxs[0]))
        logger.info("Number of validation observations: " + str(len(valid_idxs)))
        logger.info("Number of training observations " + str(len(train_idxs)))
        self.items = np.array([o for i, o in enumerate(self.items) if i in np.concatenate([keep_idxs, valid_idxs])])
        return self


    def undersample_dataset(self, items_per_class, desired_proportions, num_items):
        """
        Undersample the dataset according to the desired proportions
        :param items_per_class: number of observations per class
        :param desired_proportions: desired proportions by user
        :param num_items: total number of items
        :return: balanced set of items
        """
        # overrepresented class
        min_num_obs_class = int(num_items * min(desired_proportions))
        all_items = []
        # for each class, define the number of observations to oversample
        # all the classes must have the same number of observations, from the under represented class
        for i in range(0, len(desired_proportions)):
            # pick the items as usual for the class
            num_items_class_i = min_num_obs_class
            items_class_i = items_per_class[i]

            keep_idxs_i = np.random.choice(np.array(items_class_i), num_items_class_i, replace=False)
            all_items += keep_idxs_i.tolist()
            logger.info("Selected items for class " + str(i))
            logger.info(str(keep_idxs_i))


        return all_items

    def oversample_dataset(self, items_per_class, desired_proportions, num_items):
        """
        Oversample the dataset accordig to the desired proportions
        :param items_per_class: number of observations per class
        :param desired_proportions: desired proportions by user
        :param num_items: total number of items
        :return: balanced set of items
        """
        # overrepresented class
        max_num_obs_class = int(num_items * max(desired_proportions))
        all_items = []
        # for each class, define the number of observations to oversample
        for i in range(0, len(desired_proportions)):
            # pick the items as usual for the class
            num_items_class_i = int(desired_proportions[i] * num_items)
            items_class_i = items_per_class[i]

            keep_idxs_i = np.random.choice(np.array(items_class_i), num_items_class_i, replace=False)
            # oversample
            num_items_to_oversample = max_num_obs_class - num_items_class_i

            all_items += keep_idxs_i.tolist()

            logger.info("Selected items for class " + str(i))
            logger.info("Selected items for class " + str(keep_idxs_i))
            #print(keep_idxs_i.shape)
            if (num_items_to_oversample > 0):
                keep_idxs_i_oversampled = np.random.choice(keep_idxs_i, num_items_to_oversample, replace=True)
                logger.info("Oversampled observations " + str(i))
                logger.info(str(keep_idxs_i_oversampled))
            all_items += keep_idxs_i_oversampled.tolist()

        return all_items

    def filter_train_balance_control(self, num_items, seed=23488, desired_proportions = []):
        """
        Takes a number of observations as labeled, assumes that the evaluation observations are in the test folder, preserving desired proportions
        :param num_items: total number of items
        :param seed: The seed is fixed for reproducibility
        :return: return the filtering function by itself
        """

        global args, class_weights
        # for reproducibility
        np.random.seed(seed)
        num_items_per_class = []
        logger.info("Oversample? " + str(args.oversample))
        #create a list of lists with the items per class
        items_per_class = [[] for _ in range(len(desired_proportions))]

        #calculate the number of items per class
        for i in range(0, len(desired_proportions)):
            num_items_per_class += [int(desired_proportions[i] * num_items) ]
        #get label dictionary
        #logger.info("Number items per class ", num_items_per_class )
        label_dictionary = self.get_labels_dict()
        path_unlabeled = args.path_unlabeled
        if (args.path_unlabeled == ""):
            path_unlabeled = args.path_labeled
        # this means that a customized unlabeled dataset is not to be used, just pick the rest of the labelled data as unlabelled
        if (path_unlabeled == args.path_labeled):
            train_idxs_all_list = []
            for i, observation in enumerate(self.items):
                if (Path(observation).parts[-3] != "test"):
                    path_1 = str(Path(observation))
                    train_idxs_all_list += [i]
                    substr_train = re.findall(r"/\d+/", path_1)
                    label_num_str = re.findall(r"\d+", substr_train[0])
                    label = int(label_num_str[0])
                    proxy_label = label_dictionary[label]

                    # add the element to the corresponding sub list of observations for this class, according to label
                    items_per_class[proxy_label] += [i]
        else:
            # IGNORE THE DATA ALREADY IN THE UNLABELED DATASET
            dataset_unlabeled = torchvision.datasets.ImageFolder(path_unlabeled + "/train/")
            list_file_names_unlabeled = dataset_unlabeled.imgs
            for i in range(0, len(list_file_names_unlabeled)):
                # delete root of path
                list_file_names_unlabeled[i] = list_file_names_unlabeled[i][0].replace(path_unlabeled, "")

            list_train = []
            # add  to train if is not in the unlabeled dataset

            for i, observation in enumerate(self.items):
                path_1 = str(Path(observation))
                sub_str = args.path_labeled
                path_2 = path_1.replace(sub_str, "")
                path_2 = path_2.replace("train/", "")
                is_path_in_unlabeled = path_2 in list_file_names_unlabeled
                # add the observation to the train list, if is not in the unlabeled dataset
                if (not "test" in path_2 and not is_path_in_unlabeled):
                    list_train += [i]
                    #get substring with train and class folder
                    substr_train = re.findall(r"/\d+/", path_2)
                    label_num_str = re.findall(r"\d+", substr_train[0])
                    label = int(label_num_str[0])
                    proxy_label = label_dictionary[label]
                    #add the element to the corresponding sub list of observations for this class, according to label
                    items_per_class[proxy_label] += [i]
            logger.info("Customized number of unlabeled  observations " + str(len(list_file_names_unlabeled)))
        #concat all the observations
        keep_idxs_all = []
        if (args.oversample):
            logger.info("Oversampling the dataset...")
            keep_idxs_all  = self.oversample_dataset(items_per_class, desired_proportions, num_items)
        elif (args.undersample):
            logger.info("Undersampling the dataset...")
            keep_idxs_all  = self.undersample_dataset(items_per_class, desired_proportions, num_items)
        else:
            
            for i in range(0, len(desired_proportions)):
                #for each class, select the given number of random labels
                items_class_i = items_per_class[i]
                keep_idxs_i = np.random.choice(np.array(items_class_i), num_items_per_class[i], replace=False)
                keep_idxs_all += keep_idxs_i.tolist()

        keep_idxs_all_np = np.array(keep_idxs_all)


        #the test dataset is done when building the folder
        valid_idxs = np.array([i for i, observation in enumerate(self.items) if Path(observation).parts[-3] == "test"])
        logger.info("Number of labeled observations: " + str(len(keep_idxs_all_np)))
        logger.info("First labeled id: " + str(keep_idxs_all_np[0]))
        logger.info("Number of  validation observations: " + str(len(valid_idxs)))
        logger.info("Number  of training observations " + str(len(keep_idxs_all_np)))
        self.items = np.array([o for i, o in enumerate(self.items) if i in np.concatenate([keep_idxs_all_np, valid_idxs])])
        return self




    def get_labels_dict(self):
        """
        Get the dictionary with the labels
        :return: the dictionary with the labels
        """
        proxy_label_counter = 0
        dictionary = {-1:0}
        for i, observation in enumerate(self.items):
            if (Path(observation).parts[-3] != "test"):
                path_1 = str(Path(observation))
                substr_train = re.findall(r"/\d+/", path_1)
                label_num_str = re.findall(r"\d+", substr_train[0])
                label = int(label_num_str[0])
                #if the element does not exist, add it
                try:
                    a = dictionary[label]
                except:
                    dictionary[label] = proxy_label_counter
                    proxy_label_counter += 1
        return dictionary


class MixMatchTrainer(LearnerCallback):
    """
    Mix match trainer functions
    """
    #order = -20

    def on_train_begin(self, **kwargs):
        """
        Callback used when the trainer is beginning, inits variables
        :param kwargs:
        :return:
        """
        global data_labeled
        self.l_dl = iter(data_labeled.train_dl)
        #metrics recorder
        self.smoothL, self.smoothUL = SmoothenValue(0.98), SmoothenValue(0.98)
        self.it = 0

    def mixup(self, a_x, a_y, b_x, b_y):
        """
        Mixup augments data by mixing labels and pseudo labels and its observations
        :param a_x: observation a
        :param a_y: observation a's label
        :param b_x: observation b
        :param b_y: observation b's label
        :param alpha: distribution control coefficient
        :return:
        """
        global args
        alpha = args.alpha_mix
        l = np.random.beta(alpha, alpha)
        l = max(l, 1 - l)
        x = l * a_x + (1 - l) * b_x
        y = l * a_y + (1 - l) * b_y
        return x, y

    def sharpen(self, p):
        global args
        """
        Sharpens the distribution output, to encourage confidence
        :param p: output label distribution estimation
        :param T: temperature parameter
        :return:
        """
        T = args.T_sharpening
        u = p ** (1 / T)
        return u / u.sum(dim=1, keepdim=True)

    def on_batch_begin(self, train, last_input, last_target, **kwargs):
        """
        Called on batch training at the begining
        :param train: training dataset
        :param last_input: last received input
        :param last_target: last received target
        :return:
        """
        global data_labeled, args
        if not train: return
        try:
            x_l, y_l = next(self.l_dl)
        except:
            self.l_dl = iter(data_labeled.train_dl)
            x_l, y_l = next(self.l_dl)
        #print!
        x_ul = last_input
        with torch.no_grad():
            #calculates the pseudo sharpened labels
            ul_labels = self.sharpen(
                torch.softmax(torch.stack([self.learn.model(x_ul[:, i]) for i in range(x_ul.shape[1])], dim=1),
                              dim=2).mean(dim=1))
        #create torch array of unlabeled data
        x_ul = torch.cat([x for x in x_ul])
        #WE CAN CALCULATE HERE THE CONFIDENCE COEFFICIENT, if needed

        ul_labels = torch.cat([y.unsqueeze(0).expand(args.K_transforms, -1) for y in ul_labels])

        l_labels = torch.eye(data_labeled.c).cuda()[y_l]

        w_x = torch.cat([x_l, x_ul])
        w_y = torch.cat([l_labels, ul_labels])
        idxs = torch.randperm(w_x.shape[0])
        #create mixed input and targets, using the mixup algorithm
        mixed_input, mixed_target = self.mixup(w_x, w_y, w_x[idxs], w_y[idxs])
        bn_idxs = torch.randperm(mixed_input.shape[0])
        unsort = [0] * len(bn_idxs)
        for i, j in enumerate(bn_idxs): unsort[j] = i
        mixed_input = mixed_input[bn_idxs]

        ramp = self.it / args.rampup_coefficient if self.it < args.rampup_coefficient else 1.0
        return {"last_input": mixed_input, "last_target": (mixed_target, unsort, ramp, x_l.shape[0])}

    def on_batch_end(self, train, **kwargs):
        """
        Add the metrics at the end of the batch training
        :param train:
        :param kwargs:
        :return:
        """
        if not train: return
        self.smoothL.add_value(self.learn.loss_func.loss_x)
        self.smoothUL.add_value(self.learn.loss_func.loss_u)
        self.it += 1


        
def get_dataset_stats(args):
    """
    Calculates the dataset stats for estandarization
    :param args: arguments to use
    :return:
    """
    # meanDatasetComplete = [0.485, 0.456, 0.406],
    # stdDatasetComplete = [0.229, 0.224, 0.225]
    #some frequently used dataset stats
    #
    logger.info("Calculating dataset stats for the labelled data: ")
    (meanDatasetComplete_labeled, stdDatasetComplete_labeled) = metrics.calculate_mean_std(args.path_labeled + "/train/")


    logger.info("Calculating dataset stats for the unlabelled data: ")
    if(args.mode == "ssdl" and args.path_unlabeled  != ""):
        (meanDatasetComplete_unlabeled, stdDatasetComplete_unlabeled) = metrics.calculate_mean_std(args.path_unlabeled)
    else:
        (meanDatasetComplete_unlabeled, stdDatasetComplete_unlabeled) = (meanDatasetComplete_labeled, stdDatasetComplete_labeled)
    
    return (meanDatasetComplete_labeled, stdDatasetComplete_labeled, meanDatasetComplete_unlabeled, stdDatasetComplete_unlabeled)




def calculate_weights(list_labels):
    """
    Calculate the class weights according to the number of observations, used to implement the PBC
    :param list_labels: list of labels for the labelled dataset
    :return: per class weights
    """
    global logger, args
    array_labels = np.array(list_labels)
    logger.info("Using balanced loss: " + str(args.balanced))
    list_classes = np.unique(array_labels)

    weight_classes = np.zeros(len(list_classes))
    for curr_class in list_classes:

        number_observations_class = len(array_labels[array_labels == curr_class])
        logger.info("Number observations " + str(number_observations_class) + " for class " + str(curr_class))
        weight_classes[curr_class] = 1 / number_observations_class

    weight_classes = weight_classes / weight_classes.sum()
    #CAREFUL!!!!!!!
    #weight_classes = torch.tensor([0.3333, 0.3333, 0.3333])
    logger.info("Weights to use: " + str(weight_classes))
    weight_classes_tensor = torch.tensor(weight_classes, device ="cuda:0" )
    return weight_classes_tensor

def get_datasets():
    """
    Gets  datasets, according to args received   (FAST AI data bunches ) for labeled, unlabeled and validation
    :return: data_labeled (limited labeled data), data_unlabeled , data_full (complete labeled dataset)
    """
    global desired_labeled_classes_dist
    global args, data_labeled, logger, class_weights
    path_labeled = args.path_labeled
    path_unlabeled = args.path_unlabeled
    if (args.path_unlabeled == ""):
        path_unlabeled = path_labeled
    #get dataset mean and std
    (meanDatasetComplete_labeled, stdDatasetComplete_labeled, meanDatasetComplete_unlabeled, stdDatasetComplete_unlabeled) = get_dataset_stats(args)
    logger.info("Loading labeled data from: " + path_labeled)
    logger.info("Loading unlabeled data from: " + path_unlabeled)
    # Create two databunch objects for the labeled and unlabled images. A fastai databunch is a container for train, validation, and
    # test dataloaders which automatically processes transforms and puts the data on the gpu.
    # https://docs.fast.ai/vision.transform.html
    data_labeled = (MixMatchImageList.from_folder(path_labeled)
                    .filter_train_balance_control(args.number_labeled, seed = 4200, desired_proportions = desired_labeled_classes_dist)  # Use 500 labeled images for traning
                    .split_by_folder(valid="test")  # test on all 10000 images in test set
                    .label_from_folder()
                    .transform(get_transforms(do_flip = True, flip_vert = True, max_zoom=1, max_warp=None, p_affine=0, p_lighting = 0),
                               size=args.size_image)
                    # On windows, must set num_workers=0. Otherwise, remove the argument for a potential performance improvement
                    .databunch(bs=args.batch_size, num_workers=args.workers)
                    .normalize((meanDatasetComplete_labeled, stdDatasetComplete_labeled)))


    # normalize_funcs(mean:FloatTensor, std:FloatTensor, do_x:bool=True, do_y:bool=False)
    train_set = set(data_labeled.train_ds.x.items)
    #get the list of labels for the dataset
    list_labels = data_labeled.train_ds.y.items
    #calculate the class weights
    class_weights = calculate_weights(list_labels)
    # load the unlabeled data
    #filter picks the labeled images not contained in the unlabeled dataset, in the case of SSDL
    #the test set is in the unlabeled folder!!!!
    src = (ImageList.from_folder(path_unlabeled)
           .filter_by_func(lambda x: x not in train_set)
           .split_by_folder(valid="test")
           )

    #AUGMENT THE DATA
    src.train._label_list = MultiTransformLabelList
    # https://docs.fast.ai/vision.transform.html
    # data not in the train_set and splitted by test folder is used as unlabeled
    #WHY DOES IT NEED THE LABEL FROM FOLDER???? R/ THAT INFORMATION IS NOT USED. IS JUST FOR USING THE FAST AI LABELER
    data_unlabeled = (src.label_from_folder()
                      .transform(get_transforms(do_flip = True, flip_vert = True, max_zoom=1, max_warp=None, p_affine=0, p_lighting = 0), size=args.size_image)
                      .databunch(bs=args.batch_size, collate_fn=MixmatchCollate, num_workers=10)
                      .normalize((meanDatasetComplete_unlabeled, stdDatasetComplete_unlabeled)))
    logger.info("Information for unlabeled training data: ")
    list_labels_unlabeled = data_unlabeled.train_ds.y.items
    calculate_weights(list_labels_unlabeled)
    logger.info("Information for validation data: ")
    # Databunch with all 50k images labeled, for baseline
    data_full = (ImageList.from_folder(path_labeled)
                 .split_by_folder(valid="test")
                 .label_from_folder()
                 .transform(get_transforms(do_flip = True, flip_vert = True, max_zoom=1, max_warp=None, p_affine=0, p_lighting = 0),
                            size=args.size_image)
                 .databunch(bs=args.batch_size, num_workers=args.workers)
                 .normalize((meanDatasetComplete_unlabeled, stdDatasetComplete_unlabeled)))
    return (data_labeled, data_unlabeled, data_full)


def test_output(learn):
    """
    Debuggin purposes
    :param learn:
    :return:
    """
    im_size = 110
    img_path_ood = "/media/Data/saul/Datasets/Inbreast_folder_per_class_binary/batch_0/train/0/20586960.bmp"
    image_pil = Pili.open(img_path_ood).convert('RGB')
    image_ood_fastai = pil2fast(image_pil)
    data_transform = transforms.Compose(
        [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
    image_ood_tensor = data_transform(image_pil)
    cat_tensor, tensor_class, model_output = learn.predict(image_ood_fastai, with_dropout=True)
    print("Fastai evaluation output")
    print(model_output)
    # pytorch model
    model = learn.model
    image = torch.zeros(1, 3, im_size, im_size).cuda()
    image[0] = image_ood_tensor
    scores_all_classes = nn.functional.softmax(model(image).data.cpu(), dim=1).squeeze()
    print("Pytorch evaluation output")
    print(scores_all_classes)
    print(model(image).data.cpu())

def train_mix_match():
    """
    MixMatch's training main procedure
    """
    global data_labeled, is_colab, logger, args
    learning_rate = args.lr
    number_epochs = args.epochs
    logger = logging.getLogger('main')
    (data_labeled, data_unlabeled, data_full)= get_datasets()

    #start_nf the initial number of features
    """
    Wide ResNet with num_groups and a width of k.
    Each group contains N blocks. start_nf the initial number of features. Dropout of drop_p is applied in between the two convolutions in each block. The expected input channel size is fixed at 3.
    Structure: initial convolution -> num_groups x N blocks -> final layers of regularization and pooli
    """
    #Available backbones
    if(args.model == "wide_resnet"):
        model = models.WideResNet(num_groups=3,N=4,num_classes=args.num_classes,k = 2,start_nf=args.size_image)
    elif(args.model == "densenet"):
        model = models.densenet121(num_classes=args.num_classes)
    elif(args.model == "squeezenet"):
        model = models.squeezenet1_1(num_classes=args.num_classes)
    elif(args.model.strip() == "alexnet"):
        logger.info("Using alexnet")
        model = models.alexnet(num_classes=args.num_classes)
    #using the whole dataset for conventional supervised learning
    if (args.mode.strip() == "fully_supervised"):
        logger.info("Training fully supervised model")
        # Edit: We can find the answer ‘Note that metrics are always calculated on the validation set.’ on this page: https://docs.fast.ai/training.html 42.
        if (is_colab):
            learn = Learner(data_full, model, metrics=[accuracy])
        else: #, callback_fns = [CSVLogger]
            learn = Learner(data_full, model, metrics=[accuracy], callback_fns = [CSVLogger])
    recall = Recall(average = None)
    precision = Precision(average = None)
    #using a partial or subset of labelled data for supervised training
    if (args.mode.strip() == "partial_supervised"):
        logger.info("Training supervised model with a limited set of labeled data")
        if(is_colab):
            #uses loss_func=FlattenedLoss of CrossEntropyLoss()
            learn = Learner(data_labeled, model, metrics=[accuracy])
        else:
            if(args.balanced == 5):
                logger.info("Using balanced cross entropy")
                calculate_cross_entropy = nn.CrossEntropyLoss(weight=class_weights.float())
                learn = Learner(data_labeled, model, metrics=[accuracy], callback_fns = [CSVLogger], loss_func = calculate_cross_entropy)
            else:
                learn = Learner(data_labeled, model, metrics=[accuracy], callback_fns=[CSVLogger])

    #SSDL (MixMatch mode)
    if (args.mode.strip() == "ssdl"):
        logger.info("Training semi supervised model with limited set of labeled data")
        # https://datascience.stackexchange.com/questions/15989/micro-average-vs-macro-average-performance-in-a-multiclass-classification-settin
        if(is_colab):
            learn = Learner(data_unlabeled, model, loss_func=MixupLoss(), callback_fns=[MixMatchTrainer], metrics=[accuracy, recall, precision])
        else:
            learn = Learner(data_unlabeled, model, loss_func=MixupLoss(), callback_fns=[MixMatchTrainer, CSVLogger],
                            metrics=[accuracy])
    #unfreeze the feature extractor if desired
    if(args.unfreeze > 0):
        logger.info("Unfreezing feature extractor to retrain it")
        learn.unfreeze()

    #train the model
    learn.fit_one_cycle(number_epochs, learning_rate, wd=args.weight_decay)
    #test the model
    list_paths_classes_test = []
    for i in range(0, args.num_classes):
        #CHANGE!! DEBUGGING!!
        logger.info("Using test data in the labeled dataset, with paths:")
        logger.info(args.path_labeled + "/test/" + str(i) + "/")
        list_paths_classes_test += [args.path_labeled + "/test/" + str(i) + "/"]

    list_metrics, list_metrics_names = metrics.get_metrics_multi_class(learn, list_paths_classes_test, im_size = 220, name_report_roc = "rocs/" + args.results_file_name + "_batch_" + str(args.batch_number) + "_roc.csv")
    logger.info("Metrics information to be reported...")
    logged_frame = learn.csv_logger.read_logged_file()
    #logged_frame = correct_frame_headers(logged_frame)
    context.write_run_log_all_test_metrics_fastai(logged_frame, args.results_file_name, args.path_labeled, list_metrics, list_metrics_names)
    #save the model weights
    if(args.save_weights):
        path_weights = args.weights_path_name
        logger.info("Saving weights in: " + str(path_weights))
        learn.export(path_weights)
    #test_output(learn)

def correct_frame_headers(logged_frame):
    """
    The learner csv reader is reading wrong the headers, they must be corrected

    """
    logged_frame.columns = [ 'train_loss', 'valid_loss', 'accuracy', 'recall', 'precision', 'dummy', 'time']
    return logged_frame


if __name__ == '__main__':
    global args, counter, context, logger, is_colab, desired_labeled_classes_dist
    is_colab = False
    #gets the command line arguments
    args = cli.parse_commandline_args()
    #convert labeled desired class dist
    desired_labeled_classes_dist = []
    desired_labeled_classes_str = args.desired_labeled_classes_dist
    # assumes a string with the format '0, 0, 0, 11, 0, 0, 0, 0, 0, 19, 0, 9, 0, 0, 0, 0, 0, 0, 11'
    if (desired_labeled_classes_str != ""):
        desired_labeled_classes_dist = [float(s) for s in desired_labeled_classes_str.split(',')]

    logger = logging.getLogger('main')
    context = RunContext(logging, args)
    logger.info("Learning rate " + str(args.lr))
    #MixMatch inits
    start_time = time.time()
    train_mix_match()

    total_time = (time.time() - start_time)
    logger.info("Total training time secs: " + str(total_time))

