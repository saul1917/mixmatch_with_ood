#coding: utf-8

import argparse
import collections
import random
import os
import copy
import shutil


#This script can be modified to work with other mammogram datasets, as long as naming convention for the images and 
#file structure for the dataset are similar to the one used for the CR mammogram dataset.
#Images must exist in a root folder called all_binary located in the same directory as this script, with subdirs called 0 and 1, with benign images stored in 0 and malign in 1
#txt file called CR_all_binary_dirs must be in the same directory as this script.

#Script creates a directory called CR_batches with subdirectories called batch_#, for each of the specified batch_quantity. Each batch_# corresponds to a random subset of the dataset with train/test separated by patients
#########################################################################################################################

parser = argparse.ArgumentParser(description = "Balance and move dataset to directories for training and testing. Division in train/test takes into account patient distribution")
parser.add_argument('-b', action='store', dest='batch_quantity', default=1, type=int, help='Quantity of batches (i.e. subsets of the original dataset). Default: 5')
parser.add_argument('-p', action='store', dest='porc_test', default=0.3, type=float, help='Percentage of testing data.')
parser.add_argument('-l', action='store', dest='limit', type=int, default=400, help='Quantity of data per class for balancing via undersampling. Script tries to pick this same specified amount of images from both classes. If this amount surpasses the available amount of images, the script simply uses all of the available images')
args = parser.parse_args()


#########################################################################################################################



type_count = [0]*2
dispatch = {
    '0' : 0,
    '1' : 1,
}

def print_counter():
    global type_count
    
    print("\n\nSubclass amount ======================================")

    print('B\tM')
    print('\t'.join(str(i) for i in type_count))

    print("======================================================\n\n")


def check_limits(key, limit):
    if(limit <= type_count[dispatch[key]]):
        return True
    return False

def type_counter(key):
    type_count[dispatch[key]] += 1



db_patients = collections.OrderedDict()
db_subclass ={
    '0' : [],
    '1' : []
}


def get_pacients():
    global args

    file = open('CR_all_binary_dirs.txt')
    for row in file.readlines():
        path = row.replace("\n", "")

        
        pacient_id = path.split('/')[2].split("_")[1]

        type = path.split('/')[1]
            
        pacient_id = pacient_id + "_" + type
        if not pacient_id in db_patients:
            db_patients[pacient_id] = []

        
        db_patients[pacient_id] += [path]

        if not pacient_id in db_subclass[type]:
            db_subclass[type] += [pacient_id]
    

    

#Each patient is assumed to only have images of one class
def balance_patients(limit):
    new_txt = []

    
    flag = True


    for subclass in db_subclass:
        
        shuffled_patients = copy.deepcopy(db_subclass[subclass])
        random.shuffle(shuffled_patients)
        
        for patient in shuffled_patients:
            random.shuffle(db_patients[patient])
        
        
        while flag:
            len_pacient = len(db_subclass[subclass])

            for pacient in shuffled_patients:

                for i in range(0, len(db_patients[pacient])):
                    if i == 20 and subclass == '0':  #Can be used to reduce the number of negative samples of a patient for balancing purposes, doesn't affect distribution in CR dataset
                        break
                    
                    type_counter(subclass)
                    new_txt += [db_patients[pacient][i]]

                    
                    if check_limits(subclass, limit):
                        flag = False
                        break
                
                len_pacient-=1

                if not flag:
                    break
            if len_pacient == 0:
                flag = False

        flag = True
       

    print_counter()
    return new_txt


def count_pacients(list):
    paciente_lst = []
    a = []
    b = []

    for elem in list:
        pacient_id = elem.split('/')[2].split('_')[1] + "_" + elem.split('/')[1]
        if not pacient_id in paciente_lst:
            paciente_lst += [pacient_id]

    file = open('CR_all_binary_dirs.txt')
    for row in file.readlines():
        pacient_id = row.split('/')[2].split('_')[1]
        file_id = row.split('/')[2].split('_')[2]
        if not pacient_id in a:
            a += [pacient_id]
        
        b += [file_id]
    print ("Cantidad pacientes en resultante: %d" %(len(paciente_lst)))
    print ("Cantidad pacientes en total: %d" %(len(db_patients)))
    print ("Cantidad pacientes en total double-checked: %d" %(len(a)))
    print ("Cantidad files total: %d" %(len(b)))


def get_mlg_bng_dictionaries(list):

    bening = {
        
    }

    malign = {
        
    }


    for elem in list:


        columns = elem.split('/')
        full_name = columns[2]
        split_name = full_name.split('_')

        patient_id = split_name[1]
        type = columns[1]

        
        if type == "0":
            if not patient_id in bening:
                bening[patient_id] = []

            path = elem
            bening[patient_id] += [path]

        else:
            if not patient_id in malign:
                malign[patient_id] = []

            path = elem
            malign[patient_id] += [path]

    return (bening, malign)




#########################################################################################################################


def print_estadisticas(dictionary):
    
        
    count = 0
    for paciente in dictionary:
        print(paciente, " ", len(dictionary[paciente]))
        count += len(dictionary[paciente])
    print ("@@@@@@@@@@@@ ", count, " @@@@@@@@@@@")

def write_estadisticas(dictionary, file):
    for clase in dictionary:
        file.write("|||||||| " + clase  + " ||||||||\n")
        
            
        count = 0
        for paciente in dictionary[clase]:
            file.write(paciente +  " " +  str(len(dictionary[clase][paciente])) + "\n")
            count += len(dictionary[clase][paciente])
        file.write("@@@@@@@@@@@@ " +  str(count) +  " @@@@@@@@@@@\n")

def write_porcentajes(train, test, file):
    info_train = [0,0]
    info_test = [0,0]
    
    i = 0
    
    for clase in train:
        count = 0
        
            
        for paciente in train[clase]:             
            count += len(train[clase][paciente])
                    
        info_train[i] = count
        i = i + 1

    i = 0
    for clase in test:
        count = 0
        
            
        for paciente in test[clase]:             
            count += len(test[clase][paciente])
                    
        info_test[i] = count
        i = i + 1

    file.write("\n\n\n======================================================")
    file.write("\n Total Benignas Train "+ str(info_train[0]))
    file.write("\n Total Malignas Train "+ str(info_train[1]))
    file.write("\n Total Train "+ str(info_train[0] + info_train[1]))
    file.write("\n\n Total Benignas Test "+ str(info_test[0]))
    file.write("\n Total Malignas Test "+ str(info_test[1]))
    file.write("\n Total Test "+ str(info_test[0] + info_test[1]))

    total = info_train[0] + info_train[1] + info_test[0] + info_test[1]
    por_train = info_train[0] + info_train[1]
    por_train = por_train * 100 / total

    por_test = info_test[0] + info_test[1]
    por_test = por_test * 100 / total
    
    file.write("\n\n TOTAL "+ str(total)+ ", "+ str(por_train)+ "% y "+ str(por_test)+ "%" )





def folder(benignos, malignos, porc_test, debug=True):
    test_dic = dict()
    test_dic["0"] = dict()
    test_dic["1"] = dict()

    train_dic = dict()
    train_dic["0"] = dict()
    train_dic["1"] = dict()

    diccionario_benignos = copy.deepcopy(benignos)
    diccionario_malignos = copy.deepcopy(malignos)
    

    for dictionary, test in [(diccionario_benignos, test_dic["0"]), (diccionario_malignos, test_dic["1"])]:
        
            
        total = 0
        for paciente in dictionary:
            total += len(dictionary[paciente])
            
        total_test = int(total * porc_test)
        count = 0
        for paciente in test:
            
            count += len(test[paciente])
            
        pacientes = list(dictionary.keys())
        random.shuffle(pacientes)

        for paciente in pacientes:
            if(count >= total_test):
                break

            '''if(paciente in diccionario_benignos and paciente in diccionario_malignos and dictionary == diccionario_benignos):
                count += len(dictionary[paciente])
                test_dic["0"][paciente] = diccionario_benignos.pop(paciente)
                test_dic["1"][paciente] = diccionario_malignos.pop(paciente)
                print("---------------------------ambos " + paciente)
            else:
                
            '''
            count += len(dictionary[paciente])
            test[paciente] = dictionary.pop(paciente)
            
    train_dic["0"] = diccionario_benignos
    train_dic["1"] = diccionario_malignos

    if(debug):
        print("Estadisticas train benignos-----------------------------------------------------------------")
        print_estadisticas(train_dic["0"])
     
        
        print("Estadisticas test benignos------------------------------------------------------------------")
        print_estadisticas(test_dic["0"])


        print("\nEstadisticas train malignos-----------------------------------------------------------------")
        print_estadisticas(train_dic["1"])
        
        print("Estadisticas test malignos------------------------------------------------------------------")
        print_estadisticas(test_dic["1"])
    
    return (train_dic, test_dic)


def create_batch(batch_id, train_dict, test_dict):
    directory_name = "batches_CR/batch_" + batch_id
    train_directory = directory_name + "/train"
    train_b_directory = train_directory + "/0"
    train_m_directory = train_directory + "/1"
    test_directory = directory_name + "/test"
    test_b_directory = test_directory + "/0"
    test_m_directory = test_directory + "/1"

    for directory in ["batches_CR", directory_name, train_directory, train_b_directory, train_m_directory, test_directory, test_b_directory, test_m_directory]:
        if not os.path.exists(directory):
            os.mkdir(directory)
            
    file = open("batches_CR/batch_" + batch_id + "_info.txt","a") 
    for diccionary, folder in [(train_dict, train_directory), (test_dict, test_directory)]:
        file.write("particion " + folder + "\n")
        for clase in diccionary:
            clase_dest = clase
                
            
            for paciente in diccionary[clase]:
                for muestra in diccionary[clase][paciente]:
                        
                    destino = folder + "/" + clase_dest + "/" + muestra.split("/")[-1]
                    shutil.copy(muestra, destino)
                    file.write(muestra + "\n")

    file.write("Estadisticas train\n")
    file.write(" \n")
    write_estadisticas(train_dict, file)

    file.write("\n ")
    file.write("Estadisticas test\n")
    file.write("\n ")
    write_estadisticas(test_dict, file)


    write_porcentajes(train_dict, test_dict, file)
    
    file.close()



if __name__ == "__main__":

    for i in range(0, args.batch_quantity):
        db_patients = collections.OrderedDict()
        db_subclass ={
            '0' : [],
            '1' : []
        }
        type_count = [0]*2
        dispatch = {
            '0' : 0,
            '1' : 1,
        }
        
        get_pacients()
        txt = balance_patients(args.limit)
        count_pacients(txt)
        benigno, maligno = get_mlg_bng_dictionaries(txt)
        
    
        train, test = folder(benigno, maligno, args.porc_test)
        
        create_batch(str(i), train, test)
        

    
    
                

    
