import PIL
import os
import os.path
from PIL import Image

#Resize all images in subdirs of a specified directory.
#This operation happens IN PLACE, so existing images are overwritten 

size = 224 #Desired resolution
source_path = 'bmp_resized' #root path of subdirs with images
img_dirs = os.listdir(source_path)
for directory in img_dirs:
    for subdir, dirs, files in os.walk(os.path.join(source_path, directory)):
        for file in files:
        
            f_img = os.path.join(subdir, file)
            img = Image.open(f_img)
            img = img.resize((size,size))
            img.save(f_img)
            print(f_img, " resized")
    
