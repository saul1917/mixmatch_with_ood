# Experiment Management and Documentation

## [mlflow](https://mlflow.org/docs/latest/index.html)
mlflow helps us to document experiments. It allows to share results conveniently and analyze them via a simple GUI. It also introduces a standardized directory structure which will make our lifes easier later on when we want to analyze our results.

### How to install mlflow
Simple, just type (do that inside the oodd4ssdl conda env if you do not want to install mlflow globally)

`pip install mlflow`

And done.

### How mlflow stores experiments
By default mlfow creates a folder in the project directory called `mlruns` where experiments and runs are recorded (note: I included `*/mlruns/**` in the .gitignore for this repo so that results are stored locally for the time being). As quick overview mlflow distinguishes between the following entities 

experiment: collects different runs that belong to the same task, e.g. different training runs on the same data set

run: an individual iteration of an experiment, e.g. with a certain choice of hyperparameters or subset of data

For each run, mlflow provides the following types for documentation:

parameters: e.g. hyperparameters

metrics: e.g. losses, metrics

artifacts: really anything else, e.g. model weights, model predictions, features etc.

In addition, mlflow performs a number of bookkeeping tricks by default. E.g. it automatically detects if it being run inside a repo and keeps track of commit hashes of the repo for each run so that you know which version of code was used to perform the experiment.

### How to use mlflow GUI
I already adjusted our code to track parameters, metrics and losses. You can have a look at `MixMatch_OOD_main.py` to see how. You can also expand to track addtional things if you wish.

To access the GUI, type from the main directory of the oodd4ssdl project

`mlflow server --host 0.0.0.0`

By default it will be on port 5000. This means you can now type in your browser `http://0.0.0.0:5000` and voila: you can use the GUI.

### Sharing is caring: sharing your mlflow server with others
Note: if you of a different way please feel free to share. My compute sits behind a firewall and I found this to be a convenient way to share my local mlflow server with others.

1.  Install ngrok https://ngrok.com/download
2.  From the directory where you installed ngrok run `./ngrok http 5000`. This will create a tunnel.
3.  Take note of the url that ngrok uses for forwarding your port, e.g. `http://ceec5709.ngrok.io`
4.  Share this url with us and then we can have a look at your results, too.



# Data
Link to cifar-10 data I am currently using: https://github.com/YoongiKim/CIFAR-10-images
